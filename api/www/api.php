<?php

die("Deprecated Endpoint");
/*
require_once '../vendor/autoload.php';
session_start();

use alphayax\freebox\os\utils\FreeboxOsApplication;


// CORS
$allowedOrigin = \alphayax\freebox\os\etc\FreehubConfig::getInstance()->getAllowedOrigin();

/// Rendering
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: '. $allowedOrigin);
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Credentials, X-Freehub-token');
header('Access-Control-Allow-Credentials: true');


/// Check request method (OPTIONS, HEADER, etc... will be ignored)
if( in_array( $_SERVER['REQUEST_METHOD'], ['PUT', 'DELETE', 'GET', 'POST'])){

    /// Parse request and exec action
    $action = (new FreeboxOsApplication)->getAction();

    /// Encode and print
    echo json_encode( $action);
}


<?php

require_once '../vendor/autoload.php';

session_start();

$app = new \alphayax\freebox\os\slim\Application();

$app->group('/downloads', function(){

    /// Downloads
    $this->post('/', \alphayax\freebox\os\controller\downloads\Create::class);
    $this->get('/', \alphayax\freebox\os\controller\downloads\GetAll::class);
    $this->delete('/clear', \alphayax\freebox\os\controller\downloads\ClearDone::class);
    $this->put('/{dl_id:[0-9]+}/', \alphayax\freebox\os\controller\downloads\Update::class);
    $this->delete('/{dl_id:[0-9]+}/clear', \alphayax\freebox\os\controller\downloads\Clear::class);
    $this->delete('/{dl_id:[0-9]+}/erase', \alphayax\freebox\os\controller\downloads\Erase::class);
    $this->get('/{dl_id:[0-9]+}/files', \alphayax\freebox\os\controller\downloads\GetFiles::class);

    /// RSS Scans
    $this->group('/rss', function(){
        $this->get('/', \alphayax\freebox\os\controller\downloads\rss\GetAll::class);
        $this->post('/', \alphayax\freebox\os\controller\downloads\rss\Create::class);
        $this->put('/{rss_id:[0-9a-z]+}/', \alphayax\freebox\os\controller\downloads\rss\Update::class);
        $this->delete('/{rss_id:[0-9a-z]+}/', \alphayax\freebox\os\controller\downloads\rss\Delete::class);
        $this->get('/{rss_id:[0-9a-z]+}/check', \alphayax\freebox\os\controller\downloads\rss\Check::class);
    });
});

$app->group('/filesystem/{uid}/{path}', function() {
    $this->get('/', \alphayax\freebox\os\controller\file_system\Get::class);
});

$app->group('/freebox', function(){
    $this->group('/{uid}', function(){
        $this->get('/status', \alphayax\freebox\os\controller\freebox\Status::class);
    });
});

$app->group('/friends', function(){
    $this->get('/', \alphayax\freebox\os\controller\friends\GetAll::class);
    $this->get('/requests', \alphayax\freebox\os\controller\friends\GetRequests::class);
    $this->get('/freebox', \alphayax\freebox\os\controller\friends\GetFreeboxes::class);
});

$app->group('/link', function(){
    $this->get('/', \alphayax\freebox\os\controller\link\Get::class);
    $this->get('/authorizations', \alphayax\freebox\os\controller\link\GetAuthorizations::class);
    $this->post('/', \alphayax\freebox\os\controller\link\Update::class);
    $this->put('/', \alphayax\freebox\os\controller\link\Update2::class);
});

$app->group('/uploads', function(){
    $this->get('/', \alphayax\freebox\os\controller\uploads\GetAll::class);
    $this->delete('/{token}/', \alphayax\freebox\os\controller\uploads\Revoke::class);
});

$app->group('/user', function(){
    $this->put('/', \alphayax\freebox\os\controller\user\Update::class);
    $this->post('/signup', \alphayax\freebox\os\controller\user\Signup::class);
    $this->post('/login', \alphayax\freebox\os\controller\user\Login::class);
    $this->post('/logout', \alphayax\freebox\os\controller\user\Logout::class);
});

$app->run();


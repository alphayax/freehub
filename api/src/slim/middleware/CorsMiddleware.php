<?php

namespace alphayax\freebox\os\slim\middleware;

use alphayax\freebox\os\etc\FreehubConfig;
use Psr;

/**
 * Class CorsMiddleware
 * @package alphayax\freebox\os\slim\middleware
 */
class CorsMiddleware
{
    const ALLOWED_HEADERS = [
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'Origin',
        'Authorization',
        'Access-Control-Allow-Credentials',
        'X-Freehub-token',
    ];

    /**
     * @param \Slim\Http\Request $req
     * @param \Slim\Http\Response $res
     * @param $next
     * @return mixed
     */
    public function __invoke($req, $res, $next) {

        /** @var \Slim\Http\Response $response */
        $response = $next($req, $res);

        return $response
            ->withHeader('Access-Control-Allow-Origin' , static::getAllowedOrigin())
            ->withHeader('Access-Control-Allow-Headers', static::getAllowedHeaders())
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
    }

    /**
     * Return a string with the caller origin
     * @return string
     */
    private static function getAllowedOrigin() : string
    {
        return FreehubConfig::getInstance()->getAllowedOrigin();
    }

    /**
     * Return a string with the allowed headers
     * @return string
     */
    private static function getAllowedHeaders() : string
    {
        return implode(', ', static::ALLOWED_HEADERS);
    }

}

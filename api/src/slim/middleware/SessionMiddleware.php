<?php

namespace alphayax\freebox\os\slim\middleware;

use alphayax\freebox\os\db;
use alphayax\freebox\os\exception\SessionException;
use alphayax\freebox\os\utils;
use Psr;

/**
 * Class CorsMiddleware
 * @package alphayax\freebox\os\slim\middleware
 */
class SessionMiddleware
{
    /**
     * @param \Slim\Http\Request  $req
     * @param \Slim\Http\Response $res
     * @param                     $next
     * @return mixed
     * @throws \alphayax\freebox\os\exception\SessionException
     */
    public function __invoke($req, $res, $next)
    {
        $this->restoreSession();

        return $next($req, $res);
    }

    /**
     * Try to restore PHP Session with the X-Freehub-Token if expired
     * @throws \alphayax\freebox\os\exception\SessionException
     */
    private function restoreSession()
    {
        // If session exists, we don't have anything to restore
        if ( ! empty($_SESSION)) {
            return;
        }

        // If X-Freehub-Token is not provided, we can't restore anything
        if ( ! array_key_exists('HTTP_X_FREEHUB_TOKEN', $_SERVER)) {
            return;
        }

        // If token is empty (eg: login) we don't have anything to restore
        $token = @$_SERVER['HTTP_X_FREEHUB_TOKEN'];
        if (empty($token)) {
            return;
        }

        try {
            $user = db\User_Collection::getFromToken($token);

            // TODO : Check if token is expired

            utils\SessionUser::init($user);
        } catch (\Exception $e) {

            // TODO : Add multiple sessions

            // If token is not found the user must be disconnected
            throw new SessionException('No user found with specified token', 1401, $e);
        }
    }

}

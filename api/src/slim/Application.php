<?php

namespace alphayax\freebox\os\slim;

use alphayax\freebox\os\slim\middleware\CorsMiddleware;
use alphayax\freebox\os\slim\middleware\SessionMiddleware;
use Slim;

/**
 * Class App
 * @package alphayax\tarota
 */
class Application extends Slim\App
{

    /**
     * App constructor.
     * @param array $container
     */
    public function __construct($container = [])
    {
        parent::__construct($container);
        $this->registerErrorHandler();
        $this->registerPhpErrorHandler();
        $this->registerNotFoundHandler();
        $this->registerNotAllowedHandler();
        $this->initCors();

        $this->add(SessionMiddleware::class);
    }

    /**
     * Add Cors
     */
    private function initCors()
    {
        $this->options('/{routes:.+}', function ($request, $response, $args) {
            return $response;
        });

        $this->add(CorsMiddleware::class);
    }

    /**
     *
     */
    private function registerErrorHandler()
    {
        $c = $this->getContainer();
        $c['errorHandler'] = function ($c) {
            return function ($request, $response, $exception) use ($c) {
                return $c['response']
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->withJson([
                        'success' => false,
                        'error'   => $exception->getMessage(),
                        'data'    => [
                            'message' => $exception->getMessage(),
                            'line'    => $exception->getLine(),
                            'file'    => $exception->getFile(),
                        ],
                    ]);
            };
        };
    }

    /**
     *
     */
    private function registerPhpErrorHandler()
    {
        $c = $this->getContainer();
        $c['phpErrorHandler'] = function ($c) {
            return function ($request, $response, $error) use ($c) {
                return $c['response']
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->withJson([
                        'success' => false,
                        'error'   => $error->getMessage(),
                        'data'    => [
                            'message' => $error->getMessage(),
                            'line'    => $error->getLine(),
                            'file'    => $error->getFile(),
                        ],
                    ]);
            };
        };
    }

    /**
     *
     */
    private function registerNotFoundHandler()
    {
        $c = $this->getContainer();
        $c['notFoundHandler'] = function ($c) {
            return function ($request, $response) use ($c) {
                return $c['response']
                    ->withStatus(404)
                    ->withHeader('Content-Type', 'application/json')
                    ->withJson([
                        'success' => false,
                        'error'   => 'Route not found',
                        'data'    => [],
                    ]);
            };
        };
    }

    /**
     *
     */
    private function registerNotAllowedHandler()
    {
        $c = $this->getContainer();
        $c['notAllowedHandler'] = function ($c) {
            return function ($request, $response, $methods) use ($c) {
                return $c['response']
                    ->withStatus(405)
                    ->withHeader('Allow', implode(', ', $methods))
                    ->withHeader('Content-Type', 'application/json')
                    ->withJson([
                        'success' => false,
                        'error'   => 'Requested method is not allowed',
                        'data'    => [
                            'allowed_methods' => $methods,
                        ],
                    ]);
            };
        };
    }

}

<?php
namespace alphayax\freebox\os\services;
use alphayax\freebox\api\v3\services as v3_services;
use alphayax\freebox\api\v3\symbols as v3_symbols;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\Service;
use alphayax\freebox\os\utils\SessionUser;

/**
 * Class FreeboxService
 * @package alphayax\freebox\os\services
 */
class FreeboxService extends Service {

    /**
     * @inheritdoc
     */
    public function executeAction( $action) {
        switch( $action){

        //    case 'get_all'              : $this->getAllFromUid();      break;
        //    case 'get_from_uid'         : $this->getFreeboxFromUid();  break;
        //    case 'get_status_from_uid'  : $this->getStatusFromUid();   break;
        //    case 'get_permissions'      : $this->getPermissions();     break;
            case 'ping'                 : $this->ping();               break;
            case 'remove'               : $this->remove();             break;
            default : $this->actionNotFound( $action);  break;
        }
    }

    /*
     *
     *
    protected function getAllFromUid() {
        $freeboxes = [];

        // Add user freebox infos
        $freeboxes[] = [
            'name'  => SessionUser::getUser()->getName(),
            'uid'   => SessionUser::getUid(),
        ];

        /// Add friends freebox infos
        $friend_uid_xs = (array) SessionUser::getUser()->getFriends()->getConfirmed();
        $friends_uids = array_values( $friend_uid_xs);
        $friends = User_Collection::getFromUids( $friends_uids);

        foreach( $friends as $friend){
            $freeboxes[] = [
                'name'  => $friend->getName(),
                'uid'   => $friend->getUid(),
            ];
        }

        $this->apiResponse->setData( $freeboxes);
    }

    /*
     *
     *
    protected function getStatusFromUid() {
        $uid = @$this->apiRequest['uid'];

        $userAssoc = FreeboxAssoc::getFromUid( $uid);
        $this->application->setAppToken( $userAssoc->getAppToken());
        $this->application->setFreeboxApiHost( $userAssoc->getHost());
        $this->application->openSession();

        $connectionService = new v3_services\config\Connection\Connection( $this->application);
        $status = $connectionService->getStatus();

        $this->apiResponse->setData( $status);
    }

    /*
     *
     *
    protected function getFreeboxFromUid() {
        $uid = @$this->apiRequest['uid'];

        $userAssoc = FreeboxAssoc::getFromUid( $uid);

        $appToken       = $userAssoc->getAppToken();
        $maskedToken    = ! empty( $appToken) ? substr( $appToken, 0, 3) . '***********'. substr( $appToken, -3, 3) : '';

        $this->apiResponse->setData([
            'api_domain'  => $userAssoc->getApiDomain(),
            'https_port'  => $userAssoc->getHttpsPort(),
            'app_token'   => $maskedToken,
        ]);
    }

    /**
     *
     */
    protected function ping() {
        $uid     = @$this->apiRequest['uid'];
        $assoc_x = @$this->apiRequest['assoc'];

        /// Check parameters
        if( empty( $assoc_x['api_domain']) || empty( $assoc_x['https_port']) || empty( $uid)){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( 'Requested parameters are missing');

            return;
        }

        $assoc = new FreeboxAssoc();
        $assoc->init( $assoc_x);

        $this->application->setFreeboxApiHost( $assoc->getHost());

        /// Try to connect to the box
        try {
            $apiVersion = new v3_services\ApiVersion( $this->application);
            $maFreebox  = $apiVersion->getApiVersion();
        }
        catch( \Exception $e){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( $e->getMessage());

            return;
        }

        SessionUser::getUser()->setAssoc( $assoc);
        SessionUser::save();

        $this->apiResponse->setData( $maFreebox);
    }

    /*
     *
     *
    protected function getPermissions() {
        $assoc      = @$this->apiRequest['assoc'];

        if( ! empty( $assoc)){
            $userAssoc = new FreeboxAssoc();
            $userAssoc->init( $assoc);
        } else {
            $userAssoc = SessionUser::getAssoc();
        }

        /// Basic association check
        if( ! $userAssoc->isHostValid() || ! $userAssoc->haveAppToken()){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( 'Invalid association parameters');
            return;
        }

        $this->application->setFreeboxApiHost( $userAssoc->getHost());
        $this->application->setAppToken( $userAssoc->getAppToken());

        /// Open session
        try {
            $this->application->openSession();
        }
        catch( \Exception $e){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( $e->getMessage());
            return;
        }

        /// Save/Update association
        $userAssoc->setIsLinked();
        SessionUser::getUser()->setAssoc( $userAssoc);
        SessionUser::save();

        $this->apiResponse->setData([
            ['name' => 'explorer', 'value' => $this->application->hasPermission( v3_symbols\Permissions::EXPLORER)],
            ['name' => 'download', 'value' => $this->application->hasPermission( v3_symbols\Permissions::DOWNLOADER)],
            ['name' => 'settings', 'value' => $this->application->hasPermission( v3_symbols\Permissions::SETTINGS)],
        ]);
    }

    /**
     * Remove information about association of the current user
     */
    private function remove() {
        SessionUser::getUser()->getAssoc()->init([]);
        SessionUser::save();
    }

}

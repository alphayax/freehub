<?php
namespace alphayax\freebox\os\services;
use alphayax\freebox;
use alphayax\freebox\os\utils\SessionUser;

/**
 * Class FriendsService
 * @package alphayax\freebox\os\services
 */
class FriendsService extends freebox\os\utils\Service {

    /**
     * @inheritdoc
     */
    public function executeAction( $action) {
        switch ( $action){

        //    case 'get_all'              : $this->getAll();                    break;
        //    case 'get_requests'         : $this->getRequests();               break;
            case 'send_request'         : $this->sendRequest();               break;
            case 'answer_request'       : $this->answerRequest();             break;
            case 'remove_friend'        : $this->removeFriend();              break;
            case 'generate_key'         : $this->generateNewFriendshipKey();  break;
            case 'get_friendship_key'   : $this->getFriendshipKey();          break;
            default : $this->actionNotFound( $action);      break;
        }
    }

    /*
     *
     *
    public function getAll() {
        $friend_uid_xs = (array) freebox\os\db\User_Collection::getFromUid( SessionUser::getUid())->getFriends()->getConfirmed();
        $friend_uid_s = array_values( $friend_uid_xs);
        $friends = [];

        foreach( $friend_uid_s as $friend_uid){
            $friends[] = [
                'uid'   => $friend_uid,
                'name'  => freebox\os\db\User_Collection::getFromUid( $friend_uid)->getName(),
            ];
        }

        $this->apiResponse->setData( $friends);
    }

    /**
     *
     *
    public function getRequests() {
        $friendshipRequest_xs = (array) freebox\os\db\User_Collection::getFromUid( SessionUser::getUid())->getFriends()->getRequests();
        $friend_uid_s = array_values( $friendshipRequest_xs);
        $friends = [];

        foreach( $friend_uid_s as $friend_uid){
            $friends[] = [
                'uid'   => $friend_uid,
                'name'  => freebox\os\db\User_Collection::getFromUid( $friend_uid)->getName(),
            ];
        }

        $this->apiResponse->setData( $friends);
    }

    /**
     *
     */
    public function sendRequest()
    {
        $friendName = @$this->apiRequest['friend_name'];

        // Check auto-friendship
        if( $friendName == SessionUser::getUser()->getName()){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( "Tu veux etre ton propre ami ?");
        }

        // Check if user exists
        try {
            $user = freebox\os\db\User_Collection::getFromName( $friendName);
        } catch( \Exception $exception){
            $this->apiResponse->setSuccess( false);
            $this->apiResponse->setErrorMessage( 'Aucun utilisateur trouvé avec ce nom');
            return;
        }

        // Todo : Check if user is already a friend

        // Add friendship request
        $user->getFriends()->addFriendRequest( SessionUser::getUser()->getUid());
        freebox\os\db\User_Collection::update( $user);

        $this->apiResponse->setSuccess( true);
    }

    /**
     * Add a friend to the logged user friend's list
     * Add the logged user to the friend friend's list
     */
    public function answerRequest(){
        $uid        = @$this->apiRequest['uid'];
        $isAccepted = @$this->apiRequest['is_accepted'];

        if( $isAccepted){

            /// Add friend to user
            SessionUser::getUser()->getFriends()->addFriend( $uid);

            /// Add user to friend
            $friendUser = freebox\os\db\User_Collection::getFromUid( $uid);
            $friendUser->getFriends()->addFriend( SessionUser::getUser()->getUid());
            freebox\os\db\User_Collection::update( $friendUser);
        }

        // Remove friend request
        SessionUser::getUser()->getFriends()->removeFriendRequest( $uid);
        SessionUser::save();

        $this->apiResponse->setSuccess( true);
    }

    /**
     * Remove a friend to the logged user friend's list
     * Remove the logged user from the friend friend's list
     */
    public function removeFriend(){
        $uid = @$this->apiRequest['uid'];

        /// Remove friend to user
        SessionUser::getUser()->getFriends()->removeFriend( $uid);
        SessionUser::save();

        /// Remove user to friend
        $friendUser = freebox\os\db\User_Collection::getFromUid( $uid);
        $friendUser->getFriends()->removeFriend( SessionUser::getUser()->getUid());
        freebox\os\db\User_Collection::update( $friendUser);

        $this->apiResponse->setSuccess( true);
    }

    /**
     * Generate a new friendship key, and return the new value
     */
    public function generateNewFriendshipKey() {
        SessionUser::getUser()->getFriends()->generateNewFriendshipKey();
        SessionUser::save();

        $this->apiResponse->setSuccess( true);
        $this->apiResponse->setData([
            "friendship_key" => SessionUser::getUser()->getFriends()->getFriendshipKey(),
        ]);
    }

    /**
     * Return the current user friendship Key
     */
    public function getFriendshipKey() {
        $this->apiResponse->setSuccess( true);
        $this->apiResponse->setData([
            "friendship_key" => SessionUser::getUser()->getFriends()->getFriendshipKey(),
        ]);
    }

}


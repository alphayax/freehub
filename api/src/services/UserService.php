<?php
namespace alphayax\freebox\os\services;
use alphayax\freebox\api\v3\services as v3_services;
use alphayax\freebox\api\v3\symbols as v3_symbols;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\models\User\User;
use alphayax\freebox\os\utils\Service;
use alphayax\freebox\os\utils\SessionUser;

/**
 * Class FreeboxService
 * @package alphayax\freebox\os\services
 */
class UserService extends Service {

    /**
     * @inheritdoc
     */
    public function executeAction( $action) {
        switch( $action){

            case 'get_infos'    : $this->getInfos();     break;
            case 'set_infos'    : $this->setInfos();     break;
            case 'check_session': $this->checkSession(); break;
        //    case 'logout'       : $this->logout();       break;
        //    case 'login'        : $this->login();        break;
        //    case 'signup'       : $this->signUp();       break;
            default : $this->actionNotFound( $action); break;
        }
    }

    /**
     * Return some data from the current session
     */
    private function checkSession()
    {
        $this->apiResponse->setData([
            'is_box_linked' => SessionUser::getUser()->getAssoc()->isLinked(),
            'token_expires' => SessionUser::getUser()->getTokenExpiration(),
        ]);
    }

    /*
     * Destroy the user session
     *
    public function logout() {
        session_destroy();
    }

    /**
     * Return basic information about the current user
     */
    private function getInfos() {
        $this->apiResponse->setData([
            'name'  => SessionUser::getUser()->getName(),
        ]);
    }

    /**
     * Update basic information about the current user
     */
    private function setInfos() {
        $infos = @$this->apiRequest['infos'] ?: [];

        $login    = $infos['login'];
        $password = $infos['password'];
        $password = password_hash($password, PASSWORD_BCRYPT);

        if( $login != SessionUser::getUser()->getName() && User_Collection::nameExists($login)) {
            $this->apiResponse->setErrorMessage('This username is not available');
            return;
        }

        SessionUser::getUser()->setName( $login);
        SessionUser::getUser()->setPass( $password);
        SessionUser::save();

        $this->apiResponse->setData([
            "user info updated" => true
        ]);
    }

    /*
     *
     *
    private function login()
    {
        $usr = @$this->apiRequest['login'] ?: '';
        $pwd = @$this->apiRequest['password'] ?: '';

        try {
            $user = User_Collection::getFromName($usr);

            // Check password
            if( ! password_verify( $pwd, $user->getPass())){
                throw new \Exception('Password incorrect');
            }

            // TODO : Generate new JWT and set it to user object
            $user->generateNewToken();

            SessionUser::init( $user);
            SessionUser::save();
        } catch (\Exception $e) {
            $this->apiResponse->setSuccess(false);
            $this->apiResponse->setErrorMessage($e->getMessage());
            return;
        }

        $this->apiResponse->setData([
            'loggedIn' => true,
            'isBoxLinked' => $user->getAssoc()->isLinked(),
            'uid' => $user->getUid(),
            'token' => $user->getToken(),
            'token_expire' => $user->getTokenExpiration(),
        ]);
    }

    /**
     *
     *
    private function signUp()
    {
        $name = @$this->apiRequest['login'] ?: '';
        $pwd = @$this->apiRequest['password'] ?: '';

        /// Create user
        $user = new User();
        $user->init([
            'uid' => uniqid(),
            'name' => $name,
            'pass' => password_hash($pwd, PASSWORD_BCRYPT),
        ]);
        $user->generateNewToken();

        try {
            User_Collection::insert($user);
            SessionUser::init( $user);
        } catch (\Exception $e) {
            // Duplicate
            $this->apiResponse->setErrorMessage('User already exists');
            return;
        }

        $this->apiResponse->setData([
            'loggedIn' => true,
            'isBoxLinked' => false,
            'uid' => $user->getUid(),
            'token' => $user->getToken(),
            'token_expire' => $user->getTokenExpiration(),
        ]);
    }
    /**/
}

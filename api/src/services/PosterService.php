<?php
namespace alphayax\freebox\os\services;
use alphayax\freebox\api\v3\symbols as v3_symbols;
use alphayax\freebox\os\controllers\TvShow;
use alphayax\freebox\os\utils\MovieTitle;
use alphayax\freebox\os\utils\Service;

/**
 * Class PosterService
 * @package alphayax\freebox\os\services
 */
class PosterService extends Service {

    /**
     * @inheritdoc
     */
    public function executeAction( $action) {
        switch( $action){

            case 'get_image' : $this->getImage();       break;
            default : $this->actionNotFound( $action);  break;
        }
    }

    /**
     *
     */
    private function getImage() {
        $file_name  = @$this->apiRequest['file_name'];
        $image_type = @$this->apiRequest['image_type'];

        $movieTitle = new MovieTitle( $file_name);

        $show = TvShow::getFromTitle( $movieTitle->getCleanName());
        switch ( $image_type){
            case 'poster' : $this->apiResponse->setData( $show->getPosterUri()); break;
            case 'banner' : $this->apiResponse->setData( $show->getBannerUri()); break;
        }
    }

}

<?php

namespace alphayax\freebox\os\controller\user;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Update extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $login = $request->getParam('login');
        $password = $request->getParam('password');

        if (empty($login) || empty($password)) {
            throw new HttpException('Login ou mot de passe vide', 412);
        }

        $password_z = password_hash($password, PASSWORD_BCRYPT);

        /// If user change his username
        if( $login != SessionUser::getUser()->getName()){

            // Check if new login is available
            if( User_Collection::nameExists($login)) {
                throw new HttpException("Nom d'utilisateur déja utilisé", 409);
            }

            SessionUser::getUser()->setName( $login);
        }

        /// Update password
        SessionUser::getUser()->setPass( $password_z);

        SessionUser::save();

        return [];
    }
}

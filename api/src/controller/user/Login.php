<?php

namespace alphayax\freebox\os\controller\user;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\exception\Exception;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\utils\SessionUser;
use Psr;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class UserController
 * @package alphayax\freebox\os\controller
 */
class Login extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $login      = $request->getParam('login');
        $password   = $request->getParam('password');

        if( empty( $login) || empty( $password)){
            throw new HttpException('Login ou mot de passe vide', 412);
        }

        try {
            $user = User_Collection::getFromName($login);

            // Check password
            if( ! password_verify( $password, $user->getPass())){
                throw new HttpException('Password incorrect', 401);
            }

            // TODO : Generate new JWT and set it to user object
            $user->generateNewToken();

            SessionUser::init( $user);
            SessionUser::save();
        } catch (HttpException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new HttpException($e->getMessage(), 401, $e);
        }

        return [
            'loggedIn' => true,
            'isBoxLinked' => $user->getAssoc()->isLinked(),
            'uid' => $user->getUid(),
            'token' => $user->getToken(),
            'token_expire' => $user->getTokenExpiration(),
        ];
    }
}

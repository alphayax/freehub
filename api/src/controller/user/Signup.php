<?php

namespace alphayax\freebox\os\controller\user;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\models\User\User;
use alphayax\freebox\os\utils\SessionUser;
use Psr;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class Signup
 * @package alphayax\freebox\os\controller\user
 */
class Signup extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $login = $request->getParam('login');
        $password = $request->getParam('password');

        if (empty($login) || empty($password)) {
            throw new HttpException('Login ou mot de passe vide', 412);
        }

        /// Create user
        $user = new User();
        $user->init([
            'uid'  => uniqid(),
            'name' => $login,
            'pass' => password_hash($password, PASSWORD_BCRYPT),
        ]);
        $user->generateNewToken();

        try {
            User_Collection::insert($user); // Todo : Check sur login plutot que uid
            SessionUser::init($user);
        } catch (\Exception $e) {
            // Duplicate
            throw new HttpException($e->getMessage(), 409);
        }

        return [
            'loggedIn'     => true,
            'isBoxLinked'  => false,
            'uid'          => $user->getUid(),
            'token'        => $user->getToken(),
            'token_expire' => $user->getTokenExpiration(),
        ];
    }
}

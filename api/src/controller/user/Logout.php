<?php

namespace alphayax\freebox\os\controller\user;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\utils\SessionUser;
use Psr;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class Logout
 * @package alphayax\freebox\os\controller\user
 */
class Logout extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        session_destroy();

        return [];
    }
}

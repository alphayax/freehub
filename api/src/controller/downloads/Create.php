<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

class Create extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        // TODO : Manage other types of download creation (from url list + file)

        $url = $request->getParam('url');


        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $downloadService = new v3_services\download\Download($app);
        $downloadService->addFromUrl($url);
    }

}

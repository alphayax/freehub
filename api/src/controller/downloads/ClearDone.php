<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\services as v3_services;
use alphayax\freebox\api\v3\symbols as v3_symbols;
use Slim\Http\Request;
use Slim\Http\Response;

class ClearDone extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $cleanedTaskIds = [];
        $isSuccess = true;

        $dlService  = new v3_services\download\Download( $app);
        $downloadTasks = $dlService->getAll();
        foreach( $downloadTasks as $downloadTask){
            switch( $downloadTask->getStatus()){
                case v3_symbols\Download\Task\Status::DONE :
                    $isSuccess = $dlService->deleteFromId( $downloadTask->getId()) && $isSuccess;
                    $cleanedTaskIds[] = $downloadTask->getId();
                    break;
            }
        }

        $response->withStatus( $isSuccess ? 206 : 200);

        return $cleanedTaskIds;
    }

}

<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\api\v3\symbols as v3_symbols;
use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\models\Download\DownloadItem;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class Update
 * @package alphayax\freebox\os\controller\downloads
 */
class Update extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $downloadId = @$args['dl_id'];

        $status = $request->getParam('status');

        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $dlService  = new v3_services\download\Download( $app);
        $downloadTask = $dlService->getFromId( $downloadId);

        switch( $status){
            case 'pause'    : $downloadTask->setStatus( v3_symbols\Download\Task\Status::STOPPED);       break;
            case 'download' : $downloadTask->setStatus( v3_symbols\Download\Task\Status::DOWNLOADING);   break;
            case 'retry'    : $downloadTask->setStatus( v3_symbols\Download\Task\Status::RETRY);         break;
        }

        $isSuccess = $dlService->update( $downloadTask);
        if( ! $isSuccess){
            throw new HttpException("Unable to update status (dl_id: $downloadId", 412);
        }

        // Refresh task
        $dlService    = new v3_services\download\Download( $app);
        $downloadTask = $dlService->getFromId( $downloadId);

        return new DownloadItem( $downloadTask);
    }
}

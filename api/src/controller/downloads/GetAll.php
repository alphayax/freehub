<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\models\Download\DownloadItem;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;
use alphayax\freebox\api\v3\services as v3_services;

/**
 * Class GetAll
 * @package alphayax\freebox\os\controller\downloads
 */
class GetAll extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $userAssoc = SessionUser::getAssoc();

        $app = FreehubApplication::getInstance()->getApplication();
        $app->setAppToken( $userAssoc->getAppToken());
        $app->setFreeboxApiHost( $userAssoc->getHost());
        $app->openSession();

        $dlService     = new v3_services\download\Download( $app);
        $downloadTasks = $dlService->getAll();

        $downloadItems = [];
        foreach ($downloadTasks as $downloadTask){
            $downloadItems[] = new DownloadItem( $downloadTask);
        }

        return $downloadItems;
    }
}

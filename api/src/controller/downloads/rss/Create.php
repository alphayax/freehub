<?php

namespace alphayax\freebox\os\controller\downloads\rss;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\models\User\UserConfigDlRss;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Create extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $url = $request->getParam('url');
        $regex = $request->getParam('regex');
        $title = $request->getParam('title');

        $config = new UserConfigDlRss();
        $config->generateId();
        $config->setUrl( $url);
        $config->setTitle( $title);
        $config->setRegex( $regex);
        $config->setLastCheck( time());

        SessionUser::getUser()->getConfig()->addDlRssConfig( $config);
        SessionUser::save();

        return $config;
    }

}

<?php

namespace alphayax\freebox\os\controller\downloads\rss;

use alphayax\freebox\api\v3\services\download\Download;
use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Update extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $rssId = @$args['rss_id'];

        $rssConfig = $request->getParam('item');

        $config = SessionUser::getUser()->getConfig()->getDlRssConfigFromId( $rssId);
        $config->setTitle( $rssConfig['title']);
        $config->setUrl( $rssConfig['rss']);
        $config->setRegex( $rssConfig['pattern']);

        SessionUser::save();

        return $config;
    }

}

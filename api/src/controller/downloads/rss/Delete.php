<?php

namespace alphayax\freebox\os\controller\downloads\rss;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\models\User\UserConfigDlRss;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Delete extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $rssId = @$args['rss_id'];

        SessionUser::getUser()->getConfig()->removeDlRssConfigFromId( $rssId);
        SessionUser::save();

        return [];
    }

}

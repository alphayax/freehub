<?php

namespace alphayax\freebox\os\controller\downloads\rss;

use alphayax\freebox\api\v3\services\download\Download;
use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Check extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $rssId = @$args['rss_id'];

        $rssConfig = SessionUser::getUser()->getConfig()->getDlRssConfigFromId( $rssId);
        if( empty( $rssConfig)){
            throw new HttpException("Configuration not found (rss_id : $rssId)", 404);
        }

        $rss = @simplexml_load_file( $rssConfig->getUrl());
        if( ! $rss){
            throw new HttpException("Impossible de scanner le flux RSS (url : {$rssConfig->getUrl()})", 412);
        }

        $userAssoc = SessionUser::getUser()->getAssoc();
        $app = FreehubApplication::getInstance()->getApplication();
        $app->setAppToken( $userAssoc->getAppToken());
        $app->setFreeboxApiHost( $userAssoc->getHost());
        $app->openSession();

        $downloadService = new Download( $app);

        $downloadedItems = [];
        foreach( $rss->xpath('//item') as $item){
            $title = (string) $item->xpath('title')[0];
            $date  = (string) $item->xpath('pubDate')[0];
            $link  = (string) $item->xpath('link')[0];
            //  $desc  = (string) $item->xpath('description')[0];
            if( preg_match( $rssConfig->getRegex(), $title)){

                if( strtotime( $date) > $rssConfig->getLastCheck()){
                    $rssConfig->setLastPublication( strtotime( $date));
                    $downloadService->addFromUrl( $link);

                    $downloadedItems[] = $title;
                }
            }
        }

        if( ! empty($downloadedItems)){
            $rssConfig->setLastItems( $downloadedItems);
        }

        $rssConfig->setLastCheck( time());
        SessionUser::save();

        return $rssConfig;
    }

}

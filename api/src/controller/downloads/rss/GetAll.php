<?php

namespace alphayax\freebox\os\controller\downloads\rss;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class GetAll extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $configs = SessionUser::getUser()->getConfig()->getDlRssConfigs() ?: [];

        return $configs;
    }

}

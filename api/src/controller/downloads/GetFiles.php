<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\api\v3\services as v3_services;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class GetFiles extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $downloadTaskId = @$args['dl_id'];

        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $dlService    = new v3_services\download\Download( $app);
        $downloadFiles = $dlService->getFilesFromId( $downloadTaskId);

        return $downloadFiles;
    }
}

<?php

namespace alphayax\freebox\os\controller\downloads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\models\Download\DownloadItem;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

class Clear extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $downloadId = @$args['dl_id'];

        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $dlService  = new v3_services\download\Download( $app);
        $downloadTask = $dlService->getFromId( $downloadId);

        $isSuccess = $dlService->deleteFromId( $downloadTask->getId());
        if( ! $isSuccess){
            throw new HttpException("Unable to remove this download (dl_id: $downloadId)", 412);
        }

        return new DownloadItem($downloadTask);
    }
}

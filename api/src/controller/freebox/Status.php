<?php

namespace alphayax\freebox\os\controller\freebox;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

class Status extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $uid = @$args['uid'];

        // TODO : Check if $uid is a friend of session user (or session user)
        $userAssoc = FreeboxAssoc::getFromUid( $uid);

        $app = FreehubApplication::getInstance()->getApplicationWithAssoc($userAssoc);

        $connectionService = new v3_services\config\Connection\Connection( $app);
        $status = $connectionService->getStatus();

        return $status;
    }

}

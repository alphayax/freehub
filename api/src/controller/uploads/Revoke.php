<?php

namespace alphayax\freebox\os\controller\uploads;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class Revoke
 * @package alphayax\freebox\os\controller\uploads
 */
class Revoke extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $token = @$args['token'];

        $app = FreehubApplication::getInstance()->getApplicationWithAssoc(SessionUser::getAssoc());

        $dlService    = new v3_services\FileSystem\FileSharingLink( $app);
        $itemRemoved  = $dlService->deleteFromToken( $token);

        return $itemRemoved;
    }
}

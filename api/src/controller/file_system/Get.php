<?php

namespace alphayax\freebox\os\controller\file_system;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\models\FileSystem\FileInfo;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\api\v3\services as v3_services;
use Slim\Http\Request;
use Slim\Http\Response;

class Get extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $directory = base64_decode( @$args['path'] ?: '/');
        $uid = @$args['uid'];

        $userAssoc = FreeboxAssoc::getFromUid( $uid);
        $app = FreehubApplication::getInstance()->getApplicationWithAssoc($userAssoc);

        $fileSystemListing = new v3_services\FileSystem\FileSystemListing( $app);
        $fileInfos = $fileSystemListing->getFilesFromDirectory( $directory);
        $return = [
            'path'      => $directory,
            'path_part' => $this->getDirectoryParts( $directory),
            'files'     => [],
        ];

        foreach ( $fileInfos as $fileInfo){
            if( substr( $fileInfo->getName(), 0, 1) == '.'){
                continue;
            }
            $fileItem = new FileInfo();
            $fileItem->init( $fileInfo);
            $return['files'][] = $fileItem;
        }

        return $return;
    }

    /**
     * @param $directory
     * @return mixed
     */
    private function getDirectoryParts( $directory) {
        $parts  = explode( DIRECTORY_SEPARATOR, $directory);
        $path   = '';
        $return = [];
        foreach( $parts as $i => $part){
            if( empty( $part) && $i !== 0){
                continue;
            }
            $path .= $part . DIRECTORY_SEPARATOR;
            $return[] = [
                'name'  => $part,
                'path'  => $path,
            ];
        }
        $return[0]['name'] = 'Disques';
        return $return;
    }
}

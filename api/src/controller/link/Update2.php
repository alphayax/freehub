<?php

namespace alphayax\freebox\os\controller\link;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;
use alphayax\freebox\api\v3\services as v3_services;

class Update2 extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $assoc_x = $request->getParam('assoc');

        /// Check parameters
        if( empty( $assoc_x['api_domain']) || empty( $assoc_x['https_port'])){
            throw new HttpException('Invalid api_domain or https_port', 412);
        }

        $assoc = SessionUser::getAssoc();
        $assoc->setApiDomain( $assoc_x['api_domain']);
        $assoc->setHttpsPort( $assoc_x['https_port']);

        $app = FreehubApplication::getInstance()->getApplication();
        $app->setFreeboxApiHost( $assoc->getHost());

        /// Try to connect to the box
        try {
            $apiVersion = new v3_services\ApiVersion( $app);
            $maFreebox  = $apiVersion->getApiVersion();
        }
        catch( \Exception $e){
            throw new HttpException($e->getMessage(), 412, $e);
        }

        SessionUser::getUser()->setAssoc( $assoc);
        SessionUser::save();

        return $maFreebox;
    }
}

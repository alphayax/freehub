<?php

namespace alphayax\freebox\os\controller\link;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class Get extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $userAssoc = SessionUser::getAssoc();

        $appToken       = $userAssoc->getAppToken();
        $maskedToken    = ! empty( $appToken) ? substr( $appToken, 0, 3) . '***********'. substr( $appToken, -3, 3) : '';

        return [
            'api_domain'  => $userAssoc->getApiDomain(),
            'https_port'  => $userAssoc->getHttpsPort(),
            'app_token'   => $maskedToken,
        ];
    }
}

<?php

namespace alphayax\freebox\os\controller\link;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\exception\HttpException;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\FreehubApplication;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox\api\v3\symbols as v3_symbols;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class GetAuthorizations
 * @package alphayax\freebox\os\controller\link
 */
class GetAuthorizations extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $userAssoc = SessionUser::getAssoc();

        /// Basic association check
        if( ! $userAssoc->isHostValid() || ! $userAssoc->haveAppToken()){
            throw new HttpException('Invalid association parameters', 412);
        }

        $app = FreehubApplication::getInstance()->getApplication();
        $app->setFreeboxApiHost( $userAssoc->getHost());
        $app->setAppToken( $userAssoc->getAppToken());

        /// Open session
        try {
            $app->openSession();
        }
        catch( \Exception $e){
            throw $e; // TODO
        }

        /// Save/Update association
        $userAssoc->setIsLinked();
        SessionUser::getUser()->setAssoc( $userAssoc);
        SessionUser::save();

        return [
            ['name' => 'explorer', 'value' => $app->hasPermission( v3_symbols\Permissions::EXPLORER)],
            ['name' => 'download', 'value' => $app->hasPermission( v3_symbols\Permissions::DOWNLOADER)],
            ['name' => 'settings', 'value' => $app->hasPermission( v3_symbols\Permissions::SETTINGS)],
        ];
    }
}

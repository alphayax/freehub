<?php

namespace alphayax\freebox\os\controller\friends;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class GetRequests
 * @package alphayax\freebox\os\controller\friends
 */
class GetRequests extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $friendshipRequest_xs = (array) freebox\os\db\User_Collection::getFromUid( freebox\os\utils\SessionUser::getUid())->getFriends()->getRequests();
        $friend_uid_s = array_values( $friendshipRequest_xs);
        $friends = [];

        foreach( $friend_uid_s as $friend_uid){
            $friends[] = [
                'uid'   => $friend_uid,
                'name'  => freebox\os\db\User_Collection::getFromUid( $friend_uid)->getName(),
            ];
        }

        return $friends;
    }
}

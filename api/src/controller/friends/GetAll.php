<?php

namespace alphayax\freebox\os\controller\friends;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\utils\SessionUser;
use alphayax\freebox;
use Psr;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class GetAll
 * @package alphayax\freebox\os\controller\friends
 */
class GetAll extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $friend_uid_xs = (array) freebox\os\db\User_Collection::getFromUid( SessionUser::getUid())->getFriends()->getConfirmed();
        $friend_uid_s = array_values( $friend_uid_xs);
        $friends = [];

        foreach( $friend_uid_s as $friend_uid){
            $friends[] = [
                'uid'   => $friend_uid,
                'name'  => freebox\os\db\User_Collection::getFromUid( $friend_uid)->getName(),
            ];
        }

        return $friends;
    }
}

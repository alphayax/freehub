<?php

namespace alphayax\freebox\os\controller\friends;

use alphayax\freebox\os\controller\AbstractController;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\utils\SessionUser;
use Slim\Http\Request;
use Slim\Http\Response;

class GetFreeboxes extends AbstractController
{
    /**
     * @inheritdoc
     */
    protected function exec(Request $request, Response $response, array $args)
    {
        $freeboxes = [];

        // Add user freebox infos
        $freeboxes[] = [
            'name'  => SessionUser::getUser()->getName(),
            'uid'   => SessionUser::getUid(),
        ];

        /// Add friends freebox infos
        $friend_uid_xs = (array) SessionUser::getUser()->getFriends()->getConfirmed();
        $friends_uids = array_values( $friend_uid_xs);
        $friends = User_Collection::getFromUids( $friends_uids);

        foreach( $friends as $friend){
            $freeboxes[] = [
                'name'  => $friend->getName(),
                'uid'   => $friend->getUid(),
            ];
        }

        return $freeboxes;
    }

}

<?php

namespace alphayax\freebox\os\controller;

use alphayax\freebox\os\exception\HttpException;
use Psr;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AbstractAction
 * @package alphayax\tarota\utils
 */
abstract class AbstractController
{
    /** @var \Psr\Container\ContainerInterface */
    protected $container;

    /**
     * AbstractAction constructor.
     * @param \Psr\Container\ContainerInterface $container
     */
    public function __construct(Psr\Container\ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        try {

            $data = $this->exec($request, $response, $args);

            return $response->withJson([
                'success' => true,
                'data' => $data,
                'error' => null,
            ]);
        }
        catch (HttpException $e) {
            return $response
                ->withStatus($e->getCode())
                ->withJson([
                    'success' => false,
                    'data' => null,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                    ],
                ]);
        }
        catch( \Exception $e) {
            return $response
                ->withStatus(500)
                ->withJson([
                    'success' => false,
                    'data' => null,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                    ],
                ]);
        }
    }

    /**
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     * @return mixed
     */
    abstract protected function exec(Request $request, Response $response, array $args);

}

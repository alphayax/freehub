<?php
namespace alphayax\freebox\os\db;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\freebox\os\exception\db\DuplicateEntryException;
use alphayax\freebox\os\models\Movie;
use alphayax\freebox\os\utils\BetaSeries;
use MongoDB;

/**
 * Class Movie_Collection
 * @package alphayax\freebox\os\db
 */
class Movie_Collection {

    /**
     * @param $name
     * @return array|null|object
     */
    public static function getFromName( $name) {

        // Search on database
        $result = FreehubDb::getInstance()->getMovies()->findOne([
            'title'   => $name,
        ]);

        // Trivial case : we find the result
        if( ! empty( $result)) {
            return $result;
        }

        // Try to find on BetaSeries
        $movies = BetaSeries\services\Movies::search( $name);
        if( empty( $movies)){
            return null;
        }
        $movie = $movies[0];  // I feel lucky... Todo : better search in results

        /*
        // Check if not already present under another name
        $result = FreehubDb::getInstance()->getTvShows()->findOne([
            'title'   => $movie['title'],
        ]);
        */

        // If the show was'nt in the database
        //if( empty( $result)){

            // If the given name is not in aliases, we add it
            //if( in_array( $name, $movie['aliases'])){
            //    $movie['aliases'][] = $name;
            //}

            // Save for next search
            FreehubDb::getInstance()->getMovies()->insertOne( $movie);
            return $movie;
        //}

        /*
        // We add the given name to the aliases list (because we could'nt find it first)
        $aliases = $result['aliases'];
        $aliases[] = $name;

        // Save for next search
        FreehubDb::getInstance()->getTvShows()->updateOne( [
            '_id'   => $result['_id'],
        ], [
            '$set' => ['aliases' => $aliases ],
        ]);

        return $result;
        */
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return \alphayax\freebox\os\models\TvShow[]
     */
    public static function getAll( $offset = 0, $limit = 5) {
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new MongoDB\Driver\Manager($mongoDbUri);
        $query   = new MongoDB\Driver\Query([], [
            'skip'  => $offset,
            'limit' => $limit,
            'sort'  => [ 'title' => 1 ],
        ]);

        $cursor = $manager->executeQuery('freehub.movies', $query);
        $cursor->setTypeMap(['root' => Movie::class, 'array' => 'array']);

        return $cursor->toArray();
        // TODO : Passer par fetch
    }
    /**
     * Generic method to get movies
     * @param array $selection
     * @return Movie[]
     */
    public static function fetch( array $selection = []) {
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new MongoDB\Driver\Manager( $mongoDbUri);
        $query   = new MongoDB\Driver\Query( $selection);

        $cursor = $manager->executeQuery('freehub.movies', $query);
        $cursor->setTypeMap([ 'root' => Movie::class]);

        return $cursor->toArray();
    }

    /**
     * @param \alphayax\freebox\os\models\Movie $movie
     * @throws \alphayax\freebox\os\exception\db\DuplicateEntryException
     */
    public static function insert( Movie $movie)
    {
        /// Check if already in database
        $movie_x = static::fetch([
            'imdb_id'   => $movie->getImdbId(),
        ]);

        /// Check Duplicate
        if( ! empty( $movie_x)) {
            throw new DuplicateEntryException( 'Movie already in database for imdb_id : '. $movie->getImdbId());
        }

        /// Insert
        FreehubDb::getInstance()->getMovies()->insertOne( $movie);
    }

}

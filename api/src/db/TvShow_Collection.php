<?php
namespace alphayax\freebox\os\db;
use alphayax\freebox\os\exception\db\DuplicateEntryException;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\freebox\os\models\TvShow;
use MongoDB;

/**
 * Class TvShow_Collection
 * @package alphayax\freebox\os\db
 */
class TvShow_Collection {

    /**
     * Generic method to get tv shows
     * @param array $selection
     * @return TvShow[]
     */
    public static function fetch( array $selection = []) {
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new MongoDB\Driver\Manager( $mongoDbUri);
        $query   = new MongoDB\Driver\Query( $selection);

        $cursor = $manager->executeQuery('freehub.tv_shows', $query);
        $cursor->setTypeMap([ 'root' => TvShow::class]);

        return $cursor->toArray();
    }

    /**
     * @param $name
     * @return TvShow|null
     */
    public static function findFromAlias( $name) {
        /** @var TvShow $tvShow */
        $tvShow = FreehubDb::getInstance()->getTvShows()->findOne([
            'aliases'   => $name,
        ]);

        return $tvShow;
    }

    /**
     * @param \alphayax\freebox\os\models\TvShow $tvShow
     * @throws \alphayax\freebox\os\exception\db\DuplicateEntryException
     */
    public static function insert( TvShow $tvShow)
    {
        /// Check if already in database
        $tv_show_x = static::fetch([
            'imdb_id'   => $tvShow->getImdbId(),
        ]);

        /// Check Duplicate
        if( ! empty( $tv_show_x)) {
            throw new DuplicateEntryException( 'TvShow already in database for imdb_id : '. $tvShow->getImdbId());
        }

        /// Insert
        FreehubDb::getInstance()->getTvShows()->insertOne( $tvShow);
    }

    /**
     * @param $name
     * @return TvShow|null
     */
    public static function getFromName( $name) {

        // Search on database
        /** @var TvShow $result */
        $result = static::findFromAlias( $name);

        // Trivial case : we find the result
        if( ! empty( $result)) {
            return $result;
        }

        return null;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return \alphayax\freebox\os\models\TvShow[]
     */
    public static function getAll( $offset = 0, $limit = 5) {
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new \MongoDB\Driver\Manager($mongoDbUri);
        $query   = new \MongoDB\Driver\Query([], [
            'skip'  => $offset,
            'limit' => $limit,
            'sort'  => [ 'title' => 1 ],
        ]);

        $cursor = $manager->executeQuery('freehub.tv_shows', $query);
        $cursor->setTypeMap(['root' => TvShow::class, 'array' => 'array']);

        return $cursor->toArray();
    }

}

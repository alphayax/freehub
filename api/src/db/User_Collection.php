<?php
namespace alphayax\freebox\os\db;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\freebox\os\exception\db\DuplicateEntryException;
use alphayax\freebox\os\models\User\User;
use MongoDB;

/**
 * Class User_Collection
 * @package alphayax\freebox\os\db
 */
class User_Collection {

    /**
     * Generic method to get users
     * @param array $selection
     * @return array
     * @throws \Exception
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function fetch( array $selection = []) {
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new MongoDB\Driver\Manager($mongoDbUri);
        $query   = new MongoDB\Driver\Query( $selection);

        $cursor = $manager->executeQuery('freehub.users', $query);
        $cursor->setTypeMap([ 'root' => User::class]);

        return $cursor->toArray();
    }

    /**
     * @param string $uid
     * @return User
     * @throws \Exception
     */
    public static function getFromUid( $uid) {

        /** @var User $user_x */
        $user_x = FreehubDb::getInstance()->getUser()->findOne([
            'uid'   => $uid,
        ]);

        /// If user is found
        if( empty( $user_x)) {
            throw new \Exception( 'User not found for uuid '. $uid);
        }

        return $user_x;
    }

    /**
     * @param string $name
     * @return User
     * @throws \Exception
     */
    public static function getFromName( string $name) : User
    {
        /** @var User $user_x */
        $user_x = FreehubDb::getInstance()->getUser()->findOne([
            'name'   => $name,
        ]);

        /// If user is not found
        if( empty( $user_x)) {
            throw new \Exception('User not found');
        }

        return $user_x;
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function nameExists(string $name) : bool
    {
        return FreehubDb::getInstance()->getUser()->count([
            'name'   => $name,
        ]) > 0;
    }

    /**
     * @param string $token
     * @return User
     * @throws \Exception
     */
    public static function getFromToken(string $token) : User {

        /** @var User $user_x */
        $user_x = FreehubDb::getInstance()->getUser()->findOne([
            'token'   => $token,
        ]);

        /// If user is not found
        if( empty( $user_x)) {
            throw new \Exception('User not found');
        }

        return $user_x;
    }

    /**
     * @param string[] $uid
     * @return User[]
     */
    public static function getFromUids( array $uid) {

        // Causes Segfault :
        // try{
        //     $user_xs = FreehubDb::getInstance()->getUser()->find([
        //         'uid'   => [ '$in' => $uid],
        //     ]);
        // }
        // catch( \Exception $e){
        //     $user_xs = [];
        //     trigger_error( $e->getMessage());
        // }


        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $manager = new MongoDB\Driver\Manager($mongoDbUri);
        $query   = new MongoDB\Driver\Query([
            'uid'   => [ '$in' => $uid],
        ]);

        $cursor = $manager->executeQuery('freehub.users', $query);
        $cursor->setTypeMap([ 'root' => User::class]);

        return $cursor->toArray();
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public static function insert( User $user)
    {
        /// Check if already in database
        $user_x = FreehubDb::getInstance()->getUser()->findOne([
            'uid'   => $user->getUid(),
        ]);

        /// Check Duplicate (uid)
        if( ! empty( $user_x)) {
            throw new DuplicateEntryException( 'User already exist for uid '. $user->getUid());
        }

        /// Check Duplicate (name)
        if( static::nameExists( $user->getName())) {
            throw new DuplicateEntryException( 'Name is already taken : '. $user->getName());
        }

        /// Insert
        FreehubDb::getInstance()->getUser()->insertOne( $user);
    }

    /**
     * @param User $user
     * @return \MongoDB\UpdateResult
     */
    public static function update( User $user) {
        return FreehubDb::getInstance()->getUser()->updateOne([
            'uid' => $user->getUid(),
        ], [
            '$set' => $user,
        ]);
    }

}

<?php
namespace alphayax\freebox\os\db;
use alphayax\freebox\os\etc\FreehubConfig;
use MongoDB;

/**
 * Class FreehubDb
 * @package alphayax\freebox\os\db
 */
class FreehubDb {

    /** @var \MongoDB\Client */
    protected $client;

    /** @var static */
    protected static $self;

    /**
     * @return \alphayax\freebox\os\db\FreehubDb|static
     */
    public static function getInstance() {
        if( ! empty( static::$self)){
            return static::$self;
        }
        return new static;
    }

    /**
     * FreehubDb constructor.
     */
    protected function __construct(){
        $mongoDbUri = FreehubConfig::getInstance()->getMongoDbUri();
        $this->client = new MongoDB\Client( $mongoDbUri);
    }

    /**
     * @return \MongoDB\Collection
     */
    public function getMovies() {
        return $this->client->selectDatabase( 'freehub')->selectCollection( 'movies');
    }

    /**
     * @return \MongoDB\Collection
     */
    public function getTvShows() {
        return $this->client->selectDatabase( 'freehub')->selectCollection( 'tv_shows');
    }

    /**
     * @return \MongoDB\Collection
     */
    public function getUser() {
        return $this->client->selectDatabase( 'freehub')->selectCollection( 'users');
    }

}

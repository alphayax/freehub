<?php
namespace alphayax\freebox\os\controllers;
use alphayax\freebox\os\db\TvShow_Collection;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\freebox\os\utils\BetaSeries;

/**
 * Class TvShow
 * @package alphayax\freebox\os\controllers
 */
class TvShow {

    /** @var BetaSeries\models\TvShow\TvShow */
    protected $tvShow;

    /**
     * @param $title
     * @return \alphayax\freebox\os\models\TvShow|null
     */
    public static function getFromTitle( $title) {

        // Check in database
        $tvShow = TvShow_Collection::findFromAlias( $title);

        // If result is already in database
        if( ! empty( $tvShow)){
            return $tvShow;
        }

        $tvShow = static::addFromTitle( $title);

        return $tvShow;
    }

    /**
     * @param string $title
     * @return \alphayax\freebox\os\models\TvShow|null
     */
    public static function addFromTitle( $title) {

        $tvShow = static::searchOverInternet( $title);

        // Temporary fix to cast in subclass
        $tvShow_x = $tvShow->jsonSerialize();
        $tvShow = new \alphayax\freebox\os\models\TvShow();
        $tvShow->jsonUnserialize( $tvShow_x);

        // If the given name is not in aliases, we add it
        if( ! in_array( $title, $tvShow->getAliases())){
            $tvShow->addAlias( $title);
        }

        // Cache images
        static::cacheImages( $tvShow);

        // Save for next search
        TvShow_Collection::insert( $tvShow);

        return $tvShow;
    }


    /**
     * @param string $title
     * @return \alphayax\freebox\os\utils\BetaSeries\models\TvShow\TvShow
     * @throws \Exception
     */
    public static function searchOverInternet( string $title) : BetaSeries\models\TvShow\TvShow {

        try{
            // Try to find on TheTvDb
            $theTvDbId = static::searchOverTheTvDb( $title);

            return BetaSeries\services\TvShow::getFromTheTvDbId( $theTvDbId);

        } catch ( \Exception $e){
            // Try to find on BetaSeries
            $tvShows = BetaSeries\services\TvShow::search( $title);
            if( empty( $tvShows)){
                throw new \Exception( 'Tv show not found on internet');
            }

            // TODO Better search results
            return $tvShows[0];
        }
    }

    /**
     * @param string $title
     * @return int TvDbId
     * @throws \Exception
     * @todo Better search into results
     */
    protected static function searchOverTheTvDb( string $title) : int {
        $title = trim( $title);

        // Try to find on TheTvDb
        $series = \alphayax\freebox\os\utils\TheTvDb\services\TvShow::search($title);

        if( count( $series) == 1){
            return $series[0]->getId();
        }

        $title_lc = strtolower( $title);

        // Search in titles
        foreach( $series as $seriesSearchData){
            if( strtolower( $seriesSearchData->getSeriesName()) == $title_lc){
                return $seriesSearchData->getId();
            }
        }

        // Search in aliases
        foreach( $series as $seriesSearchData){
            $aliases = $seriesSearchData->getAliases();
            foreach ($aliases as $alias){
                if( strtolower( $alias) == $title_lc){
                    return $seriesSearchData->getId();
                }
            }
        }

        // Fallback to the first item
        return $series[0]->getId();
    }

    /**
     * @param \alphayax\freebox\os\models\TvShow $tvShow
     * @todo : Do not overwrite banner and poster
     */
    public static function cacheImages( \alphayax\freebox\os\models\TvShow $tvShow) {

        if( $tvShow->getImages()->hasShow()){
            $image = static::downloadFromBetaSeries( $tvShow, $tvShow->getImages()->getShow(), 'show');
            $tvShow->setImgBanner( $image);
        }
        if( $tvShow->getImages()->hasBanner()){
            $image = static::downloadFromBetaSeries( $tvShow, $tvShow->getImages()->getBanner(), 'banner');
            $tvShow->setImgBanner( $image);
        }

        if( $tvShow->getImages()->hasBox()){
            $image = static::downloadFromBetaSeries( $tvShow, $tvShow->getImages()->getBox(), 'box');
            $tvShow->setImgPoster( $image);
        }
        if( $tvShow->getImages()->hasPoster()){
            $image = static::downloadFromBetaSeries( $tvShow, $tvShow->getImages()->getPoster(), 'poster');
            $tvShow->setImgPoster( $image);
        }
    }

    /**
     * @param \alphayax\freebox\os\models\TvShow $tvShow
     * @param string                             $posterUrl
     * @param                                    $kind
     * @return string
     * @throws \Exception
     */
    protected static function downloadFromBetaSeries( \alphayax\freebox\os\models\TvShow $tvShow, $posterUrl, $kind) : string {

        /// Configuration check
        $static_afi = FreehubConfig::getInstance()->getStaticVolume();

        /// Download poster
        $img = file_get_contents( $posterUrl);
        if( empty( $img)){
            throw new \Exception('Image cannot be downloaded : '. $posterUrl);
        }

        $image_bf = $tvShow->getImdbId() .'.'. $kind;
        $poster_afi = $static_afi . $image_bf;
        $fileSaved = file_put_contents( $poster_afi, $img);
        if( false === $fileSaved){
            throw new \Exception('Image cannot be saved : '. $posterUrl);
        }

        return $image_bf;
    }

}

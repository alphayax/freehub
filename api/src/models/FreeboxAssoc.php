<?php
namespace alphayax\freebox\os\models;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\utils\Model;

/**
 * Class FreeboxAssoc
 * @package alphayax\freebox\os\models
 */
class FreeboxAssoc extends Model {

    /** @var string */
    protected $api_domain;

    /** @var int */
    protected $https_port;

    /** @var string */
    protected $app_token;

    /** @var string */
    protected $track_id;

    /** @var boolean */
    protected $is_linked = false;

    /**
     * @param array $data
     */
    public function init( array $data = []) {
        $this->api_domain = @$data['api_domain'] ?: '';
        $this->https_port = @$data['https_port'] ?: '';
        $this->app_token  = @$data['app_token']  ?: '';
        $this->track_id   = @$data['track_id']   ?: 0;
        $this->is_linked  = @$data['is_linked']  ?: false;
    }

    /**
     * @param string $uid
     * @return \alphayax\freebox\os\models\FreeboxAssoc
     */
    public static function getFromUid( $uid) {
        return User_Collection::getFromUid( $uid)->getAssoc();
    }

    /**
     * @return string
     */
    public function getApiDomain() : string {
        return $this->api_domain;
    }

    /**
     * @param string $api_domain
     */
    public function setApiDomain($api_domain)
    {
        $this->api_domain = $api_domain;
    }

    /**
     * @return int
     */
    public function getHttpsPort() : int {
        return $this->https_port;
    }

    /**
     * @param int $https_port
     */
    public function setHttpsPort($https_port)
    {
        $this->https_port = $https_port;
    }

    /**
     * @return string
     */
    public function getAppToken() : string {
        return $this->app_token;
    }

    /**
     * @return string
     */
    public function getTrackId() {
        return $this->track_id;
    }

    /**
     * @return string
     */
    public function getHost() : string {
        return 'https://' . $this->api_domain . ':' . $this->https_port;
    }

    /**
     * @param bool $isLinked
     */
    public function setIsLinked( bool $isLinked = true) {
        $this->is_linked = $isLinked;
    }

    /**
     * @return boolean
     */
    public function isLinked() : bool {
        return $this->is_linked;
    }

    /**
     * @return bool
     */
    public function isHostValid() : bool {
        return ! empty( $this->api_domain) && ! empty( $this->https_port);
    }

    /**
     * @return bool
     */
    public function haveAppToken() : bool {
        return ! empty( $this->app_token);
    }

}

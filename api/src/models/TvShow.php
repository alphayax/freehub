<?php
namespace alphayax\freebox\os\models;
use alphayax\freebox\os\etc\FreehubConfig;

/**
 * Class TvShow
 * @package alphayax\freebox\os\models
 */
class TvShow extends \alphayax\freebox\os\utils\BetaSeries\models\TvShow\TvShow {

    /** @var string */
    protected $img_poster = '';

    /** @var string */
    protected $img_banner = '';

    /**
     * @return string
     */
    public function getImgPoster() : string {
        return $this->img_poster;
    }

    /**
     * @param string $img_poster
     */
    public function setImgPoster( string $img_poster) {
        $this->img_poster = $img_poster;
    }

    /**
     * @return string
     */
    public function getImgBanner() : string {
        return $this->img_banner;
    }

    /**
     * @param string $img_banner
     */
    public function setImgBanner( string $img_banner) {
        $this->img_banner = $img_banner;
    }

    /**
     * @return string
     */
    public function getPosterUri() : string {
        $staticEndpoint = FreehubConfig::getInstance()->getStaticEndpoint();
        return $staticEndpoint . $this->getImgPoster();
    }

    /**
     * @return string
     */
    public function getBannerUri() : string {
        $staticEndpoint = FreehubConfig::getInstance()->getStaticEndpoint();
        return $staticEndpoint . $this->getImgBanner();
    }

}

<?php
namespace alphayax\freebox\os\models\Download;
use alphayax\freebox\api\v3\models\Download\Task;
use alphayax\freebox\os\utils\Model;
use alphayax\freebox\os\utils\MovieTitle;


class DownloadItem extends Model {

    /** @var \alphayax\freebox\api\v3\models\Download\Task */
    protected $downloadTask;

    protected $image = '';

    protected $path;

    /** @var MovieTitle */
    protected $movieTitle;

    /**
     * DownloadItem constructor.
     * @param \alphayax\freebox\api\v3\models\Download\Task $downloadTask
     */
    public function __construct( Task $downloadTask) {
        $this->downloadTask = $downloadTask;
        $this->movieTitle   = new MovieTitle( $this->downloadTask->getName());
        $this->path         = base64_decode( $this->downloadTask->getDownloadDir());
    }

}

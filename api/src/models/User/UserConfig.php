<?php
namespace alphayax\freebox\os\models\User;
use alphayax\freebox\os\utils\Model;

/**
 * Class UserFriends
 * @package alphayax\freebox\os\models\User
 */
class UserConfig extends Model {

    /** @var UserConfigDlRss[] */
    protected $dlRssConfigs = [];

    /**
     * @param \alphayax\freebox\os\models\User\UserConfigDlRss $configDlRss
     */
    public function addDlRssConfig( UserConfigDlRss $configDlRss) {
        $this->dlRssConfigs[] = $configDlRss;
    }

    /**
     * @param string $id
     * Todo : Ugly... Too high to do better right now
     */
    public function removeDlRssConfigFromId( $id) {
        $configs = [];
        foreach( $this->dlRssConfigs as $config){
            if( $config->getId() == $id){
                continue;
            }
            $configs[] = $config;
        }
        $this->dlRssConfigs = $configs;
    }

    /**
     * @return \alphayax\freebox\os\models\User\UserConfigDlRss[]
     */
    public function getDlRssConfigs() {
        return $this->dlRssConfigs;
    }

    /**
     * @param string $id
     * @return \alphayax\freebox\os\models\User\UserConfigDlRss
     */
    public function getDlRssConfigFromId( $id) {
        foreach( $this->dlRssConfigs as $config){
            if( $config->getId() == $id){
                return $config;
            }
        }
        return null;
    }

    /**
     * @param array $data
     */
    public function init( array $data = [])
    {
        $this->init_ConfigRss(@$data['dlRssConfigs'] ?: []);
    }

    /**
     * @param array $configRss_xs
     */
    protected function init_ConfigRss( array $configRss_xs = [])
    {
        foreach ($configRss_xs as $configRss_x) {
            $configRss = new UserConfigDlRss();
            $configRss->init( $configRss_x);
            $this->addDlRssConfig( $configRss);
        }
    }

}

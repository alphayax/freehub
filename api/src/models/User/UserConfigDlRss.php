<?php

namespace alphayax\freebox\os\models\User;

use alphayax\freebox\os\utils\Model;

/**
 * Class UserFriends
 * @package alphayax\freebox\os\models\User
 */
class UserConfigDlRss extends Model
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var string */
    protected $url;

    /** @var string */
    protected $regex;

    /** @var int */
    protected $lastCheck;

    /** @var int */
    protected $lastPublication;

    /** @var array */
    protected $lastItems;

    /**
     * @param array $data
     */
    public function init(array $data = [])
    {
        $this->id = @$data['id'];
        $this->title = @$data['title'];
        $this->lastCheck = @$data['last_date'];
        $this->lastPublication = @$data['last_publication'];
        $this->lastItems = @$data['last_items'] ?: [];
        $this->regex = @$data['pattern'];
        $this->url = @$data['rss'];
        /*
        foreach ( $data as $propertyName => $propertyValue){
            if( property_exists( static::class, $propertyName)){
                $this->$propertyName = $propertyValue;
            }
        }
        */
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generate a random id
     */
    public function generateId()
    {
        $this->id = uniqid();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * @param string $regex
     */
    public function setRegex($regex)
    {
        $this->regex = $regex;
    }

    /**
     * @return string
     */
    public function getLastCheck()
    {
        return $this->lastCheck;
    }

    /**
     * @param string $lastCheck
     */
    public function setLastCheck($lastCheck)
    {
        $this->lastCheck = $lastCheck;
    }

    /**
     * @return int
     */
    public function getLastPublication()
    {
        return $this->lastPublication;
    }

    /**
     * @param int $lastPublication
     */
    public function setLastPublication($lastPublication)
    {
        $this->lastPublication = $lastPublication;
    }

    /**
     * @return array
     */
    public function getLastItems(): array
    {
        return $this->lastItems;
    }

    /**
     * @param array $lastItems
     */
    public function setLastItems(array $lastItems)
    {
        $this->lastItems = $lastItems;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id'               => $this->id,
            'title'            => $this->title,
            'last_date'        => $this->lastCheck,
            'last_publication' => $this->lastPublication,
            'last_items'       => $this->lastItems,
            'pattern'          => $this->regex,
            'rss'              => $this->url,
        ];
    }

}

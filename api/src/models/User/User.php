<?php
namespace alphayax\freebox\os\models\User;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\utils\Model;

/**
 * Class User
 * @package alphayax\freebox\os\utils
 */
class User extends Model {

    /** @var string */
    protected $uid;

    /** @var string */
    protected $name;

    /** @var string */
    protected $pass;

    /** @var string */
    protected $token;

    /** @var FreeboxAssoc */
    protected $assoc;

    /** @var UserFriends */
    protected $friends;

    /** @var UserConfig */
    protected $config;

    /** @var \MongoDate */
    protected $creation_date;

    /** @var int */
    protected $token_expiration;

    /**
     * @param array $user_x
     */
    public function init( array $user_x) {
        $this->uid              =  $user_x['uid'];
        $this->name             = @$user_x['name'] ?: '';
        $this->pass             = @$user_x['pass'] ?: '';
        $this->assoc            = new FreeboxAssoc();
        $this->friends          = new UserFriends();
        $this->config           = new UserConfig();
        $this->creation_date    = time();
        $this->token_expiration = time();
        $this->assoc->init(   @$user_x['assoc']   ?: []);
        $this->friends->init( @$user_x['friends'] ?: []);
        $this->config->init(  @$user_x['config']  ?: []);
    }

    /**
     * @return string
     */
    public function getUid() : string {
        return $this->uid;
    }

    /**
     * @return bool
     */
    public function hasName() : bool {
        return ! empty( $this->name);
    }

    /**
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass(string $pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * Generate a new token
     */
    public function generateNewToken()
    {
        $this->token = hash('sha256', uniqid());
        $this->token_expiration = time() + (60 * 60 * 24);
    }

    /**
     * @return \alphayax\freebox\os\models\FreeboxAssoc
     */
    public function getAssoc() : FreeboxAssoc {
        return $this->assoc;
    }

    /**
     * @param \alphayax\freebox\os\models\FreeboxAssoc $assoc
     */
    public function setAssoc( FreeboxAssoc $assoc) {
        $this->assoc = $assoc;
    }

    /**
     * @return UserFriends
     */
    public function getFriends() : UserFriends {
        return $this->friends;
    }

    /**
     * @return \alphayax\freebox\os\models\User\UserConfig
     */
    public function getConfig() : UserConfig {
        return $this->config;
    }

    /**
     * @return int
     */
    public function getTokenExpiration()
    {
        return $this->token_expiration;
    }

    /**
     * @param int $token_expiration
     */
    public function setTokenExpiration( $token_expiration)
    {
        $this->token_expiration = $token_expiration;
    }

}

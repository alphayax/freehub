<?php
namespace alphayax\freebox\os\models\User;
use alphayax\freebox\os\utils\Model;
use MongoDB\Model\BSONArray;

/**
 * Class UserFriends
 * @package alphayax\freebox\os\models\User
 */
class UserFriends extends Model {

    /** @var string[] */
    protected $confirmed = [];

    /** @var string[] */
    protected $requests = [];

    /** @var string */
    protected $friendship_key = '';

    public function __construct() {
        $this->generateNewFriendshipKey();
    }

    /**
     * @param array $data
     */
    public function init( array $data = []){
        foreach ( $data as $propertyName => $propertyValue){
            if( property_exists( static::class, $propertyName)){
                // Arrays
                if( $propertyValue instanceof BSONArray){
                    $this->$propertyName = $propertyValue->getArrayCopy();
                    continue;
                }
                // Common case
                $this->$propertyName = $propertyValue;
            }
        }
    }

    /**
     * @return \string[]
     */
    public function getConfirmed() {
        return $this->confirmed;
    }

    /**
     * @return \string[]
     */
    public function getRequests() {
        return $this->requests;
    }

    /**
     * @return string
     */
    public function getFriendshipKey() {
        return $this->friendship_key;
    }

    /**
     * Generate a new friendship key
     */
    public function generateNewFriendshipKey(){
        $this->friendship_key = strtoupper( str_replace( '.', '', uniqid( '', true)));
    }

    /**
     * @param $uid
     */
    public function addFriend( $uid) {
        // Bad fix. Sometimes, the array is null...
        if( null === $this->confirmed){
            $this->confirmed = [];
        }
        array_push( $this->confirmed, $uid);
        $this->confirmed = array_unique( $this->confirmed);
    }

    /**
     * @param $uid
     */
    public function removeFriend( $uid) {
        // Bad fix. Sometimes, the array is null...
        if( null === $this->confirmed){
            $this->confirmed = [];
        }
        $this->confirmed = array_diff( $this->confirmed, [$uid]);
    }

    /**
     * @param $friendRequest
     */
    public function addFriendRequest( $friendRequest) {
        // Bad fix. Sometimes, the array is null...
        if( null === $this->requests){
            $this->requests = [];
        }
        array_push( $this->requests, $friendRequest);
        $this->requests = array_unique( $this->requests);
    }

    /**
     * @param $friendRequest
     */
    public function removeFriendRequest( $friendRequest) {
        // Bad fix. Sometimes, the array is null...
        if( null === $this->requests){
            $this->requests = [];
        }
        $this->requests = array_diff( $this->requests, [$friendRequest]);
    }

}

<?php
namespace alphayax\freebox\os\models;
use alphayax\freebox\os\utils;

/**
 * Class Movie
 * @package alphayax\freebox\os\models
 */
class Movie extends utils\Model {

    /** @var string */
    protected $backdrop;

    /** @var string */
    protected $characters;

    /** @var string */
    protected $comments;

    /** @var string */
    protected $director;

    /** @var string */
    protected $followers;

    /** @var Object */
    protected $genres;

    /** @var int */
    protected $id;

    /** @var string */
    protected $imdb_id;

    /** @var string */
    protected $language;

    /** @var string */
    protected $length;

    /** @var Object */
    protected $notes;

    /** @var string */
    protected $original_title;

    /** @var string */
    protected $poster;

    /** @var string */
    protected $production_year;

    /** @var string */
    protected $release_date;

    /** @var string */
    protected $sale_date;

    /** @var string */
    protected $similars;

    /** @var string */
    protected $synopsis;

    /** @var string */
    protected $title;

    /** @var int */
    protected $tmdb_id;

    /** @var string */
    protected $url;

    /** @var Object */
    protected $user;

    /**
     * @return string
     */
    public function getBackdrop(): string
    {
        return $this->backdrop;
    }

    /**
     * @param string $backdrop
     */
    public function setBackdrop($backdrop)
    {
        $this->backdrop = $backdrop;
    }

    /**
     * @return string
     */
    public function getCharacters(): string
    {
        return $this->characters;
    }

    /**
     * @param string $characters
     */
    public function setCharacters($characters)
    {
        $this->characters = $characters;
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function getDirector(): string
    {
        return $this->director;
    }

    /**
     * @param string $director
     */
    public function setDirector($director)
    {
        $this->director = $director;
    }

    /**
     * @return string
     */
    public function getFollowers(): string
    {
        return $this->followers;
    }

    /**
     * @param string $followers
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;
    }

    /**
     * @return Object
     */
    public function getGenres(): Object
    {
        return $this->genres;
    }

    /**
     * @param Object $genres
     */
    public function setGenres($genres)
    {
        $this->genres = $genres;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdb_id;
    }

    /**
     * @param string $imdb_id
     */
    public function setImdbId($imdb_id)
    {
        $this->imdb_id = $imdb_id;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLength(): string
    {
        return $this->length;
    }

    /**
     * @param string $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return Object
     */
    public function getNotes(): Object
    {
        return $this->notes;
    }

    /**
     * @param Object $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getOriginalTitle(): string
    {
        return $this->original_title;
    }

    /**
     * @param string $original_title
     */
    public function setOriginalTitle($original_title)
    {
        $this->original_title = $original_title;
    }

    /**
     * @return string
     */
    public function getPoster(): string
    {
        return $this->poster;
    }

    /**
     * @param string $poster
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;
    }

    /**
     * @return string
     */
    public function getProductionYear(): string
    {
        return $this->production_year;
    }

    /**
     * @param string $production_year
     */
    public function setProductionYear($production_year)
    {
        $this->production_year = $production_year;
    }

    /**
     * @return string
     */
    public function getReleaseDate(): string
    {
        return $this->release_date;
    }

    /**
     * @param string $release_date
     */
    public function setReleaseDate($release_date)
    {
        $this->release_date = $release_date;
    }

    /**
     * @return string
     */
    public function getSaleDate(): string
    {
        return $this->sale_date;
    }

    /**
     * @param string $sale_date
     */
    public function setSaleDate($sale_date)
    {
        $this->sale_date = $sale_date;
    }

    /**
     * @return string
     */
    public function getSimilars(): string
    {
        return $this->similars;
    }

    /**
     * @param string $similars
     */
    public function setSimilars($similars)
    {
        $this->similars = $similars;
    }

    /**
     * @return string
     */
    public function getSynopsis(): string
    {
        return $this->synopsis;
    }

    /**
     * @param string $synopsis
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getTmdbId(): int
    {
        return $this->tmdb_id;
    }

    /**
     * @param int $tmdb_id
     */
    public function setTmdbId($tmdb_id)
    {
        $this->tmdb_id = $tmdb_id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return Object
     */
    public function getUser(): Object
    {
        return $this->user;
    }

    /**
     * @param Object $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}

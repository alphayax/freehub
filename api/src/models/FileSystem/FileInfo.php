<?php
namespace alphayax\freebox\os\models\FileSystem;
use alphayax\freebox\os\utils\Model;
use alphayax\freebox\os\utils\MovieTitle;

/**
 * Class FileInfo
 * @package alphayax\freebox\os\models\FileSystem
 */
class FileInfo extends Model {

    /** @var \alphayax\freebox\api\v3\models\FileSystem\FileInfo  */
    protected $fileInfo;

    /** @var \alphayax\freebox\os\utils\MovieTitle */
    protected $movieTitle;

    /** @var string */
    protected $image = '';

    protected $path;
    protected $name;


    /**
     * @param \alphayax\freebox\api\v3\models\FileSystem\FileInfo $fileInfo
     */
    public function init( \alphayax\freebox\api\v3\models\FileSystem\FileInfo $fileInfo) {
        $this->fileInfo = $fileInfo;
        $this->name     = $this->fileInfo->getName();
        $this->path     = $this->fileInfo->getPath();

        // Videos
        if( $this->isVideo()){
            $this->movieTitle = new MovieTitle( $this->fileInfo->getName());
            $this->name  = $this->movieTitle->getCleanName();
        }
    }

    /**
     * Return true if the mimeType start with "video"
     * @return bool
     */
    protected function isVideo() {
        $mimeType = $this->fileInfo->getMimetype();
        $type = explode( '/', $mimeType);
        return $type[0] == 'video';
    }

}

<?php

namespace alphayax\freebox\os\exception;

/**
 * Class MissingHeaderException
 * @package alphayax\freebox\os\exception
 */
class MissingHeaderException extends Exception
{

}

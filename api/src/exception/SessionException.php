<?php

namespace alphayax\freebox\os\exception;

/**
 * Class SessionException
 * @package alphayax\freebox\os\exception
 */
class SessionException extends Exception
{

}

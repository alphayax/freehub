<?php

namespace alphayax\freebox\os\exception\db;

use alphayax\freebox\os\exception\Exception;

/**
 * Class DuplicateEntryException
 * @package alphayax\freebox\os\exception\db
 */
class DuplicateEntryException extends Exception
{

}

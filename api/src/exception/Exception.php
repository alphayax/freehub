<?php

namespace alphayax\freebox\os\exception;

/**
 * Class Exception
 * Base exception class for project
 * @package alphayax\freebox\os\exception
 */
class Exception extends \Exception
{

}

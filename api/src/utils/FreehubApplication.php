<?php
namespace alphayax\freebox\os\utils;
use \alphayax\freebox;

/**
 * Class FreehubApplication
 * @package alphayax\freebox\os\utils
 */
class FreehubApplication {
    use Singleton;

    const APP_ID        = 'com.alphayax.freebox.os';
    const APP_NAME      = 'Freehub';
    const APP_VERSION   = '0.2.0';

    /** @var freebox\utils\Application */
    protected $application;

    /**
     * FreeboxOsApplication constructor.
     */
    protected function __construct() {
        $this->application = new freebox\utils\Application( static::APP_ID, static::APP_NAME, static::APP_VERSION);
    }

    /**
     * @return freebox\utils\Application
     */
    public function getApplication(): freebox\utils\Application
    {
        return $this->application;
    }

    /**
     * @param \alphayax\freebox\os\models\FreeboxAssoc $userAssoc
     * @return \alphayax\freebox\utils\Application
     * @throws \Exception
     */
    public function getApplicationWithAssoc( freebox\os\models\FreeboxAssoc $userAssoc): freebox\utils\Application
    {
        $this->application->setAppToken( $userAssoc->getAppToken());
        $this->application->setFreeboxApiHost( $userAssoc->getHost());
        $this->application->openSession();

        return $this->application;
    }

}

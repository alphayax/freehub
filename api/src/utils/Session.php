<?php

namespace alphayax\freebox\os\utils;

use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\exception\MissingHeaderException;
use alphayax\freebox\os\exception\SessionException;

/**
 * Class Session
 * @package alphayax\freebox\os\utils
 */
class Session
{
    /**
     * Check the validity of the session
     * If a PHP session il already defined, we use it
     * If not, check the token validity (via the "X-Freehub-Token" header), and initialize the session
     * @throws MissingHeaderException
     * @throws \alphayax\freebox\os\exception\SessionException
     */
    public static function check() {

        // If session is active, nothing to do
        if( ! empty( $_SESSION)){
            return;
        }

        // No session, we'll try to authenticate the user
        if( ! array_key_exists('HTTP_X_FREEHUB_TOKEN', $_SERVER)){
            throw new MissingHeaderException('The X-Freehub-Token header is missing '. print_r( $_SERVER, true));
        }

        $token = @$_SERVER['HTTP_X_FREEHUB_TOKEN'];
        if( empty( $token)) {
            // SessionUser will not be initialized
            return;
        }

        try {
            $user = User_Collection::getFromToken( $token);
        } catch (\Exception $e) {
            throw new SessionException('No user found with specified token', 1401, $e);
        }

        SessionUser::init( $user);
    }

}

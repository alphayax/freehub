<?php
namespace alphayax\freebox\os\utils\BetaSeries\services;
use alphayax\freebox\os\utils\BetaSeries\BetaSeries_Rest;

/**
 * Class TvShow
 * @package alphayax\freebox\os\utils\BetaSeries\services
 */
class Movies {

    /**
     * @param string $title
     * @return array
     */
    public static function search( $title) {

        $route =  'movies/search?';

        $param = http_build_query([
            'title' => $title,
        ]);

        $rest = new BetaSeries_Rest( $route . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();

        $Movies = [];
        foreach( $rez['movies'] as $show){
            $Movies[] = $show;
        }

        return $Movies;
    }

    /**
     * @param $MovieId
     * @return mixed
     */
    public static function getPictureFromId( $MovieId) {
        $route = 'pictures/movies?';
        $param = http_build_query([
            'id' => $MovieId,
        ]);

        $rest = new BetaSeries_Rest( $route . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();

        return $rez;
    }

}

<?php
namespace alphayax\freebox\os\utils\BetaSeries\services;
use alphayax\freebox\os\utils\BetaSeries;

/**
 * Class TvShow
 * @package alphayax\freebox\os\utils\BetaSeries\services
 */
class TvShow {

    /**
     * @param string $title
     * @return \alphayax\freebox\os\utils\BetaSeries\models\TvShow\TvShow[]
     */
    public static function search( $title) {
        trigger_error( 'NOTICE : BetaSeries API Call for TvShow '. $title);

        $route = 'shows/search?';
        $param = http_build_query([
            'title' => $title,
        ]);

        $rest = new BetaSeries\BetaSeries_Rest( $route . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();

        $TvShows = [];
        foreach( $rez['shows'] as $show_x){
            $show = new BetaSeries\models\TvShow\TvShow();
            $show->jsonUnserialize( $show_x);
            $TvShows[] = $show;
        }

        return $TvShows;
    }

    /**
     * @param $TheTvDb_id
     * @return \alphayax\freebox\os\utils\BetaSeries\models\TvShow\TvShow
     * @throws \alphayax\freebox\os\utils\BetaSeries\exception\BetaSeriesException
     */
    public static function getFromTheTvDbId( $TheTvDb_id) {

        $route = 'shows/display?';
        $param = http_build_query([
            'thetvdb_id' => $TheTvDb_id,
        ]);

        $rest = new BetaSeries\BetaSeries_Rest( $route . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();

        $show = new BetaSeries\models\TvShow\TvShow();
        $show->jsonUnserialize( $rez['show']);

        return $show;
    }

    /**
     * @experimental Not used at this time
     * @param $TvShowId
     * @return mixed
     */
    public static function getPictureFromId( $TvShowId) {
        $route = 'shows/pictures?';
        $param = http_build_query([
            'id' => $TvShowId,
        ]);

        $rest = new BetaSeries\BetaSeries_Rest( $route . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();

        return $rez;
    }

}

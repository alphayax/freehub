<?php
namespace alphayax\freebox\os\utils\BetaSeries\models\TvShow;
use alphayax\freebox\os\utils;
use alphayax\freebox\os\utils\BetaSeries\models\Model;


/**
 * Class TvShow
 * @package alphayax\freebox\os\models
 */
class TvShow extends Model {

    /** @var int */
    protected $id;

    /** @var int */
    protected $thetvdb_id;

    /** @var string */
    protected $imdb_id;

    /** @var string */
    protected $title;

    /** @var string */
    protected $description;

    /** @var string */
    protected $seasons;

    /** @var \MongoDB\Model\BSONArray */
    protected $seasons_details = [];

    /** @var string */
    protected $episodes;

    /** @var string */
    protected $followers;

    /** @var string */
    protected $comments;

    /** @var string */
    protected $similars;

    /** @var string */
    protected $characters;

    /** @var string */
    protected $creation;

    /** @var \MongoDB\Model\BSONArray */
    protected $genres = [];

    /** @var string */
    protected $length;

    /** @var string */
    protected $network;

    /** @var string */
    protected $rating;

    /** @var string */
    protected $status;

    /** @var string */
    protected $language;

    /** @var \MongoDB\Model\BSONDocument */
    protected $notes;

    /** @var boolean */
    protected $in_account;

    /** @var Images */
    protected $images;

    /** @var \MongoDB\Model\BSONDocument */
    protected $aliases;

    /** @var \MongoDB\Model\BSONDocument */
    protected $user;

    /** @var string */
    protected $resource_url;

    /**
     * @inheritdoc
     * @param array $data_x
     */
    public function jsonUnserialize( array $data_x) {
        parent::jsonUnserialize( $data_x);
        $this->images = new Images();
        $this->images->jsonUnserialize( $data_x['images']);
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getThetvdbId() {
        return $this->thetvdb_id;
    }

    /**
     * @return string
     */
    public function getImdbId() {
        return $this->imdb_id;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getSeasons() {
        return $this->seasons;
    }

    /**
     * @return \MongoDB\Model\BSONArray
     */
    public function getSeasonsDetails() {
        return $this->seasons_details;
    }

    /**
     * @return string
     */
    public function getEpisodes() {
        return $this->episodes;
    }

    /**
     * @return string
     */
    public function getFollowers() {
        return $this->followers;
    }

    /**
     * @return string
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * @return string
     */
    public function getSimilars() {
        return $this->similars;
    }

    /**
     * @return string
     */
    public function getCharacters() {
        return $this->characters;
    }

    /**
     * @return string
     */
    public function getCreation() {
        return $this->creation;
    }

    /**
     * @return \MongoDB\Model\BSONArray
     */
    public function getGenres() {
        return $this->genres;
    }

    /**
     * @return string
     */
    public function getLength() {
        return $this->length;
    }

    /**
     * @return string
     */
    public function getNetwork() {
        return $this->network;
    }

    /**
     * @return string
     */
    public function getRating() {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @return \MongoDB\Model\BSONDocument
     */
    public function getNotes() {
        return $this->notes;
    }

    /**
     * @return bool
     */
    public function isInAccount() {
        return $this->in_account;
    }

    /**
     * @return Images
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * @return \MongoDB\Model\BSONDocument
     */
    public function getAliases() {
        return $this->aliases;
    }

    /**
     * @param $alias
     */
    public function addAlias( $alias) {
        $this->aliases[] = $alias;
    }

    /**
     * @return \MongoDB\Model\BSONDocument
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getResourceUrl() {
        return $this->resource_url;
    }

}

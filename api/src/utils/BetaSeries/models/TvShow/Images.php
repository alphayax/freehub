<?php
namespace alphayax\freebox\os\utils\BetaSeries\models\TvShow;
use alphayax\freebox\os\utils;
use alphayax\freebox\os\utils\BetaSeries\models\Model;

/**
 * Class TvShow
 * @package alphayax\freebox\os\models
 */
class Images extends Model {

    /** @var string */
    protected $show;

    /** @var string */
    protected $banner;

    /** @var string */
    protected $box;

    /** @var string */
    protected $poster;

    /**
     * @return string
     */
    public function getShow() {
        return $this->show;
    }

    /**
     * @return string
     */
    public function getBanner() {
        return $this->banner;
    }

    /**
     * @return string
     */
    public function getBox() {
        return $this->box;
    }

    /**
     * @return string
     */
    public function getPoster() {
        return $this->poster;
    }

    /**
     * @return string
     */
    public function hasShow() {
        return ! empty( $this->show);
    }

    /**
     * @return string
     */
    public function hasBanner() {
        return ! empty( $this->banner);
    }

    /**
     * @return string
     */
    public function hasBox() {
        return ! empty( $this->box);
    }

    /**
     * @return string
     */
    public function hasPoster() {
        return ! empty( $this->poster);
    }

}

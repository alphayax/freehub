<?php
namespace alphayax\freebox\os\utils\BetaSeries\models;
use MongoDB\BSON\Persistable;
use alphayax\freebox\os\utils;

abstract class Model implements \JsonSerializable, Persistable {
    use utils\MongoPersistable;
    use utils\JsonPersistable;
}

<?php
namespace alphayax\freebox\os\utils\BetaSeries;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\freebox\os\utils\BetaSeries\exception\BetaSeriesException;
use alphayax\rest\Rest;

/**
 * Class BetaSeries_Rest
 * @package alphayax\freebox\os\utils\BetaSeries
 */
class BetaSeries_Rest extends Rest {

    /** @var string */
    protected static $_api_host;

    /** @var string */
    protected static $_api_version;

    /** @var string */
    protected static $_api_key;

    /**
     * BetaSeries_Rest constructor.
     * @param string $route
     */
    public function __construct( string $route) {
        $this->checkConfig();
        parent::__construct( static::$_api_host . $route);

        $this->addHeader( 'X-BetaSeries-Key'    , static::$_api_key);
        $this->addHeader( 'X-BetaSeries-Version', static::$_api_version);
        $this->addHeader( 'User-Agent', 'Freehub');
        $this->addHeader( 'Accept', 'application/json');
    }

    /**
     * Check configuration, and cache it
     * @throws \Exception
     */
    private function checkConfig() {

        /// Check config cache
        if( ! empty( static::$_api_key) && ! empty( static::$_api_version) && ! empty( static::$_api_host)){
            return;
        }

        /// Configuration check
        $betaSeries_x = FreehubConfig::getInstance()->getBetaSeriesConfig();
        if( ! array_key_exists( 'api', $betaSeries_x) || ! array_key_exists( 'key', $betaSeries_x['api']) || ! array_key_exists( 'version', $betaSeries_x['api'])){
            throw new \Exception( 'beta_series api information is not set');
        }

        /// Cache config
        static::$_api_host      = $betaSeries_x['api']['endpoint'];
        static::$_api_key       = $betaSeries_x['api']['key'];
        static::$_api_version   = $betaSeries_x['api']['version'];
    }

    /**
     * @inheritdoc
     * @throws \alphayax\freebox\os\utils\BetaSeries\exception\BetaSeriesException
     */
    protected function exec() {
        parent::exec();
        if( empty( $this->curlResponse)){
            throw new BetaSeriesException( 'Empty response from API');
        }

        if( array_key_exists( 'errors', $this->curlResponse) && ! empty( $this->curlResponse['errors'])){
            $err_x = $this->curlResponse['errors'][0];
            throw new BetaSeriesException( 'API BetaSeries : '. $err_x['text'], $err_x['code']);
        }
    }

}


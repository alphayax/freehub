<?php
namespace alphayax\freebox\os\utils\TheTvDb;
use alphayax\freebox\os\etc\FreehubConfig;
use alphayax\rest\Rest;

/**
 * Class BetaSeries_Rest
 * @package alphayax\freebox\os\utils\BetaSeries
 */
class TheTvDb_Rest extends Rest {

    // TODO : Use the value in config instead of a constant
    const API_HOST = 'https://api.thetvdb.com/';

    /** @var string */
    protected static $_api_token;

    /** @var bool Is the first attempt to try to auth */
    static $_is_retry_auth = false;

    /**
     *
     */
    public static function login() {
        $theTvDb_x = FreehubConfig::getInstance()->getTheTvDbConfig();

        $service_url = $theTvDb_x['api']['endpoint'] . 'login';
        $rest = new Rest( $service_url);
        $rest->addHeader( 'User-Agent'  , 'Freehub');
        $rest->addHeader( 'Content-Type', 'application/json');
        $rest->addHeader( 'Accept'      , 'application/json');
        $rest->POST( [
            "apikey"    => $theTvDb_x['api']['key'],
            "username"  => $theTvDb_x['user']['name'],
            "userkey"   => $theTvDb_x['user']['key'],
        ]);

        static::$_api_token = $rest->getCurlResponse()['token'];

        // TODO : Use memcached or any thing else (database) to save this token
        FreehubConfig::getInstance()->saveTheTvDbToken( static::$_api_token);
    }

    /**
     * BetaSeries_Rest constructor.
     * @param string $url
     * @throws \Exception
     */
    public function __construct( $url) {
        $this->checkConfig();

        parent::__construct( $url);

        $this->addHeader( 'Authorization'   , 'Bearer '. static::$_api_token);
        $this->addHeader( 'User-Agent'      , 'Freehub');
        $this->addHeader( 'Content-Type'    , 'application/json');
        $this->addHeader( 'Accept'          , 'application/json');
    }

    /**
     * Check configuration, and cache it
     * @throws \Exception
     */
    private function checkConfig() {

        /// Configuration check
        $theTvDb_x = FreehubConfig::getInstance()->getTheTvDbConfig();
        if( empty( $theTvDb_x)){
            throw new \Exception( 'The tvDb config is not set');
        }

        $apikey    = @$theTvDb_x['api']['key'];
        $username  = @$theTvDb_x['user']['name'];
        $userkey   = @$theTvDb_x['user']['key'];

        if( empty( $apikey)|| empty( $username)|| empty( $userkey)){
            throw new \Exception( 'the_tv_db api information is missing in "freehub.json"');
        }

        /// Cache config
        static::$_api_token = @$theTvDb_x['api']['token']; // TODO : Use memcached or db

        /// Check config cache
        if( empty( static::$_api_token)){
            self::login();
        }
    }

    /**
     *
     */
    protected function exec(){
        parent::exec();
        $http_code = intval( $this->getHttpCode());
        if( $http_code == 401){
            if( static::$_is_retry_auth){
                throw new \Exception( 'Unable to authenticate after 2 attempts');
            }
            static::$_is_retry_auth = true;
            self::login();
        }
    }

}


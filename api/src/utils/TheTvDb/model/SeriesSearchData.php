<?php
namespace alphayax\freebox\os\utils\TheTvDb\model;
use alphayax\freebox\os\utils\JsonPersistable;

/**
 * Class SeriesSearchData
 * @package alphayax\freebox\os\utils\TheTvDb\model
 */
class SeriesSearchData implements \JsonSerializable {
    use JsonPersistable;

    /** @var string[] optional */
    protected $aliases = [];

    /** @var string optional */
    protected $banner = '';

    /** @var string optional */
    protected $firstAired = '';

    /** @var integer optional */
    protected $id;

    /** @var string optional */
    protected $network = '';

    /** @var string optional */
    protected $overview = '';

    /** @var string optional */
    protected $seriesName = '';

    /** @var string optional */
    protected $status = '';

    /**
     * @return \string[]
     */
    public function getAliases() {
        return $this->aliases;
    }

    /**
     * @return string
     */
    public function getBanner() {
        return $this->banner;
    }

    /**
     * @return string
     */
    public function getFirstAired() {
        return $this->firstAired;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNetwork() {
        return $this->network;
    }

    /**
     * @return string
     */
    public function getOverview() {
        return $this->overview;
    }

    /**
     * @return string
     */
    public function getSeriesName() {
        return $this->seriesName;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

}

<?php
namespace alphayax\freebox\os\utils\TheTvDb\services;
use alphayax\freebox\os\utils\TheTvDb;

/**
 * Class TvShow
 * @package alphayax\freebox\os\utils\TheTvDb\services
 */
class TvShow {

    /**
     * @param string $title
     * @return \alphayax\freebox\os\utils\TheTvDb\model\SeriesSearchData[]
     * @throws \alphayax\freebox\os\utils\TheTvDb\exception\TvDbException
     * @throws TheTvDb\exception\TvDbException
     */
    public static function search( string $title) : array {
        $url = TheTvDb\TheTvDb_Rest::API_HOST . 'search/series?';

        $param = http_build_query([
            'name' => $title,
        ]);

        $rest = new TheTvDb\TheTvDb_Rest( $url . $param);
        $rest->GET();

        $rez = $rest->getCurlResponse();
        if( empty( $rez)){
            throw new TheTvDb\exception\TvDbException( 'Empty result from API');
        }

        if( ! array_key_exists( 'data', $rez)){
            throw new TheTvDb\exception\TvDbException( 'Malformed result from API');
        }

        if( empty( $rez['data'])){
            throw new TheTvDb\exception\TvDbException( 'No results for title : '. $title);
        }

        $TvShows = [];
        foreach( $rez['data'] as $show_x){
            $show = new TheTvDb\model\SeriesSearchData();
            $show->jsonUnserialize( $show_x);
            $TvShows[] = $show;
        }

        return $TvShows;
    }

}

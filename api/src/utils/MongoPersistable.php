<?php
namespace alphayax\freebox\os\utils;
use MongoDB\Model\BSONArray;

/**
 * Trait MongoPersistable
 * @package alphayax\freebox\os\utils
 */
trait MongoPersistable {

    /**
     * Provides an array or document to serialize as BSON
     * Called during serialization of the object to BSON. The method must return an array or stdClass.
     * Root documents (e.g. a MongoDB\BSON\Serializable passed to MongoDB\BSON\fromPHP()) will always be serialized as a BSON document.
     * For field values, associative arrays and stdClass instances will be serialized as a BSON document and sequential arrays (i.e. sequential, numeric indexes starting at 0) will be serialized as a BSON array.
     * @link http://php.net/manual/en/mongodb-bson-serializable.bsonserialize.php
     * @return array|object An array or stdClass to be serialized as a BSON array or document.
     */
    public function bsonSerialize() {
        $bson = [];
        $properties = get_object_vars( $this);
        foreach( $properties as $propertyName => $propertyValue) {
            $bson[$propertyName] = $propertyValue;
        }
        return $bson;
    }

    /**
     * Constructs the object from a BSON array or document
     * Called during unserialization of the object from BSON.
     * The properties of the BSON array or document will be passed to the method as an array.
     * @link http://php.net/manual/en/mongodb-bson-unserializable.bsonunserialize.php
     * @param array $data Properties within the BSON array or document.
     */
    public function bsonUnserialize( array $data) {
        foreach( $data as $propertyName => $propertyValue){
            if( property_exists( static::class, $propertyName)){
                if( $propertyValue instanceof BSONArray){
                    $this->$propertyName = (array) $propertyValue;
                } else {
                    $this->$propertyName = $propertyValue;
                }
            }
        }
    }

}

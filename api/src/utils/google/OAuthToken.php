<?php

namespace alphayax\freebox\os\utils\google;

use alphayax\freebox\os\etc\FreehubConfig;
use Firebase\JWT\JWT;

/**
 * Class GoogleOAuthToken
 * @package utils
 */
class OAuthToken
{
    /**
     * @param $token
     * @return object
     */
    public static function decode($token)
    {
        $pubKeys = static::getPublicKeys();
        $decodedToken = JWT::decode($token, $pubKeys, ['RS256']);
        static::checkValidity( $decodedToken);
        return $decodedToken;
    }

    /**
     * @return mixed
     * @throws OAuthException
     */
    private static function getPublicKeys()
    {
        $firebaseConfig_x = FreehubConfig::getInstance()->getFirebaseConfig();
        // TODO : optim
        $pubKeys_JSON = file_get_contents($firebaseConfig_x['auth']['public_keys_url']);
        if (empty($pubKeys_JSON)) {
            throw new OAuthException('Unable to get the Google public keys');
        }

        $pubKeys = json_decode($pubKeys_JSON, true);
        if (empty($pubKeys_JSON)) {
            throw new OAuthException('Unable to decode the Google public keys');
        }

        return $pubKeys;
    }

    /**
     * @param $decodedToken
     * @throws OAuthException
     */
    private static function checkValidity( $decodedToken)
    {
        $firebaseConfig_x = FreehubConfig::getInstance()->getFirebaseConfig();
        $now = time();
        if ($now > $decodedToken->exp) {
            throw new OAuthException('Token expired');
        }

        if ($now < $decodedToken->iat) {
            throw new OAuthException('Token issue date in the future');
        }

        if ($firebaseConfig_x['auth']['audience'] !== $decodedToken->aud) {
            throw new OAuthException('Token invalid audience : ' . $decodedToken->aud);
        }

        if ($firebaseConfig_x['auth']['issuer'] !== $decodedToken->iss) {
            throw new OAuthException('Token invalid issuer');
        }

        if (empty($decodedToken->sub)) {
            throw new OAuthException('Token invalid subject');
        }
    }

}

<?php
namespace alphayax\freebox\os\utils;
use alphayax\freebox\os\db\User_Collection;
use alphayax\freebox\os\models\FreeboxAssoc;
use alphayax\freebox\os\models\User\User;

/**
 * Class User
 * @package alphayax\freebox\os\utils
 */
class SessionUser {

    /**
     * Return the current user identifier
     * @return string
     * @throws \Exception
     */
    public static function getUid() {
        $uid = static::getUser()->getUid();
        if( empty( $uid)){
            throw new \Exception( 'Uid not found on user');
        }
        return $uid;
    }

    /**
     * @return \alphayax\freebox\os\models\User\User
     * @throws \Exception
     */
    public static function getUser() {
        $user = @$_SESSION['user'];
        if( empty( $user)){
            throw new \Exception( 'No user in session');
        }
        return $user;
    }

    /**
     * Return the user assoc config
     * @return \alphayax\freebox\os\models\FreeboxAssoc
     * @throws \Exception
     */
    public static function getAssoc() : FreeboxAssoc {
        return static::getUser()->getAssoc();
    }

    /**
     * Update session user into database
     */
    public static function save(){
        User_Collection::update( $_SESSION['user']);
    }

    /**
     * @param \alphayax\freebox\os\models\User\User $user
     */
    public static function init( User $user){
        $_SESSION['user'] = $user;
    }

    /**
     * Check if the session has been initialized
     */
    public static function isInitialized() {
        return ! empty( $_SESSION['user']);
    }

}

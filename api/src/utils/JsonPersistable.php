<?php
namespace alphayax\freebox\os\utils;

/**
 * Trait JsonPersistable
 * @package alphayax\freebox\os\utils
 */
trait JsonPersistable {

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return $this->toArray( $this);
    }

    /**
     * @param $object
     * @return array
     */
    private function toArray($object) : array {
        $json = [];
        $properties = get_object_vars( $object);
        foreach( $properties as $propertyName => $propertyValue) {
            if( is_object( $propertyValue)){
                if( method_exists( $propertyValue, 'jsonSerialize')){
                    $json[$propertyName] = $propertyValue->jsonSerialize();
                } else {
                    $json[$propertyName] = $this->toArray( $propertyValue);
                }
            } else {
                $json[$propertyName] = $propertyValue;
            }
        }
        return $json;
    }

    /**
     * Specify how to decode an array from an unserialized Json string
     * @param array $data_x
     * @throws \Exception
     */
    public function jsonUnserialize( array $data_x) {
        foreach( $data_x as $propertyName => $propertyValue){

            /// If the property does not exist, skip it
            if( ! property_exists( static::class, $propertyName)) {
                continue;
            }

            $this->$propertyName = $propertyValue;
        }
    }

}

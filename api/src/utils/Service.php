<?php
namespace alphayax\freebox\os\utils;
use alphayax\freebox\utils\Application;

/**
 * Class Service
 * @package alphayax\freebox\os\utils
 */
abstract class Service {

    /** @var \alphayax\freebox\utils\Application */
    protected $application;

    /** @var \alphayax\freebox\os\utils\ApiResponse */
    protected $apiResponse;

    /** @var mixed */
    protected $apiRequest;

    /**
     * ApiResponse constructor.
     * @param \alphayax\freebox\utils\Application $application
     */
    public function __construct( Application $application){
        $this->application = $application;
        $this->apiResponse = new ApiResponse();
        $this->apiRequest  = json_decode( file_get_contents('php://input'), true);
    }

    /**
     * @param $action
     * @throws \alphayax\freebox\Exception\FreeboxApiException
     */
    abstract public function executeAction( $action);

    /**
     * @param $action
     */
    protected function actionNotFound( $action){
        $this->apiResponse->setSuccess( false);
        $this->apiResponse->setErrorMessage( "Unknown action ($action)");
    }

    /**
     * @return \alphayax\freebox\os\utils\ApiResponse
     */
    public function getApiResponse() {
        return $this->apiResponse;
    }

}

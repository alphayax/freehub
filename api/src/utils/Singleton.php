<?php
namespace alphayax\freebox\os\utils;

/**
 * Class Singleton
 * @package alphayax\freebox\os\utils
 */
trait Singleton {

    /**
     * @var static
     */
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance() {
        if( empty( static::$instance)){
            static::$instance = new static();
        }
        return static::$instance;
    }

}

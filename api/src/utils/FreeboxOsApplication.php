<?php
namespace alphayax\freebox\os\utils;
use \alphayax\freebox;

/**
 * Class FreeboxOsApplication
 * @package alphayax\freebox\os\utils
 */
class FreeboxOsApplication {

    const APP_ID        = 'com.alphayax.freebox.os';
    const APP_NAME      = 'Freehub';
    const APP_VERSION   = '0.1.0';

    /** @var freebox\utils\Application */
    protected $application;


    /**
     * FreeboxOsApplication constructor.
     */
    public function __construct() {
        $this->application = new freebox\utils\Application( static::APP_ID, static::APP_NAME, static::APP_VERSION);
    }

    /**
     * Service Factory
     * @param $service
     * @return Service
     * @throws \Exception
     */
    protected function getService( $service) {
        switch( $service){

        //  case 'download'       : return new freebox\os\services\DownloadService( $this->application);
        //  case 'download_dlrss' : return new freebox\os\services\DlRssService( $this->application);
            case 'filesystem'     : return new freebox\os\services\FileSystemService( $this->application);
            case 'friends'        : return new freebox\os\services\FriendsService( $this->application);
            case 'freebox'        : return new freebox\os\services\FreeboxService( $this->application);
            case 'poster'         : return new freebox\os\services\PosterService( $this->application);
        //    case 'upload'         : return new freebox\os\services\UploadService( $this->application);
            case 'user'           : return new freebox\os\services\UserService( $this->application);
            default : throw new \Exception( 'Unknown service : '. $service);
        }
    }

    /**
     * For API
     */
    public function getAction() {
        $service    = @$_GET['service'];
        $action     = @$_GET['action'];

        try {
            Session::check();
            $service = $this->getService( $service);
            $service->executeAction( $action);
        }
        catch ( freebox\Exception\FreeboxApiException $e){
            $apiResponse = new ApiResponse();
            $apiResponse->setSuccess( false);
            $apiResponse->setErrorMessage( $e->getApiErrorCode() .' : '. $e->getApiMessage());
            return $apiResponse;
        }
        catch( freebox\os\exception\Exception $e){
            $apiResponse = new ApiResponse();
            $apiResponse->setSuccess( false);
            $apiResponse->setErrorMessage( $e->getMessage());
            $apiResponse->setErrorCode( $e->getCode());
            return $apiResponse;
        }
        catch( \Exception $e){
            $apiResponse = new ApiResponse();
            $apiResponse->setSuccess( false);
            $apiResponse->setErrorMessage( $e->getCode() .' : '. $e->getMessage() . ' - '. $e->getTraceAsString());
            return $apiResponse;
        }

        return $service->getApiResponse();
    }

}

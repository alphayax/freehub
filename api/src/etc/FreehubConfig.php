<?php
namespace alphayax\freebox\os\etc;
use alphayax\freebox\os\utils\Singleton;

/**
 * Class FreehubConfig
 * @package alphayax\freebox\os\etc
 * Todo : Explode in classes to get sub Configs (eg: BetaSeriesConfig, TheTvTbConfig...)
 */
class FreehubConfig {
    use Singleton;

    //protected const CONFIG_AFI = __DIR__ . '/freehub.yml'; // PHP 7.1
    const CONFIG_AFI = __DIR__ . '/freehub.yml';

    /** @var array */
    protected $config = [];

    /**
     * FreehubConfig constructor.
     * Load the config file
     */
    protected function __construct(){
        $this->config = \yaml_parse_file( static::CONFIG_AFI);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getMongoDbUri() : string {
        $host = @$this->config['freehub']['mongodb']['host'];
        $port = @$this->config['freehub']['mongodb']['port'] ?: 27017;
        if( empty( $host)){
            throw new \Exception('The mongodb configuration is missing (Host is undefined)');
        }
        return "mongodb://$host:$port";
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStaticEndpoint() : string {
        $staticEndpoint = @$this->config['freehub']['static']['endpoint'];
        if( empty( $staticEndpoint)){
            throw new \Exception( 'Freehub static endpoint is not defined');
        }
        return $staticEndpoint;
    }

    /**
     * @return string
     */
    public function getAllowedOrigin()
    {
        return @$this->config['freehub']['allowedOrigin'] ?: '*';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStaticVolume() : string {
        $staticVolume = @$this->config['freehub']['static']['volume'];
        if( empty( $staticVolume)){
            throw new \Exception( 'Freehub static volume is not defined');
        }
        return $staticVolume;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getBetaSeriesConfig() : array {
        $betaSeries_x = @$this->config['external_apis']['beta_series'];
        if( empty( $betaSeries_x)){
            throw new \Exception( 'Beta series configuration is not set');
        }
        return $betaSeries_x;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getTheTvDbConfig() : array {
        $betaSeries_x = @$this->config['external_apis']['the_tv_db'];
        if( empty( $betaSeries_x)){
            throw new \Exception( 'The TvDb configuration is not set');
        }
        return $betaSeries_x;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getFirebaseConfig() : array {
        $firebase_x = @$this->config['freehub']['firebase'];
        if( empty( $firebase_x)){
            throw new \Exception( 'Firebase config is not defined (freehub > firebase)');
        }
        return $firebase_x;
    }

    /**
     * TO BE DEPRECATED => User memcached or anything else
     * @param string $token
     * @return bool
     */
    public function saveTheTvDbToken( string $token) {
        $this->config['external_apis']['the_tv_db']['api']['token'] = $token;
        return \yaml_emit_file( static::CONFIG_AFI, $this->config);
    }

}

#!/usr/bin/env php
<?php

require_once __DIR__ . '/../vendor/autoload.php';

use alphayax\freebox\os\models;
use alphayax\freebox\os\db;

$user_z = file_get_contents('alphayax.user');
$user_x = json_decode( $user_z, true);

$user = new models\User\User();
$user->init( $user_x);

db\User_Collection::update($user);

echo "User Updated !". PHP_EOL;
var_dump( $user);


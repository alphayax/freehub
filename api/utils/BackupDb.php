#!/usr/bin/env php
<?php

require_once __DIR__ . '/../vendor/autoload.php';

\alphayax\freehub_utils\db\Backup::users();
\alphayax\freehub_utils\db\Backup::tvShows();
\alphayax\freehub_utils\db\Backup::movies();

#!/usr/bin/env php
<?php

require_once __DIR__ . '/../vendor/autoload.php';

\alphayax\freehub_utils\db\Restore::users();
\alphayax\freehub_utils\db\Restore::tvShows();
\alphayax\freehub_utils\db\Restore::movies();

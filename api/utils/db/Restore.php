<?php

namespace alphayax\freehub_utils\db;

use alphayax\freebox\os\db;
use alphayax\freebox\os\exception\db\DuplicateEntryException;
use alphayax\freebox\os\models;

/**
 * Class Restore
 * @package alphayax\freehub_utils
 */
class Restore {

    /**
     *
     */
    public static function users()
    {
        echo PHP_EOL ."-- Users --". PHP_EOL;
        $user_z = @file_get_contents( 'users.json');
        if( empty( $user_z)){
            echo "users.json file not found. Skipping...";
            return;
        }

        $users_xs = json_decode( $user_z, true);
        foreach ($users_xs as $user_x){
            $user = new models\User\User();
            $user->init( $user_x);
            try{
                db\User_Collection::insert( $user);
                echo "Inserted : {$user->getUid()}" . PHP_EOL;
            }
            catch ( DuplicateEntryException $e) {
                echo "Skipping - Already in database : {$user->getUid()}" . PHP_EOL;
            }
        }
        echo PHP_EOL;
    }

    /**
     *
     */
    public static function tvShows()
    {
        echo PHP_EOL ."-- Tv Shows --". PHP_EOL;
        $tv_show_z = @file_get_contents( 'tv_shows.json');
        if( empty( $tv_show_z)){
            echo "tv_shows.json file not found. Skipping...";
            return;
        }

        $tv_show_xs = json_decode( $tv_show_z, true);
        foreach ($tv_show_xs as $tv_show_x){
            $tvShow = new models\TvShow();
            $tvShow->jsonUnserialize( $tv_show_x);

            try{
                db\TvShow_Collection::insert( $tvShow);
                echo "Inserted : {$tvShow->getTitle()}" . PHP_EOL;
            }
            catch ( DuplicateEntryException $e) {
                echo "Skipping - Already in database : {$tvShow->getTitle()}" . PHP_EOL;
            }
        }
        echo PHP_EOL;
    }

    /**
     *
     */
    public static function movies()
    {
        echo PHP_EOL ."-- Movies --". PHP_EOL;
        $movie_z = @file_get_contents( 'movies.json');
        if( empty( $movie_z)){
            echo "movies.json file not found. Skipping...";
            return;
        }

        $movie_xs = json_decode( $movie_z, true);
        foreach ($movie_xs as $movie_x){
            $movie = new models\Movie();
            $movie->jsonUnserialize( $movie_x);

            try{
                db\Movie_Collection::insert( $movie);
                echo "Inserted : {$movie->getTitle()}" . PHP_EOL;
            }
            catch ( DuplicateEntryException $e) {
                echo "Skipping - Already in database : {$movie->getTitle()}" . PHP_EOL;
            }
        }
        echo PHP_EOL;
    }

}

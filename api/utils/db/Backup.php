<?php

namespace alphayax\freehub_utils\db;

use alphayax\freebox\os\db;

/**
 * Class Backup
 * @package alphayax\freehub_utils
 */
class Backup {

    /**
     *
     */
    public static function users()
    {
        $users_o = db\User_Collection::fetch( []);
        $users = json_encode( $users_o, JSON_PRETTY_PRINT);

        file_put_contents( 'users.json', $users);
    }

    /**
     *
     */
    public static function tvShows()
    {
        $tv_shows = db\TvShow_Collection::fetch( []);
        $users = json_encode( $tv_shows, JSON_PRETTY_PRINT);

        file_put_contents( 'tv_shows.json', $users);
    }

    /**
     *
     */
    public static function movies()
    {
        $movies = db\Movie_Collection::fetch( []);
        $users = json_encode( $movies, JSON_PRETTY_PRINT);

        file_put_contents( 'movies.json', $users);
    }

}

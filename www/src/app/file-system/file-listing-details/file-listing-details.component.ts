
import {OnInit, Component, Input} from "@angular/core";
import {FileSystemService} from "../../api/file-system.service";
import {FileInfo} from "../../api/models/file-info";


@Component({
    selector: 'file-listing-details',
    templateUrl : 'file-listing-details.component.html',
    providers : [
        FileSystemService,
    ],
})



export class FileListingDetailsComponent implements OnInit {

    constructor(
        private fileSystemService : FileSystemService,
    ){ }

    @Input()
    files : FileInfo[];

    @Input()
    freeboxId : string;


    ngOnInit(){

    }

    navigate( path){
        this.fileSystemService.navigateTo( this.freeboxId, path);
    }

    playInBrowser( fileInfo : FileInfo){
        this.fileSystemService.playInBrowser( this.freeboxId, fileInfo.path, fileInfo.fileInfo.mimetype);
    }

    playOnFreebox( path){
        this.fileSystemService.playOnFreebox( this.freeboxId, path);
    }

}

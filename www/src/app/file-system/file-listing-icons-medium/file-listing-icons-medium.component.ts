
import {Component, Input} from "@angular/core";
import {FileInfo} from "../../api/models/file-info";


@Component({
    selector    : 'file-listing-icons-medium',
    templateUrl : 'file-listing-icons-medium.component.html',
})



export class FileListingIconsMediumComponent {

    @Input()
    files : FileInfo[];

    @Input()
    freeboxId : string;

}

import {Component, Input, OnInit} from '@angular/core';
import { FileInfo } from "../../api/models/file-info";
import { FileSystemService } from "../../api/file-system.service";
import { FreehubUserService } from "../../shared/freehub-user.service";
import { FileSystemControlService } from "../file-system-control.service";


@Component({
    selector: 'file-info-icon-large',
    templateUrl: 'file-info-icon-large.component.html',
    styleUrls : ['file-info-icon-large.component.css'],
    providers : [
        FileSystemService,
    ],
})

export class FileInfoIconLargeComponent implements OnInit {
    @Input()
    fileInfo: FileInfo;

    @Input()
    freeboxId : string;

    isExternalBox: boolean;

    constructor(
        private freeHubUserService: FreehubUserService,
        private fileSystemControlService : FileSystemControlService,
    ){ }

    ngOnInit(): void {
        this.isExternalBox = (this.freeboxId !== this.freeHubUserService.userStatus.uid);
    }

    navigate(){
        this.fileSystemControlService._onNavigate.next( this.fileInfo);
    }

    playInBrowser() {
        this.fileSystemControlService._onPlayOnBrowser.next( this.fileInfo);
    }

    directDownload(){
        this.fileSystemControlService._onDownloadOnBrowser.next( this.fileInfo);
    }

    boxDownload(){
        this.fileSystemControlService._onDownloadOnFreebox.next( this.fileInfo);
    }

    play(){
        this.fileSystemControlService._onPlayOnFreebox.next( this.fileInfo);
    }

    showInformationModal(){
        this.fileSystemControlService._onInformation.next( this.fileInfo);
    }

}

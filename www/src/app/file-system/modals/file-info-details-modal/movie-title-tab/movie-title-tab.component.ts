import {ViewChild, Component, Input} from "@angular/core";
import {MovieTitle} from "../../../../api/models/movie-title";

@Component({
    selector: 'movie-title-tab',
    templateUrl: 'movie-title-tab.component.html',
})

export class MovieTitleTab {

//    @ViewChild('fileInfoDetails')
//    public fileInfoDetails: ModalDirective;

    @Input()
    movieTitle: MovieTitle;

}

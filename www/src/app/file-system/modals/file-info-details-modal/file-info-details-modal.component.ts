import {ViewChild, Component} from "@angular/core";
import {FileInfo} from "../../../api/models/file-info";
import {FileSystemControlService} from "../../file-system-control.service";

@Component({
    selector: 'file-info-details-modal',
    templateUrl: 'file-info-details-modal.component.html',
})

export class FileInfoDetailsModal {

    @ViewChild('fileInfoDetails')
//    public fileInfoDetails: ModalDirective;

    fileInfo: FileInfo;

    constructor(
        private fileSystemControlService : FileSystemControlService,
    ) { }

    show( fileInfo : FileInfo){
        this.fileInfo = fileInfo;
  //      this.fileInfoDetails.show();
    }

    playInBrowser() {
        this.fileSystemControlService._onPlayOnBrowser.next( this.fileInfo);
    }

    directDownload(){
        this.fileSystemControlService._onDownloadOnBrowser.next( this.fileInfo);
    }

    boxDownload(){
        this.fileSystemControlService._onDownloadOnFreebox.next( this.fileInfo);
    }

    play(){
        this.fileSystemControlService._onPlayOnFreebox.next( this.fileInfo);
    }

}

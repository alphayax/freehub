import { Component, Input} from "@angular/core";
import {FileInfo} from "../../../../api/models/file-info";

@Component({
    selector: 'file-info-tab',
    templateUrl: 'file-info-tab.component.html',
})

export class FileInfoTab {

    @Input()
    fileInfo: FileInfo;

}

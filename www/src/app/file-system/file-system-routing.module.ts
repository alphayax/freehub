import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {FileSystemComponent} from "./file-system.component";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {BoxLinkedGuard} from "../shared/guards/box-linked.guard";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'file-system'           , component: FileSystemComponent, canActivate: [ LoggedInGuard, BoxLinkedGuard ] },
        { path: 'file-system/:uid'      , component: FileSystemComponent, canActivate: [ LoggedInGuard, BoxLinkedGuard ] },
        { path: 'file-system/:uid/:path', component: FileSystemComponent, canActivate: [ LoggedInGuard, BoxLinkedGuard ] },
    ])],
    exports: [RouterModule]
})

export class FileSystemRoutingModule {

}

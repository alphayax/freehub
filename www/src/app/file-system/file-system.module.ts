
import { NgModule } from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {FileSystemRoutingModule} from "./file-system-routing.module";
import {FileInfoIconLargeComponent} from "./file-info-icon-large/file-info-icon-large.component";
import {FileInfoIconMediumComponent} from "./file-info-icon-medium/file-info-icon-medium.component";
import {FileListingDetailsComponent} from "./file-listing-details/file-listing-details.component";
import {FileListingDetailsMovieComponent} from "./file-listing-details-movie/file-listing-details-movie.component";
import {FileListingIconsLargeComponent} from "./file-listing-icons-large/file-listing-icons-large.component";
import {FileListingIconsMediumComponent} from "./file-listing-icons-medium/file-listing-icons-medium.component";
import {FileInfoDetailsModal} from "./modals/file-info-details-modal/file-info-details-modal.component";
import {FileInfoTab} from "./modals/file-info-details-modal/file-info-tab/file-info-tab.component";
import {FileSystemComponent} from "./file-system.component";
import {MovieTitleTab} from "./modals/file-info-details-modal/movie-title-tab/movie-title-tab.component";


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FileSystemRoutingModule,
    ],
    declarations: [
        FileInfoIconLargeComponent,
        FileInfoIconMediumComponent,
        FileListingDetailsComponent,
        FileListingDetailsMovieComponent,
        FileListingIconsLargeComponent,
        FileListingIconsMediumComponent,
        //FileInfoDetailsModal,
        FileInfoTab,
        FileSystemComponent,
        MovieTitleTab,
    ],
    providers: [
    ]
})

export class FileSystemModule {

}

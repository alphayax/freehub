
import {OnInit, Component, Input} from "@angular/core";
import {FileSystemService} from "../../api/file-system.service";
import {FileInfo} from "../../api/models/file-info";


@Component({
    selector    : 'file-listing-details-movie',
    templateUrl : 'file-listing-details-movie.component.html',
    providers : [
        FileSystemService,
    ],
})



export class FileListingDetailsMovieComponent implements OnInit {

    constructor(
        private fileSystemService : FileSystemService,
    ){ }

    @Input()
    files : FileInfo[];

    @Input()
    freeboxId : string;


    ngOnInit(){
        console.log( this.files);
    }

    navigate( path){
        this.fileSystemService.navigateTo( this.freeboxId, path);
    }

    playInBrowser( fileInfo : FileInfo){
        this.fileSystemService.playInBrowser( this.freeboxId, fileInfo.path, fileInfo.fileInfo.mimetype);
    }

    playOnFreebox( path){
        this.fileSystemService.playOnFreebox( this.freeboxId, path);
    }

}


import {Component, Input, EventEmitter, Output} from "@angular/core";
import {FileInfo} from "../../api/models/file-info";


@Component({
    selector: 'file-listing-icons-large',
    templateUrl : 'file-listing-icons-large.component.html',
})



export class FileListingIconsLargeComponent {

    @Input()
    files : FileInfo[];

    @Input()
    freeboxId : string;

}

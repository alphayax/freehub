
import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {FileInfo} from "../api/models/file-info";

@Injectable()
export class FileSystemControlService {

    public _onPlayOnFreebox     : BehaviorSubject<FileInfo>;
    public _onPlayOnBrowser     : BehaviorSubject<FileInfo>;
    public _onDownloadOnFreebox : BehaviorSubject<FileInfo>;
    public _onDownloadOnBrowser : BehaviorSubject<FileInfo>;
    public _onNavigate          : BehaviorSubject<FileInfo>;
    public _onInformation       : BehaviorSubject<FileInfo>;

    constructor(
    ) {
        this._onPlayOnFreebox     = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
        this._onPlayOnBrowser     = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
        this._onDownloadOnFreebox = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
        this._onDownloadOnBrowser = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
        this._onNavigate          = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
        this._onInformation       = <BehaviorSubject<FileInfo>> new BehaviorSubject( null);
    }

    public get onPlayOnFreebox() : Observable<FileInfo> {
        return this._onPlayOnFreebox.asObservable();
    }

    public get onPlayOnBrowser() : Observable<FileInfo> {
        return this._onPlayOnBrowser.asObservable();
    }

    public get onDownloadOnFreebox() : Observable<FileInfo> {
        return this._onDownloadOnFreebox.asObservable();
    }

    public get onDownloadOnBrowser() : Observable<FileInfo> {
        return this._onDownloadOnBrowser.asObservable();
    }

    public get onNavigate() : Observable<FileInfo> {
        return this._onNavigate.asObservable();
    }

    public get onInformation() : Observable<FileInfo> {
        return this._onInformation.asObservable();
    }

}

import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import { DirectoryInfo } from "../api/models/directory-info";
import { FileSystemService } from "../api/file-system.service";
import {FileInfo} from "../api/models/file-info";
import {AyxAlertService} from "../shared/ayx-alert/ayx-alert.service";
import {FileSystemControlService} from "./file-system-control.service";
import {FileInfoDetailsModal} from "./modals/file-info-details-modal/file-info-details-modal.component";
import {FreeboxInfo} from "../api/models/freebox-info";
import {FreeboxService} from "../api/freebox.service";

@Component({
    selector: 'file-system',
    templateUrl : 'file-system.component.html',
    providers : [
        FileSystemService,
        FileSystemControlService,
        FreeboxService,
    ],
})



export class FileSystemComponent implements OnInit {

    @ViewChild( 'detailsModal')
    public detailsModal : FileInfoDetailsModal;

    protected freeboxInfos : FreeboxInfo[];

    directoryInfo: DirectoryInfo;
    uid: string;
    uid_target: string;
    path: string;

    view_mode : number;

    constructor(
        private route: ActivatedRoute,
        private fileSystemService : FileSystemService,
        private freeboxService : FreeboxService,
        private ayxAlertService : AyxAlertService,
        private fileSystemControlService : FileSystemControlService
    ){
        this.view_mode  = 2;
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            this.uid_target = params['uid'];
            this.path       = params['path'] ? atob( params['path']) : '/';
            //this.loadFromCache();
            this.getDirectoryInfo();
        });

        this.freeboxService.getAll()
            .then( freeboxInfos => {
                this.freeboxInfos = freeboxInfos;
            });

        // Subscribe to control service
        this.fileSystemControlService.onPlayOnFreebox.subscribe(     fileInfo => fileInfo && this.playOnFreebox( fileInfo));
        this.fileSystemControlService.onPlayOnBrowser.subscribe(     fileInfo => fileInfo && this.playInBrowser( fileInfo));
        this.fileSystemControlService.onDownloadOnFreebox.subscribe( fileInfo => fileInfo && this.boxDownload( fileInfo));
        this.fileSystemControlService.onDownloadOnBrowser.subscribe( fileInfo => fileInfo && this.directDownload( fileInfo));
        this.fileSystemControlService.onNavigate.subscribe(          fileInfo => fileInfo && this.navigate( fileInfo));
        this.fileSystemControlService.onInformation.subscribe(       fileInfo => fileInfo && this.detailsModal.show( fileInfo));
    }

    loadFromCache(){
        if( ! this.uid_target || ! this.path){
            return;
        }
        let cachedContent = localStorage.getItem( btoa( this.uid_target + this.path));
        if( cachedContent){
            this.directoryInfo = new DirectoryInfo();
            this.directoryInfo.parse( JSON.parse( cachedContent));
        }
    }

    saveToCache( directoryInfo){
        localStorage.setItem( btoa( this.uid_target + this.path), JSON.stringify( directoryInfo));
    }

    navigate( fileInfo : FileInfo){
        this.navigateToPath( fileInfo.path);
    }

    navigateToPath( path : string){
        this.navigateTo( this.uid_target, path);
    }

    navigateTo( uid : string, path : string){
        this.fileSystemService.navigateTo( uid, path);
    }

    getDirectoryInfo(){
        if( ! this.uid_target || ! this.path){
            return;
        }
        console.log(this.uid_target, this.path);
        this.fileSystemService.explore( this.uid_target, this.path)
            .then( directoryInfo => {

                // Save data into cache
                this.saveToCache( directoryInfo);

                // No update if user already change dir
                if( directoryInfo.path == this.path){
                    this.directoryInfo = new DirectoryInfo();
                    this.directoryInfo.parse( directoryInfo);
                }
            });
    }

    play( event : { target: string, fileInfo: FileInfo}){
        switch( event.target){
            case 'freebox' : return this.playOnFreebox( event.fileInfo);
            case 'browser' : return this.playInBrowser( event.fileInfo);
        }
    }

    playInBrowser( fileInfo : FileInfo){
        this.fileSystemService.playInBrowser( this.uid_target, fileInfo.path, fileInfo.fileInfo.mimetype)
            .catch( error => {
                console.error( error);
                this.ayxAlertService.addDanger( error);
            });
    }

    playOnFreebox( fileInfo: FileInfo){
        this.fileSystemService.playOnFreebox( this.uid_target, fileInfo.path)
            .then( () => {
                this.ayxAlertService.addSuccess( 'Lecture sur la freebox de ' + fileInfo.fileInfo.name);
            })
            .catch( error => {
                this.ayxAlertService.addDanger( error);
            });
    }

    download( event : { target: string, fileInfo: FileInfo}){
        switch( event.target){
            case 'freebox' : return this.boxDownload( event.fileInfo);
            case 'browser' : return this.directDownload( event.fileInfo);
        }
    }

    directDownload( fileInfo: FileInfo){
        this.fileSystemService.downloadInBrowser( this.uid_target, fileInfo.path)
            .then( share => {
                window.open( share.fullurl);
            });
    }

    boxDownload( fileInfo: FileInfo){
        this.fileSystemService.downloadOnFreebox( this.uid_target, fileInfo.path)
            .then( () => {
                this.ayxAlertService.addSuccess( "Téléchargement ajouté : "+ fileInfo.fileInfo.name);
            });
    }

    setViewMode( mode : number){
        this.view_mode = mode;
    }
}


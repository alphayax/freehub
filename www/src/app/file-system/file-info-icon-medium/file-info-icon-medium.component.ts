import {Component, Input, ViewChild, AfterViewInit, ElementRef} from '@angular/core';
import { PosterService } from "../../shared/poster.service";
import { FileSystemService } from "../../api/file-system.service";
import {FileInfo} from "../../api/models/file-info";
import {FileSystemControlService} from "../file-system-control.service";


@Component({
    selector    : 'file-info-icon-medium',
    templateUrl : 'file-info-icon-medium.component.html',
    styleUrls : [ 'file-info-icon-medium.component.css'],
    providers   : [
        PosterService,
        FileSystemService,
    ],
})

export class FileInfoIconMediumComponent implements AfterViewInit {

    @Input()
    fileInfo: FileInfo;

    @Input()
    freeboxId : string;

    @ViewChild( 'poster')
    public poster : ElementRef;


    constructor(
        private posterService : PosterService,
        private fileSystemControlService : FileSystemControlService,
    ){ }

    ngAfterViewInit() {

    }

    onClick(){
        // Directory : navigate into
        if( this.fileInfo.isDir()){
            this.fileSystemControlService._onNavigate.next( this.fileInfo);
            return;
        }
        // Common file : show popup
        this.fileSystemControlService._onInformation.next( this.fileInfo);
    }

}

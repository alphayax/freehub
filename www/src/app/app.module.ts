import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

import {AlertModule, ModalModule, TabsModule} from 'ngx-bootstrap';

import {AppComponent} from './app.component';
import {MenuHeaderComponent} from "./menu-header/menu-header.component";
import {ModalLoginComponent} from './modal-login/modal-login.component';
import {ModalSignupComponent} from './modal-signup/modal-signup.component';
import {FreehubApiService} from "./shared/freehub-api.service";
import {FreehubUserService} from "./shared/freehub-user.service";
import {AyxAlertService} from "./shared/ayx-alert/ayx-alert.service";

import {AppRoutingModule} from "./app-routing.module";
import {DownloadModule} from "./download/download.module";
import {FileSystemModule} from "./file-system/file-system.module";
import {SharedModule} from "./shared/shared.module";
import {HomeModule} from "./home/home.module";
import {FriendsModule} from "./friends/friends.module";
import {SettingsModule} from "./settings/settings.module";
import {UploadModule} from "./upload/upload.module";
import {AssociationModule} from "./association/association.module";
import {AyxAlert} from "./shared/ayx-alert/ayx-alert.component";


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    // Shared module
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    TabsModule.forRoot(),
    SharedModule,
    // Feature modules
    AssociationModule,
    DownloadModule,
    FileSystemModule,
    FriendsModule,
    HomeModule,
    SettingsModule,
    UploadModule,
  ],
  declarations: [
    AppComponent,
    MenuHeaderComponent,
    ModalLoginComponent,
    ModalSignupComponent,
    AyxAlert,
  ],
  providers: [
    // Locale
    //{ provide: LOCALE_ID, useValue: "fr-FR" },
    // Generic services
    AyxAlertService,
    FreehubApiService,
    FreehubUserService,
  ],
  entryComponents: [
    ModalLoginComponent,
    ModalSignupComponent,
  ],
  bootstrap: [
    AppComponent
  ],
})
export class AppModule {
}

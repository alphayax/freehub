import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {SettingsComponent} from "./settings.component";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'settings', component: SettingsComponent, canActivate: [ LoggedInGuard ] }
    ])],
    exports: [RouterModule]
})

export class SettingsRoutingModule {

}

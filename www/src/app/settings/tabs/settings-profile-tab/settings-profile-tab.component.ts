import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../api/user.service";
import {AyxAlertService} from "../../../shared/ayx-alert/ayx-alert.service";


@Component({
    selector: 'settings-profile-tab',
    templateUrl: 'settings-profile-tab.component.html',
    providers : [
      UserService,
      AyxAlertService,
    ]
})

export class SettingsProfileTabComponent implements OnInit {

    private user : {
      login: string;
      password: string;
    } = {
      login: "",
      password: "",
    };

    constructor(
        private userService : UserService,
        private ayxAlertService : AyxAlertService
    ) { }

    ngOnInit() {
        this.userService.getInfos().then(infos => {
            this.user.login = infos.name;
        });
    }

    saveProfile(){
      this.userService.update(this.user)
        .then( () => {
          this.ayxAlertService.addInfo( 'Vos informations ont bien été mises a jour');
        })
        .catch( error => {
            console.error(error);
            this.ayxAlertService.addDanger( error);
        });
    }

}


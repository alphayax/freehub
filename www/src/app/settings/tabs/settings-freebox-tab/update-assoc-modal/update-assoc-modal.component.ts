import {Component} from "@angular/core";
import {BsModalRef} from "ngx-bootstrap";
import {AyxAlertService} from "../../../../shared/ayx-alert/ayx-alert.service";
import {FreeboxAssociation} from "../../../../shared/freebox-association";
import {LinkService} from "../../../../api/link.service";

@Component({
    selector: 'update-assoc-modal',
    templateUrl: 'update-assoc-modal.component.html',
    providers: [
        LinkService,
    ],
})

export class UpdateAssocModalComponent {

    private assoc: FreeboxAssociation;

    constructor(
        public bsModalRef: BsModalRef,
        private ayxAlertService: AyxAlertService,
        private linkService : LinkService,
    ) {
        this.assoc = new FreeboxAssociation();
    }

    /**
     * Update the RSS Scan
     */
    updateAssoc(): void {
        this.linkService.updateLink( this.assoc).then( ()=> {
            this.ayxAlertService.addSuccess('Informations de connexion mises à jour');
            this.bsModalRef.hide();
        });
    }

}

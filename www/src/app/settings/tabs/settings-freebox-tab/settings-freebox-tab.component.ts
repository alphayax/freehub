import {Component, OnInit} from '@angular/core';
import {FreeboxService} from "../../../api/freebox.service";
import {FreeboxAssociation} from "../../../shared/freebox-association";
import {FreehubUserService} from "../../../shared/freehub-user.service";
import {AyxAlertService} from "../../../shared/ayx-alert/ayx-alert.service";
import {BsModalService} from "ngx-bootstrap";
import {UpdateAssocModalComponent} from "./update-assoc-modal/update-assoc-modal.component";


@Component({
    selector: 'settings-freebox-tab',
    templateUrl: 'settings-freebox-tab.component.html',
    providers : [
        FreeboxService,
      AyxAlertService,
    ]
})

export class SettingsFreeboxTabComponent implements OnInit {

    freeboxInfos : FreeboxAssociation;

    uid: string;

    constructor(
        private freeboxService : FreeboxService,
        private freeHubUserService: FreehubUserService,
        private ayxAlertService : AyxAlertService,
        private modalService: BsModalService,
    ) { }

    ngOnInit() {
        this.uid = this.freeHubUserService.userStatus.uid;
        this.getFreeboxInfo();
    }

    getFreeboxInfo() {
        this.freeboxService.getFreeboxInfo().then( freeboxInfos => {
            this.freeboxInfos = freeboxInfos;
        });
    }

    removeFreeboxFromHub(){
        this.freeboxService.removeFreebox( this.uid);
        /*
        this.freeHubUserService.refresh().then( () => {
            this.ayxAlertService.addSuccess( 'Les information de connexion à votre Freebox ont été retirées de notre systeme.');
        });
        */
    }

    showUpdateAssocModal(){
        let bsModalRef = this.modalService.show(UpdateAssocModalComponent);
            bsModalRef.content.assoc = this.freeboxInfos;
    }
}


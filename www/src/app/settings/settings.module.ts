import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {SettingsComponent} from "./settings.component";
import {SettingsRoutingModule} from "./settings-routing.module";
import {SettingsFreeboxTabComponent} from "./tabs/settings-freebox-tab/settings-freebox-tab.component";
import {SettingsProfileTabComponent} from "./tabs/settings-profile-tab/settings-profile-tab.component";
import {FormsModule} from "@angular/forms";
import {TabsModule} from "ngx-bootstrap";
import {UpdateAssocModalComponent} from "./tabs/settings-freebox-tab/update-assoc-modal/update-assoc-modal.component";


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        SettingsRoutingModule,
        TabsModule.forRoot(),
    ],
    declarations: [
        SettingsComponent,
        SettingsFreeboxTabComponent,
        SettingsProfileTabComponent,
        UpdateAssocModalComponent,
    ],
    providers: [],
    entryComponents: [
        UpdateAssocModalComponent,
    ],
})

export class SettingsModule {

}

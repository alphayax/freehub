import {Component, OnInit} from '@angular/core';
import {UploadService} from "../api/upload.service";
import {ShareLink} from "../api/models/share-link";
import {AyxAlertService} from "../shared/ayx-alert/ayx-alert.service";

@Component({
    selector: 'upload',
    templateUrl: 'upload.component.html',
    providers : [
        UploadService,
    ]
})

export class UploadComponent implements OnInit {

    shareLinks: ShareLink[];

    constructor(
        private uploadService : UploadService,
        private ayxAlertService : AyxAlertService,
    ){ }

    ngOnInit() {
        this.refresh();
    }

    refresh() : void {
        this.uploadService.getAllShareLinks()
            .then( shareLinks => {
                this.shareLinks = shareLinks;
            });
    }

    revokeAllShareLinks() : void {
        for( let shareLink of this.shareLinks){
            this.revokeShareLinksFromToken( shareLink.token);
        }
    }

    revokeShareLinksFromToken( token) : void {
        this.uploadService.revokeShareLinksFromToken( token)
            .then( data => {
                let revokedShareLink = this.getShareLinkFromToken( token);
                this.shareLinks = this.shareLinks.filter( shareLink => shareLink.token != token);
                this.ayxAlertService.addWarning( 'Partage révoqué : '+ revokedShareLink.name);
            });
    }

    getShareLinkFromToken( token) : ShareLink {
        return this.shareLinks.filter( shareLink => shareLink.token == token).pop();
    }

}


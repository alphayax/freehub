import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {UploadComponent} from "./upload.component";
import {BoxLinkedGuard} from "../shared/guards/box-linked.guard";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'upload', component: UploadComponent, canActivate: [ LoggedInGuard, BoxLinkedGuard ] }
    ])],
    exports: [RouterModule]
})

export class UploadRoutingModule {

}

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {FriendsRoutingModule} from "./friends-routing.module";
import {SharedModule} from "../shared/shared.module";

import {FriendsComponent} from "./friends.component";
import {AddFriendModalComponent} from "./modals/add-friend/add-friend-modal.component";
import {ModalSignupComponent} from "../modal-signup/modal-signup.component";
import {ModalLoginComponent} from "../modal-login/modal-login.component";
import {FormsModule} from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    FriendsRoutingModule,
  ],
  declarations: [
    FriendsComponent,
    AddFriendModalComponent,
  ],
  providers: [],
  entryComponents: [
    AddFriendModalComponent,
  ],
})

export class FriendsModule {

}

import {Component, ViewChild, OnInit} from '@angular/core';
import {AddFriendModalComponent} from "./modals/add-friend/add-friend-modal.component";
import {FriendsService} from "../api/friends.service";
import {FreehubUserService} from "../shared/freehub-user.service";
import {BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'friends',
  templateUrl: 'friends.component.html',
  providers: [
    FriendsService,
  ]
})

export class FriendsComponent implements OnInit {

  protected friends: { uid: string, name: string }[];

  protected friendshipRequests: { uid: string, name: string }[];

  protected uid: string;

  @ViewChild('addFriendModal')
  public addFriendModal: AddFriendModalComponent;

  constructor(
    private friendsService: FriendsService,
    private userService: FreehubUserService,
    private modalService: BsModalService,
    ) {
  }

  ngOnInit() {
    this.getFriends();
    this.getFriendsRequests();
    this.uid = this.userService.userStatus.uid;
  }

  /**
   * Open modal to add friend
   */
  public addFriend() {
    this.modalService.show(AddFriendModalComponent);
  }

  getFriends() {
    this.friendsService.getFriends().then(
      friends => {
        this.friends = friends;
      }
    );
  }

  getFriendsRequests() {
    this.friendsService.getFriendshipRequests().then(
      friendshipRequests => {
        console.log(friendshipRequests);
        this.friendshipRequests = friendshipRequests;
      }
    );
  }

  acceptRequest(uid: string) {
    this.friendsService.answerRequest(uid, true)
      .then(
        () => {
          this.getFriends();
          this.getFriendsRequests();
        }
      );
  }

  refuseRequest(uid: string) {
    this.friendsService.answerRequest(uid, false)
      .then(
        () => {
          this.getFriendsRequests();
        }
      );
  }

  removeFriend(uid: string) {
    this.friendsService.removeFriend(uid)
      .then(
        () => {
          this.getFriends();
          this.getFriendsRequests();
        }
      );
  }

}

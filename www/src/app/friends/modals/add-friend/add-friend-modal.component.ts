import {Component} from "@angular/core";
import {FriendsService} from "../../../api/friends.service";
import {AyxAlertService} from "../../../shared/ayx-alert/ayx-alert.service";
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'add-friend-modal',
  templateUrl: 'add-friend-modal.component.html',
  providers: [
    FriendsService,
  ]
})

export class AddFriendModalComponent {

  private friend_name: string;

  constructor(
    public bsModalRef: BsModalRef,
    private friendsService: FriendsService,
    private ayxAlertService: AyxAlertService,
    ) {
  }

  /**
   * Add a friend
   */
  addFriend(): void {

    this.friendsService.requestFriendship(this.friend_name)
      .then(() => {
        this.ayxAlertService.addInfo('Demande d\'amitié envoyée');
        this.bsModalRef.hide();
      })
      .catch(e => {
        this.ayxAlertService.addDanger(e);
      })
  }

}

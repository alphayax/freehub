import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {FriendsComponent} from "./friends.component";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'friends', component: FriendsComponent, canActivate: [ LoggedInGuard ] }
    ])],
    exports: [RouterModule]
})

export class FriendsRoutingModule {

}

import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ModalLoginComponent} from "./modal-login/modal-login.component";
import {ModalSignupComponent} from "./modal-signup/modal-signup.component";
import {FreehubUserService} from "./shared/freehub-user.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styles: [
        //'@import url("https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/darkly/bootstrap.min.css");',
        '@import url("https://bootswatch.com/4/darkly/bootstrap.min.css");',
        //'@import url("https://bootswatch.com/4/assets/css/custom.min.css");',
    ],
})

export class AppComponent implements OnInit {

  constructor(
    private us: FreehubUserService,
  ){ }

  ngOnInit(): void {
    this.us.initUserStatus();
  }

}


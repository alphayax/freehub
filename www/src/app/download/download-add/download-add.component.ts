import { Component} from "@angular/core";
import {DownloadService} from "../../api/download.service";
import {AyxAlertService} from "../../shared/ayx-alert/ayx-alert.service";

@Component({
    selector: 'download-add',
    templateUrl: 'download-add.component.html',
    providers: [DownloadService],
})

export class DownloadAddComponent {

    protected url : string;
    protected url_list : string;

    constructor(
        private downloadService: DownloadService,
        private ayxAlertService : AyxAlertService,
    ){ }

    public addDownloadFromUrl(){

        if( ! this.url){
            this.ayxAlertService.addWarning( "Vous devez spécifier une URL");
            return;
        }

        this.downloadService.addDownloadFromUrl( this.url).then( () => {
            this.ayxAlertService.addSuccess( "Téléchargement ajouté : "+ this.url);
        });
    }

    public addDownloadFromUrls(){

        if( ! this.url_list){
            this.ayxAlertService.addWarning( "Vous devez spécifier une ou plusieurs URL");
            return;
        }

        let urls = this.url_list.split( /\n/);
        for( let url of urls){
            this.downloadService.addDownloadFromUrl( url).then( () => {
                this.ayxAlertService.addSuccess( "Téléchargement ajouté : " + url);
            });
        }
    }

}

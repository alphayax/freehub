import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DownloadItem } from "../../../api/models/download-item";

@Component({
    selector: 'download-item',
    templateUrl: 'download-item.component.html',
})

export class DownloadItemComponent {

    @Input()
    downloadItem: DownloadItem;

    @Output()
    onPause = new EventEmitter<DownloadItem>();

    @Output()
    onResume = new EventEmitter<DownloadItem>();

    @Output()
    onRetry = new EventEmitter<DownloadItem>();

    @Output()
    onClear = new EventEmitter<DownloadItem>();

    @Output()
    onNavigate = new EventEmitter<DownloadItem>();

    @Output()
    onSearchNext = new EventEmitter<DownloadItem>();

    @Output()
    onSettings = new EventEmitter<DownloadItem>();

    @Output()
    onRemove = new EventEmitter<DownloadItem>();

    constructor(
    ){ }

    clearDownload() {
        this.onClear.emit( this.downloadItem);
    }

    pauseDownload() {
        this.downloadItem.downloadTask.status = 'pause';
        this.onPause.emit( this.downloadItem);
    }

    resumeDownload() {
        this.downloadItem.downloadTask.status = 'download';
        this.onResume.emit( this.downloadItem);
    }

    retryDownload() {
        this.downloadItem.downloadTask.status = 'retry';
        this.onRetry.emit( this.downloadItem);
    }

    removeDownload(){
        this.onRemove.emit( this.downloadItem);
    }

    navigate() {
        this.onNavigate.emit( this.downloadItem);
    }

    searchNext(){
        this.onSearchNext.emit( this.downloadItem);
    }

    showSettings(){
        this.onSettings.emit( this.downloadItem);
    }

}

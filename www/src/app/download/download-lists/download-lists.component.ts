import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {DownloadService} from "../../api/download.service";
import {DownloadItemList} from "../../api/models/download-item-list";
import {DownloadSettingsModal} from "./modals/download-settings-modal/download-settings-modal.component";
import {FreehubUserService} from "../../shared/freehub-user.service";
import {AyxAlertService} from "../../shared/ayx-alert/ayx-alert.service";
import {DownloadItem} from "../../api/models/download-item";
import {RssSearch} from "../../api/models/rss-search";
import {DownloadRssUpdateModal} from "../download-rss/modals/download-rss-update-modal/download-rss-update-modal.component";
import {BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'download-lists',
  templateUrl: 'download-lists.component.html',
  providers: [
      DownloadService,
  ],
})

export class DownloadListsComponent implements OnInit {

    downloadItemList : DownloadItemList;

    downloadItemsFiltered : DownloadItem[] = [];

    @ViewChild('settingsModal')
    public settingsModal : DownloadSettingsModal;

    constructor(
        private modalService: BsModalService,
        private downloadService: DownloadService,
        private freehubUserService: FreehubUserService,
        private ayxAlertService : AyxAlertService,
        private router: Router,
    ){
        this.downloadItemList = new DownloadItemList;
    }

    ngOnInit() {
        this.getDownloads();
        this.loadFromCache();
        this.filterDownloads(0);
    }

    loadFromCache(){
        let cachedContent = localStorage.getItem( 'downloads');
        if( cachedContent){
            let downloads_obj = JSON.parse( cachedContent);
            this.downloadItemList.parse( downloads_obj);
        }
    }

    saveToCache(){
        localStorage.setItem( 'downloads', JSON.stringify( this.downloadItemList.downloadItems));
    }

    getDownloads() : void {
        this.downloadService.getDownloads()
            .then( downloads_obj => {
                this.downloadItemList.parse( downloads_obj);
                this.saveToCache();
                this.ayxAlertService.addInfo( 'Téléchargements actualisés');
            })
    }

  /**
   * Filter the download according the given mode
   * @param {number} mode
   */
  filterDownloads( mode : number) : void {
      switch (mode){
        case 1 :this.downloadItemsFiltered = this.downloadItemList.getActive(); break;
        case 2 :this.downloadItemsFiltered = this.downloadItemList.getSeeding(); break;
        case 3 :this.downloadItemsFiltered = this.downloadItemList.getDownloaded(); break;
        case 4 :this.downloadItemsFiltered = this.downloadItemList.getInError(); break;
        default:
        case 0 :this.downloadItemsFiltered = this.downloadItemList.getAll(); break;
      }

  }

    cleanDone() : void {
        this.downloadService.cleanDone()
            .then( cleanedDownloadIds => {
                this.downloadItemList.removeFromIds( cleanedDownloadIds);
                this.ayxAlertService.addInfo( cleanedDownloadIds.length + ' téléchargements retirés');
            })
    }

    updateStatus( downloadTaskId : number, status : string){
        this.downloadService.updateStatusFromId( downloadTaskId, status)
            .then( download => {
                this.downloadItemList.getFromId( downloadTaskId).parse( download);
            });
    }

    cleanFromId( downloadTaskId : number) {
        this.downloadService.cleanFromId( downloadTaskId)
            .then( downloadItem => {
                this.downloadItemList.removeFromId( downloadItem.downloadTask.id);
                this.ayxAlertService.addInfo( downloadItem.downloadTask.name + ' retiré de la liste des téléchargements');
            });
    }

    /**
     * TODO : Tester
     * @param {number} downloadTaskId
     */
    removeFromId( downloadTaskId : number) {
        this.downloadService.removeFromId( downloadTaskId)
            .then( downloadItem => {
                this.downloadItemList.removeFromId( downloadItem.downloadTask.id);
                this.ayxAlertService.addWarning( downloadItem.downloadTask.name + ' supprimé du disque dur');
            });
    }

    navigate( path) {
        this.router.navigate(['/file-system', this.freehubUserService.userStatus.uid, btoa( path)]);
    }

    showSettings( downloadItem : DownloadItem){
        let bsModalRef = this.modalService.show(DownloadSettingsModal);
        bsModalRef.content.downloadItem = downloadItem;
    }

}


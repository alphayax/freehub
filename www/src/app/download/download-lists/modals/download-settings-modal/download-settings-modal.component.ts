import {Component} from "@angular/core";
import {DownloadItem} from "../../../../api/models/download-item";
import {BsModalRef} from "ngx-bootstrap";

@Component({
    selector: 'download-settings-modal',
    templateUrl: 'download-settings-modal.component.html',
})

export class DownloadSettingsModal {

    downloadItem: DownloadItem;

    constructor(
        public bsModalRef: BsModalRef,
    ) { }

}

import {OnInit, Component, Input, OnChanges} from "@angular/core";
import {DownloadFile} from "../../../../../api/models/download-file";
import {DownloadService} from "../../../../../api/download.service";

@Component({
    selector: 'download-settings-modal-files-tab',
    templateUrl: 'download-settings-modal-files-tab.component.html',
    providers: [
        DownloadService,
    ],
})

export class DownloadSettingsModalFilesTab implements OnInit, OnChanges {

    @Input()
    downloadId : number;

    public downloadFiles : DownloadFile[];

    constructor(
        private downloadService : DownloadService,
    ) {}

    ngOnInit(){
        this.refresh();
    }

    ngOnChanges(){
        this.refresh();
    }

    protected refresh(){
        this.downloadService.getFilesFromId( this.downloadId).then( files => {
            this.downloadFiles = files;
        });
    }

}

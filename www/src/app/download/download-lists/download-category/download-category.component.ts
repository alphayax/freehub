import {Component, Input} from '@angular/core';
import {DownloadItem} from "../../../api/models/download-item";

@Component({
    selector: 'download-category',
    templateUrl: 'download-category.component.html',
})

export class DownloadCategoryComponent {

    @Input()
    downloadItems: DownloadItem[];

}

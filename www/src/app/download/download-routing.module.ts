import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";

import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {BoxLinkedGuard} from "../shared/guards/box-linked.guard";

import {DownloadComponent} from "./download.component";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'download'      , component: DownloadComponent      , canActivate: [ LoggedInGuard, BoxLinkedGuard ] },
    ])],
    exports: [RouterModule]
})

export class DownloadRoutingModule {

}

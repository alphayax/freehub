import {Component} from "@angular/core";
import {BsModalRef} from "ngx-bootstrap";
import {AyxAlertService} from "../../../../shared/ayx-alert/ayx-alert.service";
import {RssSearch} from "../../../../api/models/rss-search";
import {DlRssService} from "../../../../api/dl-rss.service";

@Component({
  selector: 'download-rss-update-modal',
  templateUrl: 'download-rss-update-modal.component.html',
  providers: [
    DlRssService,
  ],
})

export class DownloadRssUpdateModal {

  private rssSearch: RssSearch;

  constructor(
    public bsModalRef: BsModalRef,
    private ayxAlertService: AyxAlertService,
    private dlRssService : DlRssService,
  ) {
    this.rssSearch = new RssSearch;
  }

  /**
   * Update the RSS Scan
   */
  updateRss(): void {
    this.dlRssService.updateRss( this.rssSearch).then( (rssSearch : RssSearch)=> {
      this.ayxAlertService.addSuccess('Scan mis à jour');
      this.bsModalRef.hide();
    });
  }

}

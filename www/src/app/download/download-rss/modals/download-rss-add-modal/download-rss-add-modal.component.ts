import {Component} from "@angular/core";
import {AyxAlertService} from "../../../../shared/ayx-alert/ayx-alert.service";
import {BsModalRef} from "ngx-bootstrap";
import {DlRssService} from "../../../../api/dl-rss.service";

@Component({
  selector: 'download-rss-add-modal',
  templateUrl: 'download-rss-add-modal.component.html',
  providers: [
      DlRssService,
  ]
})

export class DownloadRssAddModal {

  private rss_title: string;
  private rss_regex: string;
  private rss_url: string;

  constructor(
    public bsModalRef: BsModalRef,
    private dlRssService: DlRssService,
    private ayxAlertService: AyxAlertService
  ) { }


  addRss(): void {

    let rssScan = {
      'title': this.rss_title,
      'url'  : this.rss_url,
      'regex': this.rss_regex,
    };

    this.dlRssService.add( rssScan).then(rssScanAdded => {
      this.ayxAlertService.addSuccess('Nouveau scan enregistré');
      this.bsModalRef.hide();
    });
  }

}

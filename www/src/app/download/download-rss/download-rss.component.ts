import {Component, OnInit} from '@angular/core';
import {AyxAlertService} from "../../shared/ayx-alert/ayx-alert.service";
import {BsModalService} from "ngx-bootstrap";
import {DownloadRssAddModal} from "./modals/download-rss-add-modal/download-rss-add-modal.component";
import {DownloadRssUpdateModal} from "./modals/download-rss-update-modal/download-rss-update-modal.component";
import {RssSearch} from "../../api/models/rss-search";
import {DlRssService} from "../../api/dl-rss.service";

@Component({
  selector: 'download-rss',
  templateUrl: 'download-rss.component.html',
  providers: [
    DlRssService,
  ],
})

export class DownloadRssComponent implements OnInit {

  rssSearches: RssSearch[];

  constructor(
    private modalService: BsModalService,
    private ayxAlertService: AyxAlertService,
    private dlRssService : DlRssService,
    ) {
  }

  ngOnInit() {
    this.refresh();
  }

  /**
   * Get all the RSS Scans
   */
  public refresh() {
    this.dlRssService.getAll().then((rssSearches: RssSearch[]) => {
      this.rssSearches = rssSearches;
    });
  }

  /**
   * Check all the RSS Scans
   */
  public checkAll(){
    for( let rssSearch of this.rssSearches){
      this.checkRss( rssSearch);
    }
  }

  /**
   * Check the specified RSS Scan
   * @param {RssSearch} rssSearchToCheck
   */
  public checkRss(rssSearchToCheck: RssSearch) {
    this.dlRssService.check( rssSearchToCheck.id).then((rssSearchChecked : RssSearch) => {

      if (rssSearchToCheck.last_publication == rssSearchChecked.last_publication) {
        this.ayxAlertService.addInfo('Aucun nouvel élément à télécharger');
      } else {
        for (let result of rssSearchChecked.last_items) {
          this.ayxAlertService.addSuccess('Téléchargement ajouté : ' + result);
        }
      }
      this.rssSearches = this.rssSearches.map(x => x.id == rssSearchChecked.id ? rssSearchChecked : x)
    }).catch(err => {
      this.ayxAlertService.addDanger(err);
    });
  }

  /**
   * Remove a RSS Scan from id
   * @param {string} rssId
   */
  public removeFromId(rssId: string) {
    this.dlRssService.removeFromId(rssId).then(info => {
      console.log(info);
      this.refresh();
    });
  }

  /**
   * Update the specified RSS scan
   * @param {RssSearch} rssSearch
   */
  public updateRss(rssSearch: RssSearch) {
    let bsModalRef = this.modalService.show(DownloadRssUpdateModal);
    bsModalRef.content.rssSearch = rssSearch;
  }

  /**
   * Open modal to configure a new RSS scan
   */
  public addNewRss() {
    this.modalService.show(DownloadRssAddModal);
  }

}

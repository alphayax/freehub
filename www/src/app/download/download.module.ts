import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DownloadRoutingModule} from "./download-routing.module";
import {DownloadComponent} from "./download.component";
import {DownloadItemComponent} from "./download-lists/download-item/download-item.component";
import {DownloadSettingsModal} from "./download-lists/modals/download-settings-modal/download-settings-modal.component";
import {DownloadSettingsModalFilesTab} from "./download-lists/modals/download-settings-modal/download-settings-modal-files-tab/download-settings-modal-files-tab.component";
import {SharedModule} from "../shared/shared.module";
import {DownloadListsComponent} from "./download-lists/download-lists.component";
import {DownloadAddComponent} from "./download-add/download-add.component";
import {DownloadRssComponent} from "./download-rss/download-rss.component";
import {DownloadRssAddModal} from "./download-rss/modals/download-rss-add-modal/download-rss-add-modal.component";
import {DownloadRssUpdateModal} from "./download-rss/modals/download-rss-update-modal/download-rss-update-modal.component";
import {DownloadCategoryComponent} from "./download-lists/download-category/download-category.component";
import {FormsModule} from "@angular/forms";
import {AyxAlertService} from "../shared/ayx-alert/ayx-alert.service";
import {AccordionModule, BsDropdownModule, TabsModule} from "ngx-bootstrap";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DownloadRoutingModule,
    SharedModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
  ],
  declarations: [
    DownloadComponent,
    DownloadItemComponent,
    DownloadCategoryComponent,
    DownloadSettingsModalFilesTab,
    DownloadAddComponent,
    DownloadListsComponent,
    DownloadRssComponent,
    DownloadRssAddModal,
    DownloadRssUpdateModal,
    DownloadSettingsModal,
  ],
  providers: [
    AyxAlertService,
  ],
  entryComponents : [
    DownloadRssAddModal,
    DownloadRssUpdateModal,
    DownloadSettingsModal,
  ]
})

export class DownloadModule {

}

import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {FreehubApiService} from "../shared/freehub-api.service";
import {FreehubUserService} from "../shared/freehub-user.service";
import {AyxAlertService} from "../shared/ayx-alert/ayx-alert.service";

@Component({
  selector: 'app-modal-signup',
  templateUrl: './modal-signup.component.html',
  styleUrls: ['./modal-signup.component.css']
})
export class ModalSignupComponent implements OnInit {

  private login;
  private password;

  constructor(
    public bsModalRef: BsModalRef,
    private us: FreehubUserService,
    private api: FreehubApiService,
    private ayxAlert: AyxAlertService,
  ) {
  }

  ngOnInit() {
    this.login = '';
    this.password = '';
  }

  processSignUp(){

    let body = {
      login: this.login,
      password: this.password,
    };

    this.api.post( 'user/signup', body).then(
      userStatus => {
      this.us.updateUserStatus( userStatus);
      this.bsModalRef.hide();
    },
      error => {
          this.ayxAlert.addDanger(error);
    });
  }
}

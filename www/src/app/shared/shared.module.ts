import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OctetToHumanReadablePipe} from "./octet-human-readable.pipe";
import {BackgroundMovieDirective} from "./background-movie.directive";
import {FreeboxStatusComponent} from "./freebox-status/freebox-status.component";
import {LoggedInGuard} from "./guards/logged-in.guard";
import {BoxLinkedGuard} from "./guards/box-linked.guard";
import {LoadingAnimationComponent} from "./loading-animation/loading-animation.component";
import {FontAwesomeModule} from "ngx-icons";


@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    //ModalModule.forRoot(),
    //AlertModule.forRoot(),
  ],
  declarations: [
    OctetToHumanReadablePipe,
    BackgroundMovieDirective,
    FreeboxStatusComponent,
    LoadingAnimationComponent,
  ],
  exports: [
    CommonModule,
    OctetToHumanReadablePipe,
    BackgroundMovieDirective,
    FreeboxStatusComponent,
    LoadingAnimationComponent,
    FontAwesomeModule,
  ],
  providers: [
    LoggedInGuard,
    BoxLinkedGuard,
  ]
})

export class SharedModule {

}

import {Injectable} from '@angular/core';
import {FreehubApiService} from "./freehub-api.service";


@Injectable()
export class PosterService {

    public static cache : {};

    constructor(
        private freeHubApiService : FreehubApiService,
    ){
        if( ! PosterService.cache){
            console.info('Load from cache');
            PosterService.loadFromCache();
        }
    }

    /**
     * @param fileName
     * @param imageType
     * @returns {Promise<string>}
     */
    getImage( fileName : string, imageType : string = 'poster') : Promise<string>{

        if( PosterService.cache.hasOwnProperty(fileName) && PosterService.cache[fileName].hasOwnProperty(imageType)){
            if( PosterService.cache[fileName][imageType] == ''){

                // Dirty, hugly, howful fix... Cause I understand nothing to observables and promises :'(
                // So... We'll wait 2 secs to see if response of API is here
                return new Promise( (resolve, reject) => {
                    if( PosterService.cache[fileName][imageType] === '') {
                        setTimeout(() => {
                            if (PosterService.cache[fileName][imageType] !== '') {
                                resolve(PosterService.cache[fileName][imageType]);
                            }
                        }, 2000);
                    }
                });
            }
            return Promise.resolve( PosterService.cache[fileName][imageType]);
        }

        if( ! PosterService.cache.hasOwnProperty(fileName)){
            PosterService.cache[fileName] = {};
        }
        PosterService.cache[fileName][imageType] = '';
        console.info('Not in cache. Grab image from API', fileName, imageType);
        return this.freeHubApiService.send( 'poster', 'get_image', {
            "file_name" : fileName,
            "image_type" : imageType
        }).then( response => {
            PosterService.cache[fileName][imageType] = response;
            PosterService.saveToCache();
            return response;
        });
    }

    static loadFromCache(){
        let cachedContent = localStorage.getItem( 'posters');
        PosterService.cache = cachedContent ? JSON.parse( cachedContent) : {};
    }

    static saveToCache(){
        localStorage.setItem( 'posters', JSON.stringify( PosterService.cache));
    }

}


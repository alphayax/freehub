import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {FreehubUserService} from "../freehub-user.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(
    private us: FreehubUserService,
    ) {
  }

  canActivate() {
    return this.us.userStatus.loggedIn;
  }
}

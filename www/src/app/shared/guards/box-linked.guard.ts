import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { FreehubUserService } from "../freehub-user.service";

@Injectable()
export class BoxLinkedGuard implements CanActivate {

    constructor(
        private us: FreehubUserService,
    ) { }

    /**
     * @returns {Observable<boolean>}
     */
    canActivate() {
      return this.us.userStatus.isBoxLinked;
    }

}

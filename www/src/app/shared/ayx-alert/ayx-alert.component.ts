import {OnInit, Component} from "@angular/core";
import {AyxAlertService} from "./ayx-alert.service";
import {AlertData} from "./alert-data";


@Component({
  selector: 'ayx-alert',
  templateUrl: 'ayx-alert.component.html',
  styleUrls: ['ayx-alert.component.css']
})

export class AyxAlert implements OnInit {

  public alerts: Array<AlertData> = [];

  constructor(
    private ayxAlertService: AyxAlertService,
    ) {
  }

  ngOnInit() {
    this.alerts = this.ayxAlertService.alerts;
  }

  public closeAlert(i: number): void {
    this.ayxAlertService.closeAlert(i);
  }
}

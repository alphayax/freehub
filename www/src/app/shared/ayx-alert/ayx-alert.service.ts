import {Injectable} from '@angular/core';
import {AlertData} from "./alert-data";


@Injectable()
export class AyxAlertService {

  public alerts: Array<AlertData> = [];

  constructor() {
  }

  /**
   * Add an alert
   * @param {string} message
   * @param {string} type
   * @param {number} timeout
   */
  public addAlert(message: string, type: string = 'info', timeout: number = 3000): void {
    let alert = {
      msg: message,
      type: type,
      closable: true,
      timeout: timeout,
    };

    console.log(alert);

    this.alerts.push(alert);
  }

  public addInfo(message: string) {
    this.addAlert(message, 'info');
  }

  public addWarning(message: string) {
    this.addAlert(message, 'warning');
  }

  public addSuccess(message: string) {
    this.addAlert(message, 'success');
  }

  public addDanger(message: string) {
    this.addAlert(message, 'danger');
  }

  public closeAlert(i: number): void {
    this.alerts.slice(i, i + 1);
  }

}



export interface AlertData {
    msg: string;
    type: string;
    closable: boolean;
    timeout: number;
}

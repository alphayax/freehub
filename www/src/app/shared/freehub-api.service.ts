import {Injectable} from '@angular/core';

import 'rxjs/add/operator/toPromise';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {FreehubUserService} from "./freehub-user.service";


@Injectable()
export class FreehubApiService {

    constructor(private http: HttpClient,
                private us: FreehubUserService,) {
    }

    private host = '';

    /**
     * Send a request to the Freehub API
     * @param service
     * @param action
     * @param params
     * @returns {Promise<any>}
     */
    send(service: string, action: string, params = {}): Promise<any> {

        console.info('API >>>', service, action, params);
        return this.checkConfig().then(() => {
            let token = this.us.userStatus.token || "";
            let url = this.host + '/' + service + '/' + action;
            let headers = new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Credentials', 'true')
                .set('X-Freehub-token', token);

            return new Promise((resolve, reject) => {
                this.http.post<any>(url, JSON.stringify(params), {headers: headers, withCredentials: true})
                    .subscribe(
                        response => {
                            if (response.success) {
                                console.info('API <<<', service, action, params, response.data);
                                return resolve(response.data);
                            } else {

                                // Dead Session
                                if (response.error.code = 1401) {
                                    this.us.clearUserStatus();
                                    window.location.href = '';
                                }

                                return reject(response.error.message)
                            }
                        },
                        error => {
                            console.error('Freehub Api Service', error);
                            return reject(error.message || error);
                        });
            });
        });
    }

    /**
     * Send a request to the Freehub API
     * @param service
     * @param action
     * @param params
     * @returns {Promise<any>}
     */
    send2(service: string, params = {}): Promise<any> {

        console.info('API >>>', service, params);
        return this.checkConfig().then(() => {
            let token = this.us.userStatus.token || "";
            let url = this.host + service;
            let headers = new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Access-Control-Allow-Credentials', 'true')
                .set('X-Freehub-token', token);

            return new Promise((resolve, reject) => {
                this.http.post<any>(url, JSON.stringify(params), {headers: headers, withCredentials: true})
                    .subscribe(
                        response => {
                            if (response.success) {
                                console.info('API <<<', service, params, response.data);
                                return resolve(response.data);
                            } else {

                                // Dead Session
                                if (response.error.code = 1401) {
                                    this.us.clearUserStatus();
                                    window.location.href = '';
                                }

                                return reject(response.error.message)
                            }
                        },
                        error => {
                            console.error('Freehub Api Service', error);
                            return reject(error.message || error);
                        });
            });
        });
    }

    private getHttpOptions() {

        let headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Credentials', 'true')
            .set('X-Freehub-token', this.us.userStatus.token || "");

        return {
            headers: headers,
            withCredentials: true
        }
    }

    public post(service: string, params = {}): Promise<any> {

        console.info('POST >>>', service, params);
        return this.checkConfig().then(() => {


            return new Promise((resolve, reject) => {
                this.http.post<any>(this.host + service, params, this.getHttpOptions()).subscribe(
                response => {
                    if (response.success) {
                        console.info('POST <<<', service, params, response.data);
                        return resolve(response.data);
                    } else {

                        // Dead Session
                        if (response.error.code = 1401) {
                            this.us.clearUserStatus();
                            window.location.href = '';
                        }

                        return reject(response.error.message)
                    }
                },
                error => {
                    this.handleApiError( reject, error);
                });
            });
        });
    }

    public put(service: string, params = {}): Promise<any> {

        console.info('PUT >>>', service, params);
        return this.checkConfig().then(() => {


            return new Promise((resolve, reject) => {
                this.http.put<any>(this.host + service, params, this.getHttpOptions()).subscribe(
                response => {
                    if (response.success) {
                        console.info('PUT <<<', service, params, response.data);
                        return resolve(response.data);
                    } else {

                        // Dead Session
                        if (response.error.code = 1401) {
                            this.us.clearUserStatus();
                            window.location.href = '';
                        }

                        return reject(response.error.message)
                    }
                },
                error => {
                    this.handleApiError( reject, error);
                });
            });
        });
    }

    public delete(service: string): Promise<any> {

        console.info('DELETE >>>', service);
        return this.checkConfig().then(() => {


            return new Promise((resolve, reject) => {
                this.http.delete<any>(this.host + service, this.getHttpOptions()).subscribe(
                response => {
                    if (response.success) {
                        console.info('DELETE <<<', service, response.data);
                        return resolve(response.data);
                    } else {

                        // Dead Session
                        if (response.error.code = 1401) {
                            this.us.clearUserStatus();
                            window.location.href = '';
                        }

                        return reject(response.error.message)
                    }
                },
                error => {
                    this.handleApiError( reject, error);
                });
            });
        });
    }

    public get(service: string): Promise<any> {

        console.info('GET >>>', service);
        return this.checkConfig().then(() => {


            return new Promise((resolve, reject) => {
                this.http.get<any>(this.host + service, this.getHttpOptions()).subscribe(
                response => {
                    if (response.success) {
                        console.info('GET <<<', service, response.data);
                        return resolve(response.data);
                    } else {

                        // Dead Session
                        if (response.error.code = 1401) {
                            this.us.clearUserStatus();
                            window.location.href = '';
                        }

                        return reject(response.error.message)
                    }
                },
                error => {
                    this.handleApiError( reject, error);
                });
            });
        });
    }

    private handleApiError( reject, error) {
        console.error('Freehub Api Service', error.message, error);
        return reject(error.error.error.message);
    }

    /**
     * Check if the configuration is present
     * Load the config is missing
     * @returns {any}
     */
    private checkConfig(): Promise<any> {

        if (this.host) {
            return Promise.resolve(true);
        }

        return new Promise((resolve, reject) => {

            this.http
                .get<any>('assets/api.json')
                .subscribe(
                    api_config => {
                        this.host = api_config.host;
                        resolve(true);
                    },
                    err => {
                        console.error(err);
                        reject(true);
                    });
        });
    }

}


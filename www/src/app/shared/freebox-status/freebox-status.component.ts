import {Component, Input, OnInit} from '@angular/core';
import {FreeboxStatus} from "../../api/models/freebox-status";
import {FreeboxService} from "../../api/freebox.service";
import {Observable} from "rxjs";


@Component({
    selector: 'freebox-status',
    templateUrl: 'freebox-status.component.html',
    providers: [
        FreeboxService,
    ]
})

export class FreeboxStatusComponent implements OnInit {

    @Input('uid')
    uid: string;

    freeboxStatus: FreeboxStatus;

    constructor(
        private freeboxService : FreeboxService,
    ){ }

    ngOnInit() {
        Observable.interval(5000).take(5)
            .subscribe((x) => {
                this.getStatus();
            });
    }

    getStatus(){
        this.freeboxService.getFreeboxStatus( this.uid)
            .then( freeboxStatus => {
                this.freeboxStatus = freeboxStatus;
            });
    }

}

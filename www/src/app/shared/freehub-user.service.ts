import {Injectable, OnInit} from '@angular/core';


interface FreehubUserStatus {
  loggedIn: boolean,
  isBoxLinked: boolean,
  uid: string,
  token: string,
  token_expire: number,
}


@Injectable()
export class FreehubUserService implements OnInit {

  ngOnInit(): void {
  }

  public userStatus: FreehubUserStatus = {
    loggedIn: false,
    isBoxLinked: false,
    uid: null,
    token: null,
    token_expire: null,
  };

  public isLoggedIn(){
    if( ! this.userStatus){
      this.clearUserStatus();
    }
    return this.userStatus.loggedIn;
  }

  public isBoxLinked(){
    if( ! this.userStatus){
      this.clearUserStatus();
    }
    return this.userStatus.isBoxLinked;
  }

  /**
   * Update and save userStatus
   */
  public updateUserStatus(userStatus: FreehubUserStatus) {
    console.log('UserStatus', this.userStatus, userStatus);
    this.userStatus = userStatus;
    this.saveUserStatus();
  }

  /**
   * Check in cache if a user status is present
   * Load it if found
   */
  public initUserStatus() {
    console.info('initUserStatus');
    let userStatus = localStorage.getItem('user');
    if (this.userStatus) {
      this.userStatus = JSON.parse(userStatus);
    }
  }

  /**
   * Reset user status and save it in cache
   */
  public clearUserStatus() {
    this.updateUserStatus({
      loggedIn: false,
      isBoxLinked: false,
      uid: null,
      token: null,
      token_expire: null,
    });
  }

  /**
   * Save in cache the userStatus
   */
  protected saveUserStatus() {
    localStorage.setItem('user', JSON.stringify(this.userStatus));
  }

}

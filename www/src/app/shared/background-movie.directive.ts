import {Directive, ElementRef, Input, Renderer, AfterViewInit} from '@angular/core';
import {PosterService} from "./poster.service";


@Directive({
    selector: '[background-movie]',
    providers: [
        PosterService,
    ]
})

export class BackgroundMovieDirective implements AfterViewInit {

    @Input('background-movie')
    movieName : string;

    protected _imageUrl : string;

    protected _posterType : string = 'banner';

    constructor(
        protected el: ElementRef,
        protected renderer: Renderer,
        protected posterService : PosterService,
    ) { }

    @Input('background-movie-type')
    set posterType( _posterType: string){
        this._posterType = _posterType || this._posterType;
    }

    public ngAfterViewInit() : void {
        this.getBanner();
    }

    protected getBanner() : void {

        if( ! this.movieName){
            console.warn('Pas de titre ');
            return;
        }

        this.posterService.getImage( this.movieName, this._posterType)
            .then( imageUrl => {
                this._imageUrl      = imageUrl;
                let backgroundSize  = (this._posterType === 'banner') ? 'cover' : 'contain';
                this.renderer.setElementStyle( this.el.nativeElement, 'background'      , 'no-repeat center');
                this.renderer.setElementStyle( this.el.nativeElement, 'backgroundImage' , 'url("'+ this._imageUrl +'")');
                this.renderer.setElementStyle( this.el.nativeElement, 'backgroundSize'  , backgroundSize);
            });
    }

}

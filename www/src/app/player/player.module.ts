import { NgModule } from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {PlayerRoutingModule} from "./player-routing.module";
import {PlayerComponent} from "./player.component";


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PlayerRoutingModule,
    ],
    declarations: [
       PlayerComponent,
    ],
    providers: [
    ]
})

export class PlayerModule {

}

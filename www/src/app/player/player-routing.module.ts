import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {PlayerComponent} from "./player.component";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'player/:url/:mime', component: PlayerComponent, canActivate: [ LoggedInGuard ] }
    ])],
    exports: [RouterModule]
})

export class PlayerRoutingModule {

}

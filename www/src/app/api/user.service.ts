import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";


@Injectable()
export class UserService {

    constructor(
        private api: FreehubApiService,
    ) { }

    /**
     * @returns {Promise<{ name : string }>}
     */
    public getInfos() : Promise<{ name : string }>{
        return this.api.send( 'user', 'get_infos');
    }

    /**
     * @returns {Promise<void>}
     */
    public update(infos : { login: string; password: string; }) : Promise<void>{
        return this.api.put( 'user/', infos);
    }

    /**
     * @returns {Promise<{ is_box_linked : boolean, token_expires: number}>}
     */
    public checkSession() : Promise<{ is_box_linked : boolean, token_expires: number}> {
        return this.api.send( 'user', 'check_session');
    }

  /**
   *
   * @returns {Promise<any>}
   */
    public logout(){
      return this.api.post( 'user/logout');
    }

}


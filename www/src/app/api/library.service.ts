import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {FreehubUserService} from "../shared/freehub-user.service";


@Injectable()
export class LibraryService {

    constructor(
        private freeHubApiService: FreehubApiService,
        private freeHubUserService: FreehubUserService,
    ) { }

    getMovies( offset : number = 0, limit : number = 0) : Promise<any[]> {
        return this.freeHubApiService.send( 'library', 'get_movies', {
            "offset" : offset,
            "limit"  : limit,
        });
    }

    getTvShows( offset : number = 0, limit : number = 0) : Promise<any[]> {
        return this.freeHubApiService.send( 'library', 'get_tv_shows', {
            "offset" : offset,
            "limit"  : limit,
        });
    }

}


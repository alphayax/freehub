import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {Router} from "@angular/router";
import {FreehubUserService} from "../shared/freehub-user.service";
import {ShareLink} from "./models/share-link";
import {DirectoryInfo} from "./models/directory-info";


@Injectable()
export class FileSystemService {

    constructor(
        private api: FreehubApiService,
        private freeHubUserService: FreehubUserService,
        private router: Router,
    ) { }

    navigateTo( uid, path){
        this.router.navigate(['/file-system', uid, btoa( path)]);
    }

    explore( uid, path) : Promise<DirectoryInfo> {
        return this.api.get( 'filesystem/' + uid + '/' + btoa(path) +'/');
    }

    playInBrowser( uid, path, mime) {
        return this.api.send( 'filesystem', 'share', {
            "path" : path,
            "uid"  : uid,
        }).then( shareLink => {
            let link = ['/player', btoa( shareLink.url), btoa( mime)];
            this.router.navigate(link);
        });
    }

    playOnFreebox( uid_target, path){
        return this.api.send( 'filesystem', 'play', {
            "uid_dst" : uid_target,
            "path"    : path
        })
    }

    downloadInBrowser( uid, path) : Promise<ShareLink>{
        return this.api.send( 'filesystem', 'direct_download', {
            "uid_dst" : uid,
            "path"    : path
        });
    }

    downloadOnFreebox( uid_target, path){
        return this.api.send( 'filesystem', 'box_download', {
            "uid_dst" : uid_target,
            "path"    : path
        });
    }


}


import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {FreeboxAssociation} from "../shared/freebox-association";


@Injectable()
export class LinkService {

    constructor(
        private api: FreehubApiService,
    ) { }

    getLink() : Promise<FreeboxAssociation>{
        return this.api.get( 'link/');
    }

    removeLink(){
        // Will be DELETE on link/
        return this.api.send( 'freebox', 'remove')
    }

    getPermissions() {
        return this.api.get( 'link/authorizations')
    }

    upsertLink( association = null) {
        return this.api.post( 'link/', {
            "assoc" : association
        })
    }

    updateLink( association = null) {
        return this.api.put( 'link/', {
            "assoc" : association
        })
    }

}



import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";


@Injectable()
export class FriendsService {

    constructor(
        private api: FreehubApiService,
    ) { }

    /**
     * Get the current user friend list
     * @returns {Promise<{ uid: string, name: string }>}
     */
    public getFriends() : Promise<{ uid: string, name: string }[]> {
        return this.api.get( 'friends/');
    }

    /**
     * Get the list of all user who want to be friend with the current user
     * @returns {Promise<{ uid: string, name: string }>}
     */
    public getFriendshipRequests() : Promise<{ uid: string, name: string }[]> {
        return this.api.get( 'friends/requests');
    }

    /**
     * Send a friendship request from the current user to another user
     * @param friendName
     * @returns {Promise<boolean>}
     */
    public requestFriendship( friendName) : Promise<boolean> {
        return this.api.send( 'friends', 'send_request', {
            "friend_name" : friendName
        });
    }

    /**
     * Send the answer of a another user request
     * @param uid
     * @param is_accepted
     * @returns {Promise<boolean>}
     */
    public answerRequest( uid : string, is_accepted : boolean) : Promise<boolean> {
        return this.api.send( 'friends', 'answer_request', {
            "uid" : uid,
            "is_accepted" : is_accepted,
        });
    }

    /**
     * Remove another user from the current user friend list
     * @param uid
     * @returns {Promise<boolean>}
     */
    public removeFriend( uid : string) : Promise<boolean> {
        return this.api.send( 'friends', 'remove_friend', {
            "uid" : uid,
        });
    }

    /**
     * Generate a new friendship key for the current user, and return it
     * @returns {Promise<{ friendship_key : string }>}
     */
    public getFriendshipKey() : Promise<{ friendship_key : string }> {
        return this.api.send( 'friends', 'get_friendship_key');
    }

    /**
     * Generate a new friendship key for the current user, and return it
     * @returns {Promise<{ friendship_key : string }>}
     */
    public generateNewFriendshipKey() : Promise<{ friendship_key : string }> {
        return this.api.send( 'friends', 'generate_key');
    }

}


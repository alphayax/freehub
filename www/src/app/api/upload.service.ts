import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";


@Injectable()
export class UploadService {

    constructor(
        private api: FreehubApiService,
    ) { }

    getAllShareLinks() : Promise<any> {
        return this.api.get( 'uploads/');
    }

    revokeShareLinksFromToken( token) : Promise<any> {
        return this.api.delete( 'uploads/'+ token + '/');
    }

}

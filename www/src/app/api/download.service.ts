import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {DownloadItem} from "./models/download-item";
import {DownloadFile} from "./models/download-file";


@Injectable()
export class DownloadService {

    constructor(
        private api : FreehubApiService,
    ) { }

    /**
     * Get downloads
     * @returns {Promise<DownloadItem[]>}
     */
    getDownloads() : Promise<DownloadItem[]> {
        return this.api.get( 'downloads/')
            .then( response => response as DownloadItem[]);
    }

    cleanDone() : Promise<number[]> {
        return this.api.delete( '/downloads/clear')
            .then( response => response as number[]);
    }

    cleanFromId( downloadTaskId : number) : Promise<DownloadItem> {
        return this.api.delete( '/downloads/' + downloadTaskId + '/clear');
    }

    removeFromId( downloadTaskId : number) : Promise<DownloadItem> {
        return this.api.delete( '/downloads/' + downloadTaskId + '/erase');
    }

    /**
     * API Call to update status of a download item
     * @param {number} downloadTaskId
     * @param {string} status
     * @returns {Promise<DownloadItem>}
     */
    updateStatusFromId( downloadTaskId : number, status : string) : Promise<DownloadItem>{
        return this.api.put( 'downloads/' + downloadTaskId + '/', {
            "status" : status
        });
    }

    /**
     * API Call to get files in a download task
     * @param {number} downloadTaskId
     * @returns {Promise<DownloadFile[]>}
     */
    getFilesFromId( downloadTaskId : number) : Promise<DownloadFile[]>{
        return this.api.get( 'downloads/'+ downloadTaskId + '/files');
    }

    /**
     * Add a download from URL
     * @param url
     * @returns {Promise<any>}
     */
    public addDownloadFromUrl( url : string){
        return this.api.post( 'downloads/', {
            "url" : url,
        });
    }
}


import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {FreeboxAssociation} from "../shared/freebox-association";
import {FreeboxStatus} from "./models/freebox-status";
import {FreeboxInfo} from "./models/freebox-info";


@Injectable()
export class FreeboxService {

    constructor(
        private api: FreehubApiService,
    ) { }

    getAll() : Promise<FreeboxInfo[]>{
        return this.api.get( 'friends/freebox');
    }

    getFreeboxInfo() : Promise<FreeboxAssociation>{
        return this.api.get( 'link/');
    }

    getFreeboxStatus( uid) : Promise<FreeboxStatus>{
        return this.api.get( 'freebox/'+ uid + '/status');
    }

    removeFreebox( uid){
        // Will be DELETE on link/
        return this.api.send( 'freebox', 'remove', {
            "uid"   : uid
        })
    }

    getPermissions() {
        return this.api.get( 'link/authorizations')
    }

    updateLink( association = null) {
        return this.api.post( 'link/', {
            "assoc" : association
        })
    }

}


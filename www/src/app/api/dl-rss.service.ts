import {Injectable} from '@angular/core';
import {FreehubApiService} from "../shared/freehub-api.service";
import {RssSearch} from "./models/rss-search";


@Injectable()
export class DlRssService {

  constructor(
    private api: FreehubApiService,
    ) {
  }

  /**
   * API Call to update a RSS Search
   * @param rssSearch
   * @returns {Promise<RssSearch>}
   */
  public updateRss(rssSearch): Promise<RssSearch> {
    return this.api.put('downloads/rss/' + rssSearch.id + '/', {
      'item': rssSearch,
    });
  }

  /**
   * API Call to get all RSS Search
   * @returns {Promise<RssSearch[]>}
   */
  public getAll() : Promise<RssSearch[]> {
    return this.api.get('downloads/rss/');
  }

  /**
   * API Call to create a new RSS scan
   * @param rssScan
   * @returns {Promise<any>}
   */
  public add( rssScan) {
      return this.api.post('downloads/rss/', rssScan);
  }

  /**
   * API Call to check a RSS Search
   * @param {string} rssId
   * @returns {Promise<RssSearch>}
   */
  public check(rssId: string) : Promise<RssSearch>{
    return this.api.get('downloads/rss/' + rssId + '/check');
  }

  /**
   * API Call to delete a RSS Search
   * @param {string} rssId
   * @returns {Promise<any>}
   */
  public removeFromId(rssId: string) : Promise<any> {
    return this.api.delete('downloads/rss/'+ rssId + '/');
  }

}



export class MovieTitle {
    rawTitle  : string;
    episode   : number;
    season    : number;
    cleanName : string;

    public parse( movieTitle) : void {
        if( ! movieTitle){
            return;
        }
        this.rawTitle  = movieTitle.rawTitle;
        this.episode   = movieTitle.episode;
        this.season    = movieTitle.season;
        this.cleanName = movieTitle.cleanName;
    }

    public getSeason() : string {
        return MovieTitle.normalizeNumber( this.season);
    }

    public getEpisode() : string {
        return MovieTitle.normalizeNumber( this.episode);
    }

    protected static normalizeNumber(number : number) : string {
        if( ! number){
            return '';
        }
        return number.toLocaleString( 'fr-FR', {minimumIntegerDigits : 2});
    }

    protected isSeasonFinaleEpisode() : boolean {
        return null !== this.rawTitle.match( /FINAL/i );
    }

    protected isSeasonIntegrale() : boolean {
        return (this.season && ! this.episode);
    }

    public hasSeason() : boolean {
        return this.season > 0;
    }

    public hasEpisode() : boolean {
        return this.episode > 0;
    }

    public isTvShow() : boolean {
        return ! this.hasEpisode() && ! this.hasSeason();
    }

    public getNextName() : string {

        // If episode has no season, just increment the episode number
        if( ! this.hasSeason()) {
            return this.cleanName + ' E' + MovieTitle.normalizeNumber( this.episode + 1);
        }

        // If the season is integral, just increment the season number
        if( this.isSeasonIntegrale()){
            return this.cleanName + ' S' + MovieTitle.normalizeNumber( this.season + 1);
        }

        if( this.hasEpisode()){
            if( this.isSeasonFinaleEpisode()){
                return this.cleanName + ' S' + MovieTitle.normalizeNumber( this.season + 1) + 'E'+ MovieTitle.normalizeNumber( 1);
            } else {
                return this.cleanName + ' S' + this.getSeason() + 'E'+ MovieTitle.normalizeNumber( this.episode + 1);
            }
        }

        return this.cleanName;
    }

}

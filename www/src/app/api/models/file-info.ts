
import { MovieTitle } from "./movie-title";

export interface FileInfoOriginal {
    filecount : any;
    foldercount: any;
    hidden: boolean;
    index : number;
    link: boolean;
    mimetype: string;
    modification : number;
    name : string;
    path : string;
    size: number;
    target : any;
    type: string;
}

export class FileInfo {

    fileInfo : FileInfoOriginal;

    movieTitle : MovieTitle;

    name: string;
    path: string;
    image: string;

    constructor(){
        this.movieTitle = new MovieTitle;
    }

    public parse( obj){
        this.fileInfo   = obj.fileInfo;
        this.name       = obj.name;
        this.path       = obj.path;
        this.image      = obj.image;
        this.movieTitle.parse( obj.movieTitle);
    }

    public isDir() : boolean {
        return this.fileInfo.type === 'dir';
    }

    public isVideo() : boolean {
        return this.fileInfo.mimetype.substr( 0, 5) === 'video';
    }

    public isAudio() : boolean {
        return this.fileInfo.mimetype.substr( 0, 5) === 'audio';
    }

    public isPicture() : boolean {
        return this.fileInfo.mimetype.substr( 0, 5) === 'image';
    }

    public isBasicFile() : boolean {
        return ! this.isDir() && ! this.isVideo() && ! this.isAudio() && ! this.isPicture();
    }

    public canBePlay() : boolean {
        return this.isVideo() || this.isAudio();
    }

    public canBeStreamed() : boolean {
        return (
            this.fileInfo.mimetype === 'video/ogg' ||
            this.fileInfo.mimetype === 'video/mp4' ||
            this.fileInfo.mimetype === 'video/webm'
        );
    }

}

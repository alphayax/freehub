
export class RssSearch {
    id : string;
    title : string;
    last_date : number;
    last_publication : number;
    last_items : string[];
    pattern : string;
    rss : string;
}

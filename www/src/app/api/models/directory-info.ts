
import {FileInfo} from "./file-info";

export class DirectoryPart {
    name: string;
    path: string;
}

export class DirectoryInfo {
    path: string;
    path_part: DirectoryPart;
    files : FileInfo[];

    public parse( obj) : void {
        this.path = obj.path;

        this.path_part = obj.path_part;

        this.files = [];
        for( let fileInfo_obj of obj.files){
            let fileInfo = new FileInfo();
                fileInfo.parse( fileInfo_obj);
            this.files.push( fileInfo);
        }
    }
}

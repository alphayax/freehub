
import { MovieTitle } from "./movie-title";

export interface DownloadTask {
    archive_password : string;
    created_ts : number;
    download_dir : string;
    error : string;
    eta : number;
    id : number;
    io_priority : string;
    name : string;
    queue_pos : number;
    rx_bytes : number;
    rx_pct : number;
    rx_rate : number;
    size : number;
    status : string;
    stop_ratio : number;
    tx_bytes : number;
    tx_pct : number;
    tx_rate : number;
    type : string;
}

export class DownloadItem {
    downloadTask : DownloadTask;
    movieTitle : MovieTitle;
    image : string;
    path: string;

    constructor(){
        this.movieTitle = new MovieTitle;
    }

    public parse( download: DownloadItem) {
        this.downloadTask   = download.downloadTask;
        this.path           = download.path;
        this.image          = this.image || download.image;
        this.movieTitle.parse( download.movieTitle);
    }

    public isSeeding(){
        return this.downloadTask.status === 'seeding';
    }

    public isDownloaded(){
        return this.downloadTask.status === 'done';
    }

    public isDownloading(){
        return this.downloadTask.status === 'downloading';
    }

    public isActive(){
        return this.isDownloading() || ( this.isSeeding() && this.downloadTask.tx_rate > 0)
    }

    public isStoppable() {
        return this.downloadTask.status !== 'stopping' || ( ! this.isStopped() &&  ! this.isInError());
    }

    public isStopped() {
        return this.downloadTask.status === 'stopped';
    }

    public isInError() {
        return this.downloadTask.status === 'error';
    }

    public getCurrentRatio() : number {
        return this.downloadTask.tx_bytes / this.downloadTask.rx_bytes;
    }

    public getStopRatio() : number {
        return this.downloadTask.stop_ratio / 100;
    }

    public getStatus() : string {
        switch( this.downloadTask.status){
            case 'seeding'      : return 'En partage';
            case 'downloading'  : return 'En cours de téléchargement';
            case 'error'        : return 'En erreur';
            case 'stopped'      : return 'Arrété';
            case 'done'         : return 'Terminé';
            default : return this.downloadTask.status;
        }
    }

    public getType() : string {
        switch( this.downloadTask.type){
            case 'bt'   : return 'Bittorent';
            case 'http' : return 'Direct download';
            case 'nzb'  : return 'Usenet';
            default : return this.downloadTask.type;
        }
    }

    /**
     * Transform a number of seconds into a readable string of time
     * @returns {string}
     */
    public getEta() : string {
        let seconds : number = this.downloadTask.eta;
        let minutes : number;
        let hours   : number;
        let days    : number;

        /// Trivial case
        if( ! seconds || seconds === 0){
            return '';
        }

        /// Seconds
        if( seconds < 60){
            return seconds + 's';
        }

        /// Minutes
        minutes = Math.floor( seconds / 60);
        seconds = seconds % 60;
        if( minutes < 60){
            return minutes + 'mn '+ seconds + 's';
        }

        /// hours
        hours   = Math.floor( minutes / 60);
        minutes = minutes % 60;
        if( hours < 24){
            return hours + 'h '+ minutes + 'mn '+ seconds + 's';
        }

        /// Days
        days  = Math.floor( hours / 24);
        hours = hours % 24;

        return days + 'd ' + hours + 'h '+ minutes + 'mn '+ seconds + 's';
    }

}


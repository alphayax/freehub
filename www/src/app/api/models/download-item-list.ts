import {DownloadItem} from "./download-item";


export class DownloadItemList {

    downloadItems : DownloadItem[];

    constructor(){
        this.downloadItems = [];
    }

    public parse( downloadItem_obj : DownloadItem[]) : void{
        this.downloadItems = [];
        for( let download_obj of downloadItem_obj){
            let downloadItem = new DownloadItem();
                downloadItem.parse( download_obj);
            this.downloadItems.push( downloadItem);
        }
        this.sort();
    }

    public sort(){
        this.downloadItems.sort( (a:DownloadItem, b:DownloadItem) => a.downloadTask.name.localeCompare( b.downloadTask.name));
    }

    public removeFromIds( removedDownloadIds : number[]) : void {
        for( let removedDownloadId of removedDownloadIds){
            this.removeFromId( removedDownloadId);
        }
    }

    public removeFromId( removedDownloadId : number) : void {
        this.downloadItems = this.downloadItems.filter( download => removedDownloadId !== download.downloadTask.id);
    }

    public getAll() : DownloadItem[] {
        return this.downloadItems;
    }

    public getAllByCategories() : DownloadItem[] {

        return this.downloadItems;
    }

    public getSeeding() : DownloadItem[] {
        return this.downloadItems.filter( download => download.isSeeding());
    }

    public getActive() : DownloadItem[] {
        return this.downloadItems.filter( download => download.isActive());
    }

    public getDownloaded() : DownloadItem[] {
        return this.downloadItems.filter( download => download.isDownloaded());
    }

    public getInError() : DownloadItem[] {
        return this.downloadItems.filter( download => download.isInError());
    }

    public getAllCount() : number {
        return this.getAll().length;
    }

    public getSeedingCount() : number {
        return this.getSeeding().length;
    }

    public getActiveCount() : number {
        return this.getActive().length;
    }

    public getDownloadedCount() : number {
        return this.getDownloaded().length;
    }

    public getInErrorCount() : number {
        return this.getInError().length;
    }

    public getFromId( downloadTaskId: number) : DownloadItem {
        return this.downloadItems.filter( download => downloadTaskId === download.downloadTask.id).pop();
    }

}


export class DownloadFile {
    id       : string;
    task_id  : string;
    path     : string;
    filepath : string;
    name     : string;
    mimetype : string;
    size     : number;
    rx       : number;
    status   : string;
    priority : string;
    error    : string;
}

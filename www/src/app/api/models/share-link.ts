
export class ShareLink {
    name : string;
    fullurl : string;
    expire : number;
    token : string;
    path : string;
}

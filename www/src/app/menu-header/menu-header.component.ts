import {Component, OnInit} from '@angular/core';
import {FreehubUserService} from "../shared/freehub-user.service";
import {Router} from "@angular/router";
import {UserService} from "../api/user.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ModalSignupComponent} from "../modal-signup/modal-signup.component";
import {ModalLoginComponent} from "../modal-login/modal-login.component";


@Component({
  selector: 'menu-header',
  templateUrl: 'menu-header.component.html',
  providers: [UserService],
})

export class MenuHeaderComponent implements OnInit {

  constructor(
    private us: FreehubUserService,
    private userService: UserService,
    private modalService: BsModalService,
    ) {
  }

  ngOnInit(): void {

  }

  logout() {
    this.userService.logout().then(
      () => {
        this.us.clearUserStatus();
        window.location.href = '';
      }
    );
  }

  isLoggedIn() {
    return this.us.isLoggedIn();
  }

  isBoxLinked() {
    return this.us.isBoxLinked();
  }


  openModalLogin() {
    this.modalService.show(ModalLoginComponent);
  }

  openModalSignUp() {
    this.modalService.show(ModalSignupComponent);
  }
}


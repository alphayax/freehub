import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {FreehubUserService} from "../shared/freehub-user.service";
import {FreehubApiService} from "../shared/freehub-api.service";
import {AyxAlertService} from "../shared/ayx-alert/ayx-alert.service";

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.css']
})
export class ModalLoginComponent {

  private login;

  private password;

  constructor(
    public bsModalRef: BsModalRef,
    private us: FreehubUserService,
    private api: FreehubApiService,
    private ayxAlert: AyxAlertService,
    ) {
  }

  processLogin() {

    let body = {
      login: this.login,
      password: this.password,
    };

    this.api.post('user/login', body).then(
      userStatus => {
        this.us.updateUserStatus(userStatus);
        this.bsModalRef.hide();
      },
      error => {
        this.ayxAlert.addDanger(error);
      });
  }
}

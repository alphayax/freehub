import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { LoggedInGuard } from "../shared/guards/logged-in.guard";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'home', component: HomeComponent, canActivate: [ LoggedInGuard ] }
    ])],
    exports: [RouterModule]
})

export class HomeRoutingModule {

}

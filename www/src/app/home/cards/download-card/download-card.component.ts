import { Component, OnInit } from "@angular/core";
import { DownloadService } from "../../../api/download.service";
import { DownloadItemList } from "../../../api/models/download-item-list";
import {FreehubUserService} from "../../../shared/freehub-user.service";


@Component({
    selector: 'download-card',
    templateUrl: 'download-card.component.html',
    providers : [
        DownloadService,
    ]
})

export class DownloadCardComponent implements OnInit{

    protected downloadItemList : DownloadItemList;

    constructor(
        private downloadService : DownloadService,
        private us : FreehubUserService,
    ) {
        this.downloadItemList = new DownloadItemList;
    }

    ngOnInit() {
      this.refresh();
    }

    isBoxLinked(){
      return this.us.userStatus.isBoxLinked;
    }

    refresh(){
        if( this.isBoxLinked()){
            this.downloadService.getDownloads().then( downloads => {
                this.downloadItemList.parse( downloads);
            });
        }
    }

    getAllCount(): number {
        return this.downloadItemList.getAllCount();
    }

    getSeedingCount() : number {
        return this.downloadItemList.getSeedingCount();
    }

    getActiveCount() : number {
        return this.downloadItemList.getActiveCount();
    }

    getDownloadedCount() : number {
        return this.downloadItemList.getDownloadedCount();
    }

}

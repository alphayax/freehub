import { Component, OnInit } from "@angular/core";
import {FreehubUserService} from "../../../shared/freehub-user.service";


@Component({
    selector: 'freebox-card',
    templateUrl: 'freebox-card.component.html',
    providers : [
    ]
})

export class FreeboxCardComponent implements OnInit {

    constructor(
        private us : FreehubUserService,
    ) { }

    ngOnInit() {

    }

    isBoxLinked(){
      return this.us.userStatus.isBoxLinked;
    }

    getUid(){
      return this.us.userStatus.uid;
    }
}

import { Component, OnInit } from "@angular/core";
import {FriendsService} from "../../../api/friends.service";


@Component({
    selector: 'friends-card',
    templateUrl: 'friends-card.component.html',
    providers : [
        FriendsService,
    ]
})

export class FriendsCardComponent implements OnInit {

    friends : { uid: string, name: string }[] = [];

    friendshipRequests : { uid: string, name: string }[] = [];

    constructor(
        private friendsService : FriendsService,
    ) {}

    ngOnInit() {
        this.friends = [];
        this.friendshipRequests = [];

        this.friendsService.getFriends().then(
            friends => {
                this.friends = friends;
            }
        );
        this.friendsService.getFriendshipRequests().then(
            friendshipRequests => {
                this.friendshipRequests = friendshipRequests;
            }
        );
    }

}

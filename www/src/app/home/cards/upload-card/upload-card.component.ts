import { Component, OnInit } from "@angular/core";
import {UploadService} from "../../../api/upload.service";
import {ShareLink} from "../../../api/models/share-link";
import {FreehubUserService} from "../../../shared/freehub-user.service";


@Component({
    selector: 'upload-card',
    templateUrl: 'upload-card.component.html',
    providers : [
        UploadService,
    ]
})

export class UploadCardComponent implements OnInit{

    protected shareLinks : ShareLink[];

    constructor(
        private uploadService : UploadService,
        private us : FreehubUserService,
    ) {
        this.shareLinks = [];
    }

    public isBoxLinked(){
      return this.us.userStatus.isBoxLinked;
    }

    ngOnInit() {
      this.refresh();
    }

    refresh(){
      if( ! this.isBoxLinked()) {
        return;
      }

      this.uploadService.getAllShareLinks().then(shareLinks => {
        this.shareLinks = shareLinks;
      });
    }

}

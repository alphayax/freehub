import {Component, OnInit} from '@angular/core';
import {FreehubUserService} from "../shared/freehub-user.service";

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    providers: [],
})

export class HomeComponent implements OnInit {

    constructor(
        private us : FreehubUserService
    ) {}

    ngOnInit(): void {

    }

    isLogged(){
      return this.us.userStatus.loggedIn;
    }

}


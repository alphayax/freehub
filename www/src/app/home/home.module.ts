
import { NgModule } from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {HomeComponent} from "./home.component";
import {HomeRoutingModule} from "./home-routing.module";
import {DownloadCardComponent} from "./cards/download-card/download-card.component";
import {UploadCardComponent} from "./cards/upload-card/upload-card.component";
import {FreeboxCardComponent} from "./cards/freebox-card/freebox-card.component";
import {FriendsCardComponent} from "./cards/friends-card/friends-card.component";


@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        SharedModule,
    ],
    declarations: [
        HomeComponent,
        DownloadCardComponent,
        FreeboxCardComponent,
        FriendsCardComponent,
        UploadCardComponent,
    ],
    providers: [
    ]
})

export class HomeModule {

}

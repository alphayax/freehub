import { NgModule } from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {AssociationComponent} from "./association.component";
import {AssociationRoutingModule} from "./association-routing.module";
import {AssociationStep1Component} from "./association-step1/association-step1.component";
import {AssociationStep2Component} from "./association-step2/association-step2.component";
import {AssociationStep3Component} from "./association-step3/association-step3.component";
import {FormsModule} from "@angular/forms";


@NgModule({
    imports: [
        CommonModule,
      FormsModule,
        SharedModule,
        AssociationRoutingModule,
    ],
    declarations: [
        AssociationComponent,
        AssociationStep1Component,
        AssociationStep2Component,
        AssociationStep3Component,
    ],
    providers: [
    ]
})

export class AssociationModule {

}

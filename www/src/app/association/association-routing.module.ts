import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {AssociationComponent} from "./association.component";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'association', component: AssociationComponent, canActivate: [ LoggedInGuard ] }
    ])],
    exports: [RouterModule]
})

export class AssociationRoutingModule {

}

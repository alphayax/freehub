import {Component} from '@angular/core';
import {FreeboxAssociation} from "../shared/freebox-association";
import {FreeboxService} from "../api/freebox.service";

@Component({
    selector: 'association',
    templateUrl: 'association.component.html',
    providers : [
        FreeboxService,
    ]
})

export class AssociationComponent {

    step: number;

    freeboxAssociation : FreeboxAssociation;
    permissions : any;

    constructor(
        private freeboxService : FreeboxService,
    ){
        this.checkFreebox();
        this.freeboxAssociation = new FreeboxAssociation;
        this.step = 0;
    }

    checkFreebox() {
        this.freeboxService.getPermissions()
            .then( result => {
                this.permissions = result;
                this.step = 0;
            })
            .catch( () => {
                this.step = 1;
            })
    }

    onStep1Validation( freeboxAssociation: FreeboxAssociation){
        this.freeboxAssociation = freeboxAssociation;
        this.step = 2;
    }

    onStep2Validation( freeboxAssociation: FreeboxAssociation){
        this.freeboxAssociation = freeboxAssociation;
        this.step = 3;
    }

    onStep3Validation( freeboxAssociation: FreeboxAssociation){
        this.freeboxAssociation = freeboxAssociation;
        this.checkFreebox();
    }

}


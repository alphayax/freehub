import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {FreeboxAssociation} from "../../shared/freebox-association";
import {FreeboxService} from "../../api/freebox.service";
import {FreehubUserService} from "../../shared/freehub-user.service";

@Component({
    selector: 'association-step3',
    templateUrl: 'association-step3.component.html',
    providers : [
        FreeboxService,
    ]
})

export class AssociationStep3Component {

    @Input()
    freeboxAssociation : FreeboxAssociation;

    @Output()
    onStepValidation = new EventEmitter<FreeboxAssociation>();

    constructor(
        private freeboxService : FreeboxService,
    ){ }

    validate(){

        this.freeboxService.getPermissions().then( freebox => {
            console.log( freebox);
            this.onStepValidation.emit( this.freeboxAssociation);
        })
        .catch( error => {
            console.log(error);
        });
    }

}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FreeboxAssociation} from "../../shared/freebox-association";
import {FreeboxService} from "../../api/freebox.service";
import {FreehubUserService} from "../../shared/freehub-user.service";
import {AyxAlertService} from "../../shared/ayx-alert/ayx-alert.service";
import {HttpClient} from "@angular/common/http";

@Component({
    selector: 'association-step2',
    templateUrl: 'association-step2.component.html',
    providers : [
        FreeboxService,
      AyxAlertService,
    ]
})

export class AssociationStep2Component {

    @Input()
    freeboxAssociation : FreeboxAssociation;

    @Output()
    onStepValidation = new EventEmitter<FreeboxAssociation>();

    constructor(
        private freeboxService : FreeboxService,
        private freeHubUserService : FreehubUserService,
        private ayxAlertService : AyxAlertService,
    ){ }

    saveToken(){

        /// Check parameters
        if( ! this.freeboxAssociation.app_token){
            this.ayxAlertService.addWarning( 'Erreur : Tous les champs ne sont pas renseignes');
            return;
        }

        // Clean token
        this.freeboxAssociation.app_token = this.freeboxAssociation.app_token.replace( /\\\//g, '/');

        this.freeboxService.updateLink( this.freeboxAssociation).then( freebox => {
            console.log( freebox);
            this.onStepValidation.emit( this.freeboxAssociation);
        });
    }

}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FreeboxAssociation} from "../../shared/freebox-association";
import {FreeboxService} from "../../api/freebox.service";

@Component({
    selector: 'association-step1',
    templateUrl: 'association-step1.component.html',
    providers : [
        FreeboxService,
    ]
})

export class AssociationStep1Component {

    @Input()
    freeboxAssociation : FreeboxAssociation;

    @Output()
    onStepValidation = new EventEmitter<FreeboxAssociation>();

    constructor(
        private freeboxService : FreeboxService,
    ){ }

    /**
     * Update the linkage
     */
    saveAssociation(){
        this.freeboxService.updateLink( this.freeboxAssociation).then( freebox => {
            console.log( freebox);
            this.onStepValidation.emit( this.freeboxAssociation);
        });
    }

}

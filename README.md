
# Freehub (Project NOT under ACTIVE development)

[![build status](https://gitlab.com/alphayax/freehub/badges/master/build.svg)](https://gitlab.com/alphayax/freehub/commits/master)

Freebox interconnection

## Installation

### Backend

```bash
cd api
composer install
```

### Frontend

```bash
cd www
npm install
ng build
```

## Configuration

### Backend

Create `freehub.yml` file (from `api/src/etc/freehub.sample.yml`)

### Frontend

Create `api.json` file (from `www/src/assets/api.sample.json`)

## Run


```bash
docker-compose up
```
